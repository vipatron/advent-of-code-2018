// import { getInput } from '../helpers/helpers.js'
console.clear();

const INPUT = "initial state: .#####.##.#.##...#.#.###..#.#..#..#.....#..####.#.##.#######..#...##.#..#.#######...#.#.#..##..#.#.#\n\n#..#. => .\n##... => #\n#.... => .\n#...# => #\n...#. => .\n.#..# => #\n#.#.# => .\n..... => .\n##.## => #\n##.#. => #\n###.. => #\n#.##. => .\n#.#.. => #\n##..# => #\n..#.# => #\n..#.. => .\n.##.. => .\n...## => #\n....# => .\n#.### => #\n#..## => #\n..### => #\n####. => #\n.#.#. => #\n.#### => .\n###.# => #\n##### => #\n.#.## => .\n.##.# => .\n.###. => .\n..##. => .\n.#... => #";
// const INPUT = "initial state: #..#.#..##......###...###\n\n...## => #\n..#.. => #\n.#... => #\n.#.#. => #\n.#.## => #\n.##.. => #\n.#### => #\n#.#.# => #\n#.### => #\n##.#. => #\n##.## => #\n###.. => #\n###.# => #\n####. => #";

const day12 = (generations, isPart2) => {
	const tokens = INPUT.split("\n");
	let state = tokens[0].match(/[.#]+/)[0];
	console.log ("Initial State:", state);
	console.log("============");
	let rules = tokens.slice(2).reduce(
		(patternMap, rule) => {
			let matches = rule.match(/([.#]+).* ([.#])/);
			//matches[1] is the rule, and matches[2] is the replacement.
			patternMap[matches[1]] = matches[2];
			return patternMap;
		}, 
		{}//return an object where the patterns are keys, and the replacement string(#|.) are values.
	);
	// console.log(rules);
	
	let startIndex = 0;
	let endIndex = state.length - 1;
	// console.log(startIndex, endIndex);
	let finalGenEndIndexOffset = 0;
	
	//during each iteration...
	for (let gen = 1; gen <= generations; gen++){
		//if the beginning and end of the strings aren't "..."
		let matches = state.match(/^(\.*).*[^\.](\.*)$/);
		if (matches[1].length < 3){
			//prepend/append what is necessary until they are. matches[1/2].length >=3
			let prefix =  ".".repeat(3 - matches[1].length);
			state = prefix + state;
			//adjust indices appropriately.
			startIndex -= prefix.length;
		}
		if (matches[2].length < 3){
			//prepend/append what is necessary until they are. matches[1/2].length >=3
			let suffix =  ".".repeat(3 - matches[2].length);
			state += suffix;
			//adjust indices appropriately.
			endIndex += suffix.length;
		}

		//apply the rules to every element in stateString[2, len-2)
		let nextState = state.slice(0,2);
		for (let i = 2; i < state.length-2; i++){
			//map the result onto the new Statestring (+=?)
			nextState += rules[state.slice(i-2, i+3)] || ".";//only necessary for test input
		}
		nextState += state.slice(-2);
		// //print the string?
		// console.log(startIndex, nextState, endIndex);
		// console.log(state);
		
		if (isPart2){
			if (nextState.match(/^\.+(.*#)\.+$/)[1] === state.match(/^\.+(.*#)\.+$/)[1]){
				console.log(startIndex, nextState, endIndex);
				console.log(gen+":", state);
				state = nextState;
				finalGenEndIndexOffset = endIndex - gen;
				break;

			}
		}
		
		// console.log(nextState.slice(-3-startIndex), startIndex, endIndex);
		state = nextState;
		
		finalGenEndIndexOffset = endIndex - gen;
	}
	// //tally the score by iterating over the string from [startIndex to endIndex], and adding the index to the tally if stateString[index] === "#"
	// let tally = 0;
	// for (let i=0; i<state.length; i++){
		// if (state[i] === "#"){
			// tally += startIndex + i;
		// }
	// }
	
	//tally the score from [endIndex to startIndex], and adding 
	let tally = 0;
	for (let i=0; i<state.length; i++){
		if (state[state.length - 1 - i] === "#"){
			tally += generations + finalGenEndIndexOffset - i;
		}
	}
	
	return tally;
}

let puzzle1result, puzzle2result;

console.time("string approach20");
puzzle1result = day12(20, false);
console.timeEnd("string approach20");
console.log("puzzle1: ", puzzle1result);

// console.time("bit approach20");
// puzzle2result = part2(20);
// console.timeEnd("bit approach20");

// console.log("puzzle2: ", puzzle2result);

//the bit algo breaks after 42 generations.
let uppertest = 130;

console.time("string approach"+uppertest);
puzzle1result = day12(50000000000, true);
console.timeEnd("string approach"+uppertest);
console.log("puzzle1: ", puzzle1result);


// console.time("bit approach"+uppertest);
// puzzle2result = part2(uppertest);
// console.timeEnd("bit approach"+uppertest);
// console.log("puzzle2: ", puzzle2result);