import { getInput } from '../helpers/helpers.js'
console.clear();

const VERBOSE = false;
const MODERATE = true || VERBOSE;
const TERSE = false || MODERATE;

// const INPUT = getInput(23);

// const INPUT = "pos=<0,0,0>, r=4\npos=<1,0,0>, r=1\npos=<4,0,0>, r=3\npos=<0,2,0>, r=1\npos=<0,5,0>, r=3\npos=<0,0,3>, r=1\npos=<1,1,1>, r=1\npos=<1,1,2>, r=1\npos=<1,3,1>, r=1";

//sample part 2:
// const INPUT = "pos=<10,12,12>, r=2\npos=<12,14,12>, r=2\npos=<16,12,12>, r=4\npos=<14,14,14>, r=6\npos=<50,50,50>, r=200\npos=<10,10,10>, r=5";

//no outlier sample:
// const INPUT = "pos=<10,12,12>, r=2\npos=<12,14,12>, r=2\npos=<16,12,12>, r=4\npos=<14,14,14>, r=6\npos=<10,10,10>, r=5";
// const INPUT = "pos=<10,12,10>, r=2"; //to test my line theory.
// const INPUT = "pos=<10,12,12>, r=2\npos=<12,14,12>, r=2\npos=<16,12,12>, r=4";

// const INPUT = "pos=<3,2,0>, r=1\npos=<-2,3,0>, r=1\npos=<-3,-4,0>, r=2\npos=<3,-3,0>, r=1";//checks that boundary line algo works in all quadrants.
// const INPUT = "pos=<11,12,12>, r=2\npos=<12,17,12>, r=2\npos=<16,12,12>, r=4";//single overlap
const INPUT = "pos=<11,12,12>, r=2\npos=<12,17,12>, r=2\npos=<11,17,12, r=2>, r=2\npos=<16,12,12>, r=4";//tests for child Quadtrees (2 bots in one subdivision)


// const INPUT = "pos=<10,12,12>, r=2\npos=<12,17,12>, r=2\npos=<16,12,12>, r=4\npos=<14,14,14>, r=6";

const day23 = () => {
	const tokens = INPUT.trim().split("\n");

	const puzzle1result = part1(tokens);
	const puzzle2result = part2(tokens);
	
	console.log("puzzle1: ", puzzle1result);
	console.log("puzzle2: ", puzzle2result);
}

const manhattanDistance = (bot1, bot2) =>
	Math.abs(bot1.x - bot2.x) + 
	Math.abs(bot1.y - bot2.y) + 
	Math.abs(bot1.z - bot2.z);

const TwoDManhattanDistance = (bot1, bot2) => Math.abs(bot1.x - bot2.x) + Math.abs(bot1.y - bot2.y);

function part1(lines){
	const nanobots = lines.map(
		line => {
			const [x,y,z,r] = line.match(/-?\d+/g).map(num => parseInt(num));
			return {x,y,z,r};
		}
	);
	// console.log("nanobots[]", nanobots);
	const strongest = nanobots.reduce(
		(strongest, bot) => bot.r > strongest.r ? bot : strongest
	);
	// console.log("strongest", strongest);
	
	
	// console.log("self distance:", manhattanDistance(strongest));
	const inRange = nanobots.filter(
		bot => manhattanDistance(bot, strongest) <= strongest.r
	);
	// nanobots.forEach(
		// bot => console.log(`The nanobot at (${bot.x},${bot.y},${bot.z}) is distance ${manhattanDistance(bot)} away and so it is ${manhattanDistance(bot) <= strongest.r ? "IN RANGE" : "NOT in range"}.`)
	// );

	return inRange.length;
}

function part2(lines){
	const nanobots = lines.map(
		line => {
			const [x,y,z,r] = line.match(/-?\d+/g).map(num => parseInt(num));
			return {x,y,z,r};
		}
	);

	let gridRange = nanobots.reduce(
		(extents, bot) => ({
			xMin: Math.min(bot.x - bot.r, extents.xMin),
			xMax: Math.max(bot.x + bot.r, extents.xMax),
			yMin: Math.min(bot.y - bot.r, extents.yMin),
			yMax: Math.max(bot.y + bot.r, extents.yMax),
			zMin: Math.min(bot.z - bot.r, extents.zMin),
			zMax: Math.max(bot.z + bot.r, extents.zMax)
		}),
		{
			xMin: Infinity,
			xMax: Number.NEGATIVE_INFINITY,
			yMin: Infinity,
			yMax: Number.NEGATIVE_INFINITY,
			zMin: Infinity,
			zMax: Number.NEGATIVE_INFINITY
		}
	);
	console.log("bots", nanobots);
	console.log("gridRange", gridRange);

	if (MODERATE){//print the formatted grid.
		const numBotsIn2DRange = point => nanobots.map(bot => TwoDManhattanDistance(bot, point) <= bot.r).reduce((sum, elem) => sum + (elem ? 1 : 0), 0)

		const PRINT_FROM_ORIGIN = false;
		const LIMIT_RANGE_TO_BOTS = false;
		if (PRINT_FROM_ORIGIN){
			gridRange.xMin = 0;
			gridRange.yMin = 0;
		}
		if (LIMIT_RANGE_TO_BOTS){
			gridRange = nanobots.reduce(
				(extents, bot) => ({
					xMin: Math.min(bot.x, extents.xMin),
					xMax: Math.max(bot.x, extents.xMax),
					yMin: Math.min(bot.y, extents.yMin),
					yMax: Math.max(bot.y, extents.yMax),
				}),
				{
					xMin: Infinity,
					xMax: Number.NEGATIVE_INFINITY,
					yMin: Infinity,
					yMax: Number.NEGATIVE_INFINITY,
				}
			);
		}

		const grid = Array(gridRange.yMax - gridRange.yMin + 1).fill(null).map(row => Array(gridRange.xMax - gridRange.xMin + 1).fill(0));
		console.log(`(${gridRange.xMin}, ${gridRange.yMin}) to (${gridRange.xMax}, ${gridRange.yMax})`);
		for (let y=0; y<grid.length; y++){
			for(let x=0; x<grid[y].length; x++){
				const point = {x: x+gridRange.xMin, y: y+gridRange.yMin};
				grid[y][x] = numBotsIn2DRange(point);
				if (grid[y][x] === 0 && point.x === 0) grid[y][x] = '|';//for testing, draw axes.
				if (grid[y][x] === 0 && point.y===0) grid[y][x] = '-';
			}
		}
		
		nanobots.forEach(bot => {
			const y = bot.y-gridRange.yMin;
			const x = bot.x-gridRange.xMin;
			grid[y][x] = 'X';
		});
		let formattingStrings = [];
		let gridString = grid.reduce(
			(str, row) => row.join("") + "\n" + str, ""
		).replace(
			/[\dX|-]/g, match => {
				let color = {
					X: "turquoise", //"black; color: turquoise",
					'|': "black;",
					'-': "black;",
					1: "rebeccapurple",
					2: "darkblue",
					3: "darkgreen",
					4: "gold",
					5: "orangered"
				}[match] || "default";
				formattingStrings.push("background:"+color, "background: default");
				return "%c"+match+"%c";
			}
		);
		console.log(gridString, ...formattingStrings);			
	}



	const biggestHalfAxis = Math.ceil(Math.log2(Object.values(gridRange).reduce((max, elem) => Math.max(max, elem))));
	console.log("rounded log2 of largest half-axis:", biggestHalfAxis);

	//cubically partition space:
	//a subpartition is needed if an (bot+range) octahedron is completely enclosed by an octahedral subpartition.
	//if it isn't it remains in the enclosing partition.
	//therefore, all overlaps have to be between a node's own octahedra and those of its descendants.
	// we can bubble up collisions, and compare their manhattan distance from the origin, keeping only the most-colliding subtrees.
	//the position of a collision/overlap is tha
	//the most-collided subtree
	class BoundaryLine {
		constructor(slope, intercept, depAxis = 'y', indAxis = 'x') {
			this.slope = slope;
			this.intercept = intercept;
			this.dependentAxis = depAxis;
			this.independentAxis = indAxis;
		}
	}

	class BoundaryNameGenerator{
		constructor(dimensions = 3){
			this._axes = ['z', 'y', 'x'].slice(3 - dimensions);
		}

		get axes(){
			return this._axes;
		}

		get numAxes(){
			return this._axes.length;
		}

		getBoundaryNames(includeOpposites = true){
			let result = [];
			const dimensionStack = [];

			const pushAxisOntoStack = (axisIndex, currentNode="") => {// currentNode is the "head" of our tree investigation of possible strings.
				dimensionStack.push(currentNode + '-' + this._axes[axisIndex]);
				dimensionStack.push(currentNode + '+' + this._axes[axisIndex]);
			}

			let axis = 0;
			pushAxisOntoStack(axis);//initialize the stack.
			axis++;
			while (dimensionStack.length > 0){
				if (axis < this.numAxes){//keep lengthening the string and push onto the stack.
					pushAxisOntoStack(axis, dimensionStack.pop());
					axis++;
				} else {//remove the string from the stack and place in results.
					if (includeOpposites){
						result.push(dimensionStack.pop(), dimensionStack.pop());
					} else {//don't includeOpposites
						const completedNames = [dimensionStack.pop(), dimensionStack.pop()];
						completedNames.forEach(name => {
							if(!result.includes(opposite(name))){
								result.push(name);
							}
						})	
					}
					axis-= 2;//once to push it back for the layer of the tree we just removed, and once to account for the fact that we pushed past that layer in the conditional above.
				}
				axis = dimensionStack.length && (dimensionStack[dimensionStack.length -1].length)/2; //since each axis is represented by a sign and a letter, the length of the string at the top of the stack divided by 2 is the index of the next axis to include.
				//Note: when the stack is empty, axis is "NaN", but it won't matter, since the loop will never iterate again.
			}
			return result;
		}
	}
	
	const axesGenerator = new BoundaryNameGenerator(2);
	const NUM_AXES = axesGenerator.numAxes;
	const AXES = axesGenerator.axes;
	const EDGE_LABELS = axesGenerator.getBoundaryNames();
	const UNPAIRED_EDGE_LABELS = axesGenerator.getBoundaryNames(false);//no opposites.
	function opposite(edgeLabel){
		return edgeLabel.replace(/[\+-]/g, sign => sign === '+' ? '-' : '+');
	}

	class BoundingBox {
		constructor(botOrBox) {	
			if (botOrBox instanceof BoundingBox){//clone it
				EDGE_LABELS.forEach(edge => {
					this[edge] = Object.assign({}, botOrBox[edge]);
				});
				this.bots = botOrBox.bots ? [...botOrBox.bots] : [];
			} else{//bot
				this.bots = [botOrBox];
				let bot = botOrBox;
				EDGE_LABELS.forEach(edge => {
					const [depAxis, indAxis] = edge.match(/[zyx]/g);
					const [depSign, indSign] = edge.match(/[-+]/g).map(signString => parseInt(signString+1));
					const slope = -(depSign * indSign);//+y+x and -y-x are the NE and SW edges of the rhombus, and both have slopes of -1. Conversely for +y-x and -y+x.
					this[edge] = new BoundaryLine(
						slope, 
						(bot[depAxis] + depSign*bot.r) -(slope*bot[indAxis]), //intercept: e.g.: b = (y ± r) - (mx); where "radius" of the rhombus is the offset for the y value of the point from which we're calculating (bot.x, bot.y±r), so if we are on the "north side/+y", we add r, and vice-versa.
						depAxis, //independent axis
						indAxis  //dependent axis
					);
				});
			}
		}

		/**
		 * @method subdivide
		 * @returns BoundaryBox[] or null if no subdivision possible;
		 */
		subdivide(){
			//we're going to need two lines to divide this region into quarters, with slopes +1 and -1.

			const quarters = Array(4).fill(null).map(el => new BoundingBox(this));
			// let NUM_AXES = 3; //3d testing code.
			// const quarters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];

			//for each axis, create a grouping of two equal halves of the subdivisions.
			for (let grouping = 0; grouping < NUM_AXES; grouping++){
				const groups = [[], []];//create the nested [] to contain the two groups.
				//first, iterate over every element of the subdivision and assign it to a grouping.
				for (let i=0; i<quarters.length; i++){
					//doing (integer division by powers of 2) % 2 sorts into [0, 1], bifurcating the space along each dimension.
					groups[ Math.floor( i/(2**grouping) %2 ) ].push(quarters[i]);
				}
				//do stuff with the just-generated grouping.

				//every grouping shares one bifurcating edge, and the complementary grouping shares the opposite edge. Therefore, there are 2**NUM_AXES subdivisions, this can be accomplished in NUM_AXES bifurcations, which happens to be the number of groupings and /*TODO: check this -> the number of unpaired edges?*/

				const edge = UNPAIRED_EDGE_LABELS[grouping];
				const oppositeEdge = opposite(edge);
				const average = (this[edge].intercept + this[oppositeEdge].intercept)/2;
				if (average === 0){
					//set the intercept to ±1 based on the the dependent axis' sign, as in +y+x or +y-x
					groups[0].forEach(subdivision => subdivision[edge].intercept = -parseInt(edge[0]+1));
					groups[1].forEach(subdivision => subdivision[oppositeEdge].intercept = -parseInt(oppositeEdge[0]+1));
				} else {
					groups[0].forEach(//16 => [1,8] [9, 16]
						subdivision => subdivision[edge].intercept = edge[0] === '+' ? Math.floor(average) : Math.ceil(average)
					);
					groups[1].forEach(
						subdivision => subdivision[oppositeEdge].intercept = oppositeEdge[0] === '+' ? Math.floor(average) : Math.ceil(average)
					);
				}
			}
			if (quarters.every(subdivision => subdivision.equals(this))) return null;
			else return quarters;//containing the subdivisions. (make sure not to include the axes.)
		}
		
		equals(otherBox){
			let hasSameBoundaries = true;
			for (let i = 0; i < EDGE_LABELS.length; i++) {
				const thisEdge = this[EDGE_LABELS[i]];
				const otherEdge = otherBox[EDGE_LABELS[i]];
				if (thisEdge.slope !== otherEdge.slope || thisEdge.intercept !== otherEdge.intercept){//compare the similar edges looking for differences.
					hasSameBoundaries = false;
					break;
				} 
			}
			return hasSameBoundaries;
		}

		getIntersection(otherBox){//intersection of two boundary boxes.
			/* LOGIC: let's consider the two sets of four parallel (-1/+1 slope) lines bounding each 2D region.
				there is only a region of intersection if: the intercept of specific face (i.e., SW in y-x space) is between the two parallel lines in the other box, as in:
				this.SW <= otherBox.SW < this.NE, aka:
				this['-y-x'] <= otherBox['-y-x'] < this['+y+x']
				NOTE: the non-similar parallel face from the other region can't be equal, or the regions abut but do not intersect, thus one side has strict inequalities.
			*/
			if (this === otherBox) {
				console.log ("%csame region given as both parameters.", "background: black", this.bot);
			} else {
				//iterate over all possible combinations of the faces of the regions (signs refer to the direction from the center to the edge)
				const overlapRegion = new BoundingBox(this);
				overlapRegion.bots = [...(this.bots || []), ...(otherBox.bots || [])];//testing code?
				EDGE_LABELS.forEach(edge => {
					//depending on which region's edge is more "central", we'll pick that edge and include it in the overlap region.
					if (edge[0] === '+'){//the sign of the direction of the edge from the center in the dependent axis determines whether to choose the edge with the largest or smallest intercept.
						overlapRegion[edge] = this[edge].intercept < otherBox[edge].intercept ? this[edge] : otherBox[edge]; //if intercepts equal, same line, so region 2's will work.
					} else {//-y
						overlapRegion[edge] = this[edge].intercept > otherBox[edge].intercept ? this[edge] : otherBox[edge];
					}
				});
	
				//now that we've constructed the overlapRegion, let's check whether it's volume is nonzero.
				let nonZero = true;
				//technically, in the case of nonzero overlaps, we'll do double the minimum comparisons by checking face/oppositeFace pairs twice, but checking for those duplicates is slower than brute-forcing 4(2d) or 12(3d) options.
				for (let i=0; i<EDGE_LABELS.length; i++){
					const oppositeFace = opposite(EDGE_LABELS[i]);
	
					if ((EDGE_LABELS[i][0] === '+' && overlapRegion[EDGE_LABELS[i]].intercept <= overlapRegion[oppositeFace].intercept) ||
						(EDGE_LABELS[i][0] === '-' && overlapRegion[EDGE_LABELS[i]].intercept >= overlapRegion[oppositeFace].intercept )){
							nonZero = false;
							break;
						}
				}
				if (nonZero){
					VERBOSE && console.log("%coverlap exists!", "background: darkgreen", overlapRegion);
					// return new BoundingBox(overlapRegion);
					return overlapRegion;
				} else {
					VERBOSE && console.log("%cno overlap found", "background: darkred");
					return null;
				}
				VERBOSE && console.log("overlap object:", overlapRegion);
	
				// if (this['-y-x'].intercept <= otherBox['-y-x'].intercept && otherBox['-y-x'].intercept < this['+y+x'].intercept){
				// 	console.log("%cSW of region 2 intersects region 1", "background: navy");
				// }
			}
		}

		contains(otherBox){
			const intersection = this.getIntersection(otherBox);
			if (intersection && intersection.equals(otherBox)){//intersection can return null, so we need to check. 
				return true;
			} else {
				return false;
			}
		}

		get corners(){
			//each corner represents a positively
		}
	}

	/**
	 * Class representing a quad-tree.
	 */
	class QuadTree{
		/**
		 * Create a QuadTree
		 * @param {BoundingBox} region the region this node represents
		 * @param {BoundingBox[]} containedBotRegions array of regions of bots contained in this subtree.
		 * @param {QuadTree} parent the parent of this node.
		 */
		constructor(region, containedBotRegions, parent){
			//Note: we assume every bot in bots[] is contained by 'space.' If one or more aren't, they will incorrectly "belong" to the root node. Not a huge problem, but it might interfere with some operations on the root node. /*TODO: define intersection functions*/
			this.parent = parent;
			this.region = region;

			//create structures to assign bots to child nodes or this node.
			this.ownBots = [];
			const subdivisions = region.subdivide();// the four subdivided child nodes' regions, stored as an array.
			const botsOfChildren = Array(subdivisions.length).fill(null).map(x => []);//an array of subarrays representing the bots contained in each child node.

		// console.log("this:", region, "children:", subdivisions);
			//iterate over the containedBots and assign them to potential child regions.
			containedBotRegions.forEach(bot => {
				let containedByChild = false;
				for(let i=0; i<subdivisions.length; i++){
					if(subdivisions[i].contains(bot)){
						botsOfChildren[i].push(bot);
						containedByChild = true;
						break;
					}
				}
				if (!containedByChild){
					this.ownBots.push(bot);
				}
			});
		// console.log("botsOfChildren:", botsOfChildren);

			//create child nodes as appropriate.
			this.children = [];
			for (let i=0; i<subdivisions.length; i++){//for each potential child region...
				//if a child region would contain two bots, then there is potential for an intersection in that child.
				//if a child region contains 0 or 1 bots, then there is no potential for an intersection in that child. Don't include it in the tree. The "intersection" resulting from an analysis of a 1-bot node would be the bot's region itself, which would then be tested for intersection with the ownBots of this node, so a 1-child node would increase both space and time complexity.
				if (botsOfChildren[i].length > 1){
					this.children.push(new QuadTree(subdivisions[i], botsOfChildren[i], this));
				} else {
					this.ownBots.push(...botsOfChildren[i]);//whether 1 or 0 children in the non-node region, spread/push will work.
				}
			}
		}

		findGreatestOverlapRegion(){
		/*
			// For every node, there are only five sets whose self-intersections have to be checked:
			// {ownbots}, {ownbots + child1} ... {ownbots +child4}
			let greatestOverlapRegion = null;
			let regionsToTest = [...this.bots];
			//find the greatest overlap region from each of this.children and add them to regionsToTest[]
			//find the greatest overlap between this.bots[];
			if (regionsToTest.length){//there is overlap only if there are bots.
				if (regionsToTest.length === 1){//if there is only one bot, the overlap IS that bot.
					greatestOverlapRegion = regionsToTest[0];
				} else {//actually have to calculate the intersections. O(n^2);
					for (let region1 = 1;)
					regionsToTest.forEach(region1 => regionsToTest.forEach(region2 => getIntersection(region1, region2)));
				}
			}

			return greatestOverlapRegion;
		*/
		}
	}

	const regions = nanobots.map(bot => new BoundingBox(bot));
	console.log("regions", regions);

	const crossesOrigin = nanobots.map(
		bot => Math.abs(bot.x) - bot.r < 1 &&
				Math.abs(bot.y) - bot.r < 1 &&
				Math.abs(bot.z) - bot.r < 1
	);
	console.log("#regions cross origin:", crossesOrigin.reduce((sum, el) => sum + (el ? 1: 0), 0), "out of total:", nanobots.length);

	const farthestIntercept = regions.reduce(
		(farthestIntercept, region) => Math.max(
			Math.abs(region['+y+x'].intercept),
			Math.abs(region['-y-x'].intercept),
			Math.abs(region['+y-x'].intercept),
			Math.abs(region['-y+x'].intercept),
			farthestIntercept
		)
		, 0
	);
	console.log("farthest Intercept", farthestIntercept);
	const world = new BoundingBox({x:0, y:0, z:0, r: 2**Math.ceil(Math.log2(farthestIntercept))});
	world.bots = [];//don't want a dead bot at the origin.
	console.log("world", world, "instanceof", world instanceof BoundingBox);

	const myQuadTree = new QuadTree(world, regions, null);
	console.log("my Quad tree", myQuadTree);

/*  TESTING CODE
	//testing contains()
	console.log("world contains all bot regions?", regions.map(region => world.contains(region)));
	console.log(regions[2].bots[0],"contains", regions[1].bots[0],"?", regions[2].contains(regions[1]));
	console.log(regions[2].bots[0],"contains", regions[0].bots[0],"?", regions[2].contains(regions[0]));

	//testing BoundingBox.subdivide();
	const gaia = world.subdivide();
	console.log("gaia", gaia, "elements are Bounding Box:", gaia[0] instanceof BoundingBox);

	//testing terminal subdivision
	const subGaia = gaia[0].subdivide();
	console.log("gaia[0] subdivisions", subGaia);
	const subSubGaia = subGaia[0].subdivide();
	console.log("subgaia[0] subdivisions", subSubGaia);
	const subSubSubGaia = subSubGaia[0].subdivide();
	console.log("subgaia[0] subdivisions", subSubSubGaia);
	const subSubSubSubGaia = subSubSubGaia[0].subdivide();
	console.log("subgaia[0] subdivisions", subSubSubSubGaia);
	const subSubSubSubSubGaia = subSubSubSubGaia[0].subdivide();
	console.log("subgaia[0] subdivisions", subSubSubSubSubGaia);

	//testing BoundaryNameGenerator.
	console.log("axes", axesGenerator.axes);
	console.log("boundary names", axesGenerator.getBoundaryNames());
	console.log("edge names:", EDGE_LABELS, "unpaired edge names:", UNPAIRED_EDGE_LABELS);

	//testing BoundingBox.equals();
	const anotherWorld = new BoundingBox(world);
	console.log("another world:", anotherWorld);
	console.log("world === anotherWorld?", world === anotherWorld, "both are BoundingBoxes?", world instanceof BoundingBox && anotherWorld instanceof BoundingBox, "boundaries the same?", world.equals(anotherWorld));
	console.log("gaia[0] equals gaia[1]?", gaia[0].equals(gaia[1]));
*/

	/*TODO: Contains() fxn, similar to intersects, but for bounding box. same bounds => contained.*/
	// regions.forEach(region1 => regions.forEach(region2 => getIntersection(region1, region2)));

	return "tbd";
}

day23();