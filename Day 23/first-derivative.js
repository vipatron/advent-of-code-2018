import { getInput } from '../helpers/helpers.js'
console.clear();

const VERBOSE = false;
const MODERATE = false || VERBOSE;
const TERSE = true || MODERATE;

const INPUT = getInput(23);

// const INPUT = "pos=<0,0,0>, r=4\npos=<1,0,0>, r=1\npos=<4,0,0>, r=3\npos=<0,2,0>, r=1\npos=<0,5,0>, r=3\npos=<0,0,3>, r=1\npos=<1,1,1>, r=1\npos=<1,1,2>, r=1\npos=<1,3,1>, r=1";

//sample part 2:
// const INPUT = "pos=<10,12,12>, r=2\npos=<12,14,12>, r=2\npos=<16,12,12>, r=4\npos=<14,14,14>, r=6\npos=<50,50,50>, r=200\npos=<10,10,10>, r=5";

//no outlier sample:
// const INPUT = "pos=<10,12,12>, r=2\npos=<12,14,12>, r=2\npos=<16,12,12>, r=4\npos=<14,14,14>, r=6\npos=<10,10,10>, r=5";
// const INPUT = "pos=<10,12,12>, r=2\npos=<12,14,12>, r=2\npos=<16,12,12>, r=4";
// const INPUT = "pos=<10,12,12>, r=2\npos=<12,17,12>, r=2\npos=<16,12,12>, r=4";

const day23 = () => {
	const tokens = INPUT.trim().split("\n");

	const puzzle1result = part1(tokens);
	const puzzle2result = part2(tokens);
	
	console.log("puzzle1: ", puzzle1result);
	console.log("puzzle2: ", puzzle2result);
}

const manhattanDistance = (bot1, bot2) =>
	Math.abs(bot1.x - bot2.x) + 
	Math.abs(bot1.y - bot2.y) + 
	Math.abs(bot1.z - bot2.z);

const TwoDManhattanDistance = (bot1, bot2) => Math.abs(bot1.x - bot2.x) + Math.abs(bot1.y - bot2.y);

function part1(lines){
	const nanobots = lines.map(
		line => {
			const [x,y,z,r] = line.match(/-?\d+/g).map(num => parseInt(num));
			return {x,y,z,r};
		}
	);
	// console.log("nanobots[]", nanobots);
	const strongest = nanobots.reduce(
		(strongest, bot) => bot.r > strongest.r ? bot : strongest
	);
	// console.log("strongest", strongest);
	
	
	// console.log("self distance:", manhattanDistance(strongest));
	const inRange = nanobots.filter(
		bot => manhattanDistance(bot, strongest) <= strongest.r
	);
	// nanobots.forEach(
		// bot => console.log(`The nanobot at (${bot.x},${bot.y},${bot.z}) is distance ${manhattanDistance(bot)} away and so it is ${manhattanDistance(bot) <= strongest.r ? "IN RANGE" : "NOT in range"}.`)
	// );

	return inRange.length;
}

function part2(lines){
	const nanobots = lines.map(
		line => {
			const [x,y,z,r] = line.match(/-?\d+/g).map(num => parseInt(num));
			return {x,y,z,r};
		}
	);
	
	nanobots.sort((a,b) => a.r - b.r);

	const radiusRange = [nanobots[0], nanobots[nanobots.length -1]].map(bot => `(${bot.x}, ${bot.y}, ${bot.z}) r:${bot.r}`);
	console.log("range for the radii:", radiusRange);
	
	let noDuplicates = true;
	for (let i=0; i<nanobots.length; i++){
		for (let j=i+1; j<nanobots.length; j++){
			if (//duplicated point
				nanobots[i].x === nanobots[j].x &&
				nanobots[i].y === nanobots[j].y &&
				nanobots[i].z === nanobots[j].z
			){
				noDuplicates = false;
				break;
			}
		}
		if (!noDuplicates) break;
	}
	console.log("There are no duplicated points:", noDuplicates);

	
	const gridRange = nanobots.reduce(
		(extents, bot) => ({
			xMin: Math.min(bot.x, extents.xMin),
			xMax: Math.max(bot.x, extents.xMax),
			yMin: Math.min(bot.y, extents.yMin),
			yMax: Math.max(bot.y, extents.yMax),
			zMin: Math.min(bot.z, extents.zMin),
			zMax: Math.max(bot.z, extents.zMax)
		}),
		{
			xMin: Infinity,
			xMax: Number.NEGATIVE_INFINITY,
			yMin: Infinity,
			yMax: Number.NEGATIVE_INFINITY,
			zMin: Infinity,
			zMax: Number.NEGATIVE_INFINITY
		}
	);
	console.log("extents", gridRange);
	console.log("xWidth:", gridRange.xMax - gridRange.xMin, Math.log2(gridRange.xMax - gridRange.xMin));
	console.log("yWidth:", gridRange.yMax - gridRange.yMin, Math.log2(gridRange.yMax - gridRange.yMin));
	console.log("zWidth:", gridRange.zMax - gridRange.zMin, Math.log2(gridRange.zMax - gridRange.zMin));
	const biggestHalfAxis = Object.values(gridRange).reduce((max, elem) => Math.max(max, elem));
	console.log("largest half-axis:", biggestHalfAxis, "log2:", Math.log2(biggestHalfAxis), "cube edge size:", 2*Math.ceil(Math.log2(biggestHalfAxis)));

	const numBotsIn2DRange = point => nanobots.map(bot => TwoDManhattanDistance(bot, point) <= bot.r).reduce((sum, elem) => sum + (elem ? 1 : 0), 0)

	if (MODERATE){//print the formatted grid.
		const grid = Array(gridRange.yMax - gridRange.yMin + 1).fill(null).map(row => Array(gridRange.xMax - gridRange.xMin + 1).fill(0));
		console.log(`(${gridRange.xMin}, ${gridRange.yMin}) to (${gridRange.xMax}, ${gridRange.yMax})`);
		for (let y=0; y<grid.length; y++){
			for(let x=0; x<grid[y].length; x++){
				const point = {x: x+gridRange.xMin, y: y+gridRange.yMin};
				grid[y][x] = numBotsIn2DRange(point);
			}
		}
		
		nanobots.forEach(bot => {
			const y = bot.y-gridRange.yMin;
			const x = bot.x-gridRange.xMin;
			grid[y][x] = 'X';
		});
		let formattingStrings = [];
		let gridString = grid.reduce(
			(str, row) => str + row.join("") + "\n", ""
		).replace(
			/X/g, match => formattingStrings.push("background:turquoise", "background: default") && "%c"+match+"%c"
		);
		console.log(gridString, ...formattingStrings);			
	}
// return;
	const histoDeltas = {//we'll need a delta object for each axis.
		x: {},
		y: {},
		z: {}
	};
	const maxima = {//{height: [[rangeStart, rangeEnd], ...]}
		x: {},
		y: {},
		z: {}
	};

	['x', 'y', 'z'].forEach( axis => {//summarize the axis's local maxima and their ranges in slice indices.
		console.log("%cSTARTING AXIS: "+axis, "background: teal");
		nanobots.forEach( bot => {
			//figure out the start and end slices for each bot.
			const startSlice = bot[axis] - bot.r;
			const endSlice = bot[axis] + bot.r + 1;//+1 accounts for the voxel on which it ends. These are "slices"
			VERBOSE && console.log(`(${bot.x}, ${bot.y}, ${bot.z}) start:${startSlice} end: ${endSlice}`)
	
			//increment/decrement the delta at each slice value.
			if (! histoDeltas[axis][startSlice]) histoDeltas[axis][startSlice]=0;
			histoDeltas[axis][startSlice]++;
			if (! histoDeltas[axis][endSlice]) histoDeltas[axis][endSlice]=0;
			histoDeltas[axis][endSlice]--;
		})
		//acquire the précis of this axis' histogram. [[slice, dHeight], ...]
		//Note: start/endSlice above are numbers, but they get cast to strings when used as keys in the histoDeltas{} object. Thus, they need to be recast to numbers in order to be sorted.
		const axisHistoDescription = Object.entries(histoDeltas[axis]).map(entry => [parseInt(entry[0]), entry[1]]).sort((a,b) => a[0] - b[0]);
		VERBOSE && console.log("histo précis for", axis,":", axisHistoDescription);
		// console.log("signs of précis:", axisHistoDescription.map(delta => [delta[1], Math.sign(delta[1])]));

		//loop over the précis, keeping track of the new height of the histogram.
		let histoHeight = 0;
		for (let i=0; i<axisHistoDescription.length-1; i++){
			histoHeight += axisHistoDescription[i][1];
			//if dHeight changes direction between this slice and the next, there is a maximum between them.
			if (Math.sign(axisHistoDescription[i][1]) === 1 &&
				Math.sign(axisHistoDescription[i+1][1]) === -1){
				
				VERBOSE && console.log("%cfound maximum beginning at "+i, "background: darkred");

				//if this height isn't in the list of maxima, make space for its ranges
				//push a [startSlice, endSlice] array representing this array onto the key for this height.
				if (!maxima[axis][histoHeight]) maxima[axis][histoHeight] = [];
				maxima[axis][histoHeight].push([axisHistoDescription[i][0], axisHistoDescription[i+1][0]]);
				i++;//because we already know [i+1]'s sign.
			}
		}
	});
	TERSE && console.log("histoDeltaSlices:", histoDeltas);
	// let numPointsToCheck = 1;
	// ['x', 'y', 'z'].forEach(axis =>{
	// 	let maximaValues = Object.keys(maxima[axis]);//keys for the array of range arrays
	// 	console.log(axis, "axis #delta slices:", maximaValues.length);
	// 	let axisPointsToCheck = [];//midpoints of the 
	// 	maximaValues.forEach(maximumValue => {
	// 		axi
	// 	})
	// });
	TERSE && console.log("%clocal maxima:", "background: darkgreen", maxima);
	
	const peakRanges = { x:{}, y:{}, z:{} }; //global maximum for each axis.
	let numPixelsToInvestigate = 1;
	for (let axis in maxima){// calculate the globalMaximum for that axis and slice ranges that describe it.
		const localMaxima = Object.keys(maxima[axis]); //get the heights of the local maxima.
		// console.log ("maxima for", axis, ":", ...localMaxima);

		const globalMaximum = localMaxima.sort((a,b) => b-a)[0];//sort heights in descending order, pick biggest.
		TERSE && console.log("global maximum for", axis, "axis:", globalMaximum);
		
		peakRanges[axis] = maxima[axis][globalMaximum];
		TERSE && console.log("peaks for", axis, peakRanges[axis]);

		//testing code to see how much problem domain reduction we can get by just diminishing the area.
		const widthOfPeaks = peakRanges[axis].reduce((sum, range) => sum + range[1] - range[0], 0);
		console.log("peakWidth", widthOfPeaks);
		numPixelsToInvestigate *= widthOfPeaks;


	}
	//more of the domain reduction testing code.
	const originalVolume = (gridRange.xMax - gridRange.xMin) * (gridRange.yMax - gridRange.yMin) * (gridRange.zMax - gridRange.zMin);
	console.log("pixels to explore", numPixelsToInvestigate, "out of", originalVolume, "percentage:", 100*numPixelsToInvestigate/originalVolume );

	//real code: we only need to check the minimum coordinates in each range.
	console.log("global maxima all axes:", peakRanges);
	const cornerPoints = [];
	for (let xRangeIndex = 0; xRangeIndex < peakRanges.x.length; xRangeIndex++){
		for (let yRangeIndex = 0; yRangeIndex < peakRanges.y.length; yRangeIndex++){
			for (let zRangeIndex = 0; zRangeIndex < peakRanges.z.length; zRangeIndex++){
				cornerPoints.push({
					x: peakRanges.x[xRangeIndex][0],//.axis[current range][startSlice]
					y: peakRanges.y[yRangeIndex][0],
					z: peakRanges.z[zRangeIndex][0]
				});
			}
		}
	}

/*
	console.log("corner points", cornerPoints);
	const distancesToCorners = cornerPoints.map(point => manhattanDistance({x:0, y:0, z:0}, point));
	console.log("distances", distancesToCorners);

	//real code: we only need to check the CENTRAL COORDINATES
	console.log("global maxima all axes:", peakRanges);
	const midPoints = [];
	for (let xRangeIndex = 0; xRangeIndex < peakRanges.x.length; xRangeIndex++){
		for (let yRangeIndex = 0; yRangeIndex < peakRanges.y.length; yRangeIndex++){
			for (let zRangeIndex = 0; zRangeIndex < peakRanges.z.length; zRangeIndex++){
				midPoints.push({
					x: Math.floor((peakRanges.x[xRangeIndex][0] + peakRanges.x[xRangeIndex][1])/2),//.axis[current range][startSlice]
					y: Math.floor((peakRanges.y[yRangeIndex][0] + peakRanges.y[yRangeIndex][1])/2),
					z: Math.floor((peakRanges.z[zRangeIndex][0] + peakRanges.z[zRangeIndex][1])/2)
				});
			}
		}
	}
	console.log("mid points", midPoints);
	const inRangeOfMidpoints = midPoints.map(point => 
		nanobots.map(
			bot => manhattanDistance(bot, point) <= bot.r
		).reduce(
			(sum, elem) => sum + (elem ? 1 : 0), 0
		)
	);
	console.log("#bots in range of midpoints:", inRangeOfMidpoints);
	const distancesToMidpoints = midPoints.map(point => manhattanDistance({x:0, y:0, z:0}, point));
	console.log("distances", distancesToMidpoints);
	

	const closestCorner = {};
	for (let axis in peakRanges){
		for (let rangeIndex = 0; rangeIndex < peakRanges[axis].length; rangeIndex++){
			closestCorner[axis] = peakRanges[axis][0][0];//[axis][lowest range][startSlice]
		}
	}
	console.log(closestCorner);
	
	const numBotsInRange = point => nanobots.map(bot => manhattanDistance(bot, point) <= bot.r).reduce((sum, elem) => sum + (elem ? 1 : 0), 0)

	const cubeEdge = 10;
	const cube = {};
	for (let x=0; x <cubeEdge; x++){
		const xSquare = cube[x] || (cube[x] = {});
		for (let y=0; y <cubeEdge; y++){
			const yLine = xSquare[y] || (xSquare[y] = {});
			for (let z=0; z <cubeEdge; z++){
				const point = {
					x: midPoints[0].x - x,//closestCorner.x + x,
					y: midPoints[0].y - y,
					z: midPoints[0].z - z
				}
				// yLine[z] = point;
				yLine[z] = numBotsInRange(point);
			}		
		}			
	}
	console.log("cube of size", cubeEdge+":", cube);


	const botsInRange = numBotsInRange(closestCorner);
	console.log("# bots in range of closest corner:", botsInRange);

	//move outward from the midpoint with the bigger #in range.
	let center = midPoints[0];
	const inRangeOfCenter = inRangeOfMidpoints[0];
	['x', 'y', 'z'].forEach( axis => {
		let less = Object.assign({}, center);
		while (numBotsInRange(less) === inRangeOfCenter) less[axis]--;
		console.log("num bots in range of -side pt:", less, numBotsInRange(less));
		let more = Object.assign({}, center);
		while (numBotsInRange(more) === inRangeOfCenter) more[axis]++;
		console.log("num bots in range of +side pt:", more, numBotsInRange(more));
	});
*/

	return "tbd";

	// return originalVolume;
}

day23();