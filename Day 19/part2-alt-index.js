const altProgram = registers => {
	let arr = [1, 2, 4, 7, 8, 14, 16, 28, 32, 56, 64, 112, 128, 224, 448, 896];
	console.log("sum(factors 896):", arr.reduce((a,b) => a+b));
	
	let a, c, d, e, f;
	c = d = e = f = 0;
	a = 1;
	f = f + 2;
	f = f * f;
	f = 19 * f;
	f = 11 * f;
	e = e + 2;
	e = e * 22;
	e = e + 16;
	f = f + e;
	if (a==1){
		e = 27;
		e *= 28;
		e += 29;
		e *= 30;
		e *= 14;
		e *= 32;
		f = f + e;
		a = 0;
	}
	console.log("after initialization");
	console.log("a:", a, "c:", c, "d:", d, "e:", e, "f:", f);
	c = 1;
	let sqrtF = Math.sqrt(f);
	do {
		if (f % c === 0){
			a += c + f/c;
		}
		++c;
	} while (c <= sqrtF);
	console.log("a:", a);
	return a;
};
