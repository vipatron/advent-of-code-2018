// import { getInput } from '../helpers/helpers.js'
console.clear();

// const INPUT = getInput(19);
const INPUT = "#ip 1\naddi 1 16 1\nseti 1 4 2\nseti 1 0 3\nmulr 2 3 4\neqrr 4 5 4\naddr 4 1 1\naddi 1 1 1\naddr 2 0 0\naddi 3 1 3\ngtrr 3 5 4\naddr 1 4 1\nseti 2 4 1\naddi 2 1 2\ngtrr 2 5 4\naddr 4 1 1\nseti 1 1 1\nmulr 1 1 1\naddi 5 2 5\nmulr 5 5 5\nmulr 1 5 5\nmuli 5 11 5\naddi 4 2 4\nmulr 4 1 4\naddi 4 16 4\naddr 5 4 5\naddr 1 0 1\nseti 0 7 1\nsetr 1 5 4\nmulr 4 1 4\naddr 1 4 4\nmulr 1 4 4\nmuli 4 14 4\nmulr 4 1 4\naddr 5 4 5\nseti 0 9 0\nseti 0 4 1\n";

// const INPUT = "#ip 0\nseti 5 0 1\nseti 6 0 2\naddi 0 1 0\naddr 1 2 3\nsetr 1 0 0\nseti 8 0 4\nseti 9 0 5";

const day19 = () => {
	const tokens = INPUT.trim().split("\n");
	// console.log(tokens);
	const IP_Index = tokens.shift().match(/\d+/)[0];
	console.log("IP Index: ", IP_Index);
	const program = tokens.map(line => line.split(" ").map((x, i) => i === 0 ? x : parseInt(x)));
	// console.log(program);
	// console.log("=================");

	console.time("puzzle1");
	const puzzle1result = 2040;
	// const puzzle1result = executeProgram([0, 0, 0, 0, 0, 0], IP_Index, program);
	console.timeEnd("puzzle1");
	console.log("%c puzzle1: ", "background: green",puzzle1result);
	
	console.log("%cParsed Program:", "background: yellow; color:black");
	console.log(parseProgram(program));
	console.log("%c===================", "background: yellow; color: black");
	console.time("puzzle2");
	// const puzzle2result = "tbd";
	const puzzle2result = altProgram([1,0,0,0,0,0]);
	// const puzzle2result = executeProgram([1, 0, 0, 0, 0, 0], IP_Index, program);	
	console.timeEnd("puzzle2");
	console.log("puzzle2: ", puzzle2result);
}

const executeProgram = (registers, IP_Index, program) => {
	let IP = registers[IP_Index];
	let counter = 0;
	let instructionCalls = Array(program.length).fill(0);
	while (counter < 10000000 && IP > -1 && IP < program.length){
	// while (counter < 10 && IP > -1 && IP < program.length){
		registers[IP_Index] = IP;
		const instruction = program[IP];
		instructionCalls[IP]++;
		const after = OPERATIONS[instruction[0]](instruction, registers);
		(counter < 100 || counter % 1000000 === 0) && console.log(`cnt: ${counter/1000000}M ip=${IP} [${registers.join(", ")}] ${instruction.join(" ")} [${after.join(", ")}]`)
		registers = after;
		IP = registers[IP_Index] + 1;
		counter++;
		// (counter < 100 || counter % 1000000 === 0) && console.log(`loop:${counter} calls: [${instructionCalls.join(" ")}]`)
	}
	console.log("counter:", counter);
	return registers[0];
}

const parseProgram = program => {
	let result = program.map((line, i) => {
		let lineResult;
		lineResult = cmdString(line) || `${line.join(" ")}`;
		if (lineResult.includes("b =")){
			if (!/[acdef]/.test(lineResult)){
				let expression = lineResult.match(/b =(.*)/)[1].replace(/b/g, i);
				let value = eval(expression);
				// window.alert("("+value+")")
				lineResult = "GOTO " + (value + 1);//changed to point to correct line.
			} else {
				lineResult = lineResult.replace("b =", "GOTO").replace("b", i+1); //need the +1 to GOTO the correct line.
			}
		} else {
			lineResult = lineResult.replace("b", i); //we know the value of the instruction pointer at any line.
		}
		return `[${i}]: ${lineResult}`;
	});
	/** RESUME HERE:
	TODO: gt or eq BEFORE A GOTO is an IF/ELSE STATEMENT WITH TWO POSSIBLE GOTO OUTCOMES.
	TODO: summarize the nested operations on a variable? (Not strictly necessary).
	*/
	return "\n"+result.join("\n");
}

const cmdString = line => {
	switch (line[0]){
		case 'setr':
			return `${varName(line[3])} = ${varName(line[1])}`;
		case 'seti':
			return `${varName(line[3])} = ${line[1]}`;
		case 'addr':
			return `${varName(line[3])} = ${varName(line[1])} + ${varName(line[2])}`;
		case 'addi':
			return `${varName(line[3])} = ${varName(line[1])} + ${line[2]}`;
		case 'mulr':
			return `${varName(line[3])} = ${varName(line[1])} * ${varName(line[2])}`;
		case 'muli':
			return `${varName(line[3])} = ${varName(line[1])} * ${line[2]}`;
		case 'gtrr':
			return `(${varName(line[1])} > ${varName(line[2])}) ? -> ${varName(line[3])}`;
		case 'eqrr':
			return `(${varName(line[1])} == ${varName(line[2])}) ? -> ${varName(line[3])}`;
		default:
			return undefined;
	}
}

const varName = registerNumber => String.fromCharCode(97 + registerNumber);

const OPERATIONS = {
	addr: ([, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A] + before[B];
		return after;
	},
	addi: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A] + B;
		return after;
	},
	mulr: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A] * before [B];
		return after;
	},
	muli: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A] * B;
		return after;
	},
	banr: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A] & before [B];
		return after;
	},
	bani: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A] & B;
		return after;
	},
	borr: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A] | before [B];
		return after;
	},
	bori: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A] | B;
		return after;
	},
	setr: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A];
		return after;
	},
	seti: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = A;
		return after;
	},
	gtir: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = A > before [B] ? 1 : 0;
		return after;
	},
	gtri: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A] > B ? 1 : 0;
		return after;
	},
	gtrr: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A] > before [B] ? 1 : 0;
		return after;
	},
	eqir: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = A == before [B] ? 1 : 0;
		return after;
	},
	eqri: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A] == B ? 1 : 0;
		return after;
	},
	eqrr: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A] == before [B] ? 1 : 0;
		return after;
	}
};

const altProgram = registers => {
	let arr = [1, 2, 4, 7, 8, 14, 16, 28, 32, 56, 64, 112, 128, 224, 448, 896];
	console.log("sum(factors 896):", arr.reduce((a,b) => a+b));
	
	let [a, b, c, d, e, f] = registers;
	// c = d = e = f = 0;
	// a = 1;
	f = f + 2;
	f = f * f;
	f = 19 * f;
	f = 11 * f;
	e = e + 2;
	e = e * 22;
	e = e + 16;
	f = f + e;
	if (a==1){
		e = 27;
		e *= 28;
		e += 29;
		e *= 30;
		e *= 14;
		e *= 32;
		f = f + e;
		a = 0;
	}
	console.log("after initialization");
	console.log("a:", a, "c:", c, "d:", d, "e:", e, "f:", f);
	c = 1;
	let sqrtF = Math.sqrt(f);
	do {
		if (f % c === 0){
			a += c + f/c;
		}
		++c;
	} while (c <= sqrtF);
	console.log("a:", a);
	return a;
};

day19();