console.clear();

// const INPUT = "Step G must be finished before step S can begin.\nStep T must be finished before step Q can begin.\nStep A must be finished before step B can begin.\nStep H must be finished before step X can begin.\nStep V must be finished before step O can begin.\nStep Z must be finished before step P can begin.\nStep R must be finished before step J can begin.\nStep L must be finished before step Y can begin.\nStep Y must be finished before step E can begin.\nStep W must be finished before step X can begin.\nStep X must be finished before step B can begin.\nStep K must be finished before step E can begin.\nStep Q must be finished before step P can begin.\nStep U must be finished before step B can begin.\nStep M must be finished before step O can begin.\nStep P must be finished before step N can begin.\nStep I must be finished before step J can begin.\nStep B must be finished before step C can begin.\nStep C must be finished before step O can begin.\nStep J must be finished before step F can begin.\nStep F must be finished before step O can begin.\nStep E must be finished before step D can begin.\nStep D must be finished before step N can begin.\nStep N must be finished before step S can begin.\nStep S must be finished before step O can begin.\nStep W must be finished before step O can begin.\nStep L must be finished before step P can begin.\nStep N must be finished before step O can begin.\nStep T must be finished before step D can begin.\nStep G must be finished before step I can begin.\nStep V must be finished before step X can begin.\nStep B must be finished before step N can begin.\nStep R must be finished before step N can begin.\nStep H must be finished before step J can begin.\nStep B must be finished before step S can begin.\nStep P must be finished before step I can begin.\nStep A must be finished before step J can begin.\nStep A must be finished before step U can begin.\nStep B must be finished before step D can begin.\nStep T must be finished before step A can begin.\nStep U must be finished before step D can begin.\nStep T must be finished before step L can begin.\nStep I must be finished before step E can begin.\nStep R must be finished before step U can begin.\nStep H must be finished before step S can begin.\nStep P must be finished before step F can begin.\nStep Q must be finished before step C can begin.\nStep A must be finished before step P can begin.\nStep X must be finished before step E can begin.\nStep Q must be finished before step N can begin.\nStep E must be finished before step N can begin.\nStep Q must be finished before step O can begin.\nStep J must be finished before step S can begin.\nStep X must be finished before step P can begin.\nStep K must be finished before step U can begin.\nStep F must be finished before step E can begin.\nStep C must be finished before step E can begin.\nStep H must be finished before step K can begin.\nStep W must be finished before step B can begin.\nStep G must be finished before step O can begin.\nStep F must be finished before step N can begin.\nStep I must be finished before step D can begin.\nStep G must be finished before step V can begin.\nStep E must be finished before step S can begin.\nStep Y must be finished before step P can begin.\nStep G must be finished before step E can begin.\nStep P must be finished before step J can begin.\nStep U must be finished before step N can begin.\nStep U must be finished before step F can begin.\nStep X must be finished before step U can begin.\nStep X must be finished before step C can begin.\nStep R must be finished before step Q can begin.\nStep Q must be finished before step E can begin.\nStep Z must be finished before step E can begin.\nStep X must be finished before step F can begin.\nStep J must be finished before step D can begin.\nStep X must be finished before step M can begin.\nStep Y must be finished before step D can begin.\nStep K must be finished before step J can begin.\nStep Z must be finished before step J can begin.\nStep M must be finished before step P can begin.\nStep T must be finished before step M can begin.\nStep F must be finished before step S can begin.\nStep P must be finished before step S can begin.\nStep X must be finished before step I can begin.\nStep U must be finished before step J can begin.\nStep M must be finished before step B can begin.\nStep Q must be finished before step D can begin.\nStep Z must be finished before step I can begin.\nStep D must be finished before step S can begin.\nStep J must be finished before step N can begin.\nStep D must be finished before step O can begin.\nStep T must be finished before step H can begin.\nStep P must be finished before step D can begin.\nStep M must be finished before step F can begin.\nStep Y must be finished before step S can begin.\nStep H must be finished before step I can begin.\nStep Y must be finished before step W can begin.\nStep X must be finished before step J can begin.\nStep L must be finished before step W can begin.\nStep G must be finished before step N can begin.";
const INPUT = "Step C must be finished before step A can begin.\nStep C must be finished before step F can begin.\nStep A must be finished before step B can begin.\nStep A must be finished before step D can begin.\nStep B must be finished before step E can begin.\nStep D must be finished before step E can begin.\nStep F must be finished before step E can begin.";

const duration = chr => chr.charCodeAt() - 64; // A == 65. (for full minute, chr-4)

const addStepToGraph = (step, graph) => { //step is a string, graph is {}
	graph[step] = {
		comesAfter: [],
		comesBefore: []
	};
};

const deleteStepFromGraph = (step, graph) => {
	graph[step].comesBefore.forEach(
		subsequent => graph[subsequent].comesAfter.splice(graph[subsequent].comesAfter.indexOf(step), 1)
	);
	delete graph[step];	
};


const createConstraintGraph = (constraints) => {
	const constraintGraph = {};

	constraints.forEach((constraint) => {
		if (!constraintGraph.hasOwnProperty(constraint.antecedent)){
			addStepToGraph(constraint.antecedent, constraintGraph);
		}
		constraintGraph[constraint.antecedent].comesBefore.push(constraint.subsequent);
		
		if (!constraintGraph.hasOwnProperty(constraint.subsequent)){
			addStepToGraph(constraint.subsequent, constraintGraph);
		}
		constraintGraph[constraint.subsequent].comesAfter.push(constraint.antecedent);
	});

	return constraintGraph;
};

const getAvailableSteps = graph => Object.keys(graph).filter(step => graph[step].comesAfter.length === 0 ).sort();


const day7 = () => {
	const constraints = INPUT.split("\n").map(
		str => ({
			antecedent: str.match(/^Step (\w)/)[1],
			subsequent: str.match(/step (\w)/)[1]
		})
	);
	// console.log(constraints);
	
	let constraintGraph = createConstraintGraph(constraints);
	
	let traversalPath = [];
	let availableSteps;
	while ((availableSteps = getAvailableSteps(constraintGraph)).length){
		// console.log("steps:", availableSteps);

		let step = availableSteps[0];
		traversalPath.push(step);
		// console.log("path so far:", traversalPath);
		
		deleteStepFromGraph(step, constraintGraph);
	}
	const puzzle1result = traversalPath.join("");
	
	/* PUZZLE 2 STARTS HERE */

	//reset the input and output
	constraintGraph = createConstraintGraph(constraints);
	traversalPath = []; //only reusing this for testing.
			
	// create an array of workers.
	const NUM_WORKERS = 2;
	const workers = new Array(NUM_WORKERS).fill(null).map(x => ({timeFree: 0, step: "."}));
	console.log(workers);
	
	//create a variable to keep track of "current" time (updated every time a step is removed from a worker).
	let time = 0;

	console.log("Second\tWorkr 1\tWorkr2\tDone");
		console.log(`${time}\t${workers[0].step}\t${workers[1].step}\t${traversalPath.join('')}`);
	// while there are more steps available to take...
		let counter = 0;
	while ((availableSteps = getAvailableSteps(constraintGraph)).length && counter < 10){		

		console.log("avail:", availableSteps);
		//this algorithm requires that steps not be removed until no worker is processing them. Which means that availableSteps[] is a superset of workers[].step

		if (counter){
			availableSteps = availableSteps.filter(step => {
					console.log("step:", step);
					console.log("workers", workers);
					console.log("find index", workers.findIndex(worker => worker.step === step));
					return (workers.findIndex(worker => worker.step === step) > -1)
				}
			);
		}
	console.log("inner while # times run:", ++counter);

		console.log("filteredAvail:", availableSteps);
		
		let nextFree;
		while (workers.some(worker => worker.timeFree === time && availableSteps.length)){
			nextFree = workers.findIndex(worker => worker.timeFree === time);
			console.log("time", time, "nextFree", nextFree, workers[nextFree]);

			/* for the worker who will be free soonest... (workers[nextFree]); */
			// take that worker's current step and move it onto the traversalPath and deleteStepFromGraph.
			traversalPath.push(workers[nextFree].step) // will have to filter out "."'s	

			let nextStepToProcess = availableSteps.shift();
			if (workers[nextFree].step !== "."){
				deleteStepFromGraph(workers[nextFree].step, constraintGraph);
			}

			// set worker.timeFree to:
				//if current step == ".": time + duration(new step) //time's value will be the last one 
				//if current step != ".": timeFree + duration(new step)
			workers[nextFree].timeFree = duration(nextStepToProcess) + (workers[nextFree].step === "." ? time : workers[nextFree].timeFree);
			// put the next availableStep onto the worker
			workers[nextFree].step = nextStepToProcess;
		}
		console.log("out of while");
		// set time to lowest value for timeFree
		time = workers.filter(worker => worker.timeFree !== 0).
			reduce((min, worker) => Math.min(worker.timeFree, min), Infinity);
		workers.forEach(worker => {if (worker.timeFree<time){ worker.timeFree = time}});
		console.log(`${time}\t${workers[0].step+workers[0].timeFree}\t${workers[1].step+workers[1].timeFree}\t${traversalPath.join('')}`);
	}
	console.log("traversalPath", traversalPath.join(""));//print the steps.
	// console.log(traversalPath.filter(s => s!== ".").join(""));//print the steps.
	// after exiting the while loop, the workers array will be in the state in which it was at the moment the last step was added. Reduce the array to the maximum value for worker.timeFree and add 1 to get the answer.
	
	const puzzle2result = "tbd";
	
	console.log("puzzle1: ", puzzle1result);
	console.log("puzzle2: ", puzzle2result);
}


day7();