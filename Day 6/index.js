console.clear();

const INPUT = "61, 90\n199, 205\n170, 60\n235, 312\n121, 290\n62, 191\n289, 130\n131, 188\n259, 82\n177, 97\n205, 47\n302, 247\n94, 355\n340, 75\n315, 128\n337, 351\n73, 244\n273, 103\n306, 239\n261, 198\n355, 94\n322, 69\n308, 333\n123, 63\n218, 44\n278, 288\n172, 202\n286, 172\n141, 193\n72, 316\n84, 121\n106, 46\n349, 77\n358, 66\n309, 234\n289, 268\n173, 154\n338, 57\n316, 95\n300, 279\n95, 285\n68, 201\n77, 117\n313, 297\n259, 97\n270, 318\n338, 149\n273, 120\n229, 262\n270, 136";
// const INPUT = "1, 1\n1, 6\n8, 3\n3, 4\n5, 5\n8, 9";

const printGrid = (grid) => {
	for (let y = 0; y<grid.length; y++){
		let row = "";
		for (let x = 0; x < grid.length; x++){
			row += grid[x][y];
		}
		console.log(row);
	}
};

const isFilled = (grid) => grid.every(row => row.every(pixel => pixel !== "*"));

//points are objects with x & y props.
const manhattanDistance = (point1, point2) => Math.abs(point1.x - point2.x) + Math.abs(point1.y - point2.y);

const day6 = () => {
	const points = INPUT.split("\n").map(
		str => ({
			x: str.match(/^\d+/)[0],
			y: str.match(/\d+$/)[0]
		})
	);
	
	//creates a list of symbols for each point (uppercase letters, then lowercase)
	const letters = Array(points.length);
	for (let i=0; i<letters.length; i++){
		letters[i] = String.fromCharCode(i + (i < 26 ? 65 : 71));
	}
	console.log(letters);
	
	//grid is a square large enough to include all points from 0 to largest coordinate in point.
	const gridSize = 1 + points.reduce(
		(max, point) => Math.max(max, point.x, point.y),
		Number.NEGATIVE_INFINITY
	);
	const grid = Array(gridSize).fill(null).map(elem => Array(gridSize).fill("*"));
	
	//puzzle1: for each point in grid, check the manhattanDistance for all points[]. Once distances[] is filled, find the smallest, and fill grid in based on that.
	//puzzle2: sum up distances[]. If < 10,000, then increment the safeArea counter.
	let safeArea = 0;
	for (let x = 0; x < grid.length; x++){
		for (let y = 0; y < grid.length; y++){
			let distances = Array(points.length);
			for (let i=0; i < points.length; i++){
				distances[i] = manhattanDistance({x, y}, points[i]);
			}
			let {symbol} = distances.reduce(
				(result, distance, i) => {
					if (distance < result.distance){
						//this is the new result.
						result.symbol = letters[i];
						result.distance = distances[i];
					} else if (distance === result.distance){
						//new result is a tie, at least until a lower distance is found.
						result.symbol = ".";
					}
					return result;
				},
				{symbol: "*", distance: Infinity}
			);
			grid[x][y] = symbol; // puzzle1 info
			let sumOfDistances = distances.reduce((a,b) => a + b );
			if (sumOfDistances < 10000){
				safeArea++;
			}
		}
	}
	
	//iterate over grid, creating a histogram of the counts.
	let counts = {};
	for (let x=0; x<gridSize; x++){
		for (let y=0; y<gridSize; y++){
			//if there is no property, create and set to 0 to allow the subsequent increment to initialize the count for the letter at grid[x][y] correctly.
			if (!counts.hasOwnProperty(grid[x][y])){
				counts[grid[x][y]] = 0;
			}
			counts[grid[x][y]]++;
			//if the pixel is on the edge of the grid, set it to Infinity. Subsequent increments will keep the value Infinity.
			if (x===0 || x===gridSize-1 || y===0 || y===gridSize-1){
				counts[grid[x][y]] = Infinity;
			}
		}
	}
	
	console.log(counts);
	delete counts ['.'];
	let biggestFinite = Number.NEGATIVE_INFINITY;
	for (let point in counts){
		if (counts[point] !== Infinity && counts[point] > biggestFinite){
			biggestFinite = counts[point];
		}
	}
	
	printGrid(grid);
	
	console.log(points);
	console.log("gridSize:", gridSize);
	
	const puzzle1result = biggestFinite;
	// const puzzle1result = "tbd";
	const puzzle2result = safeArea;	
	
	console.log("puzzle1: ", puzzle1result);
	console.log("puzzle2: ", puzzle2result);
}


day6();