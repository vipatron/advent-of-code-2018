console.clear();

const INPUT = "61, 90\n199, 205\n170, 60\n235, 312\n121, 290\n62, 191\n289, 130\n131, 188\n259, 82\n177, 97\n205, 47\n302, 247\n94, 355\n340, 75\n315, 128\n337, 351\n73, 244\n273, 103\n306, 239\n261, 198\n355, 94\n322, 69\n308, 333\n123, 63\n218, 44\n278, 288\n172, 202\n286, 172\n141, 193\n72, 316\n84, 121\n106, 46\n349, 77\n358, 66\n309, 234\n289, 268\n173, 154\n338, 57\n316, 95\n300, 279\n95, 285\n68, 201\n77, 117\n313, 297\n259, 97\n270, 318\n338, 149\n273, 120\n229, 262\n270, 136";
// const INPUT = "1, 1\n1, 6\n8, 3\n3, 4\n5, 5\n8, 9";

const printGrid = (grid, mask) => {
	for (let y = 0; y<grid.length; y++){
		let gridRow = "";
		let maskRow = "";
		for (let x = 0; x < grid.length; x++){
			gridRow += grid[x][y];
			maskRow += mask[x][y];
		}
		console.log(gridRow, maskRow);
	}
}

const isFilled = (grid) => grid.every(row => row.every(pixel => pixel !== "*"));

//points are objects with x & y props.
const manhattanDistance = (point1, point2) => Math.abs(point1.x - point2.x) + Math.abs(point1.y - point2.y);

const day6 = () => {
	const points = INPUT.split("\n").map(
		str => ({
			x: str.match(/^\d+/)[0],
			y: str.match(/\d+$/)[0]
		})
	);
	
	const letters = Array(points.length);
	// const letters = Array(52);
	for (let i=0; i<letters.length; i++){
		letters[i] = String.fromCharCode(i + (i < 26 ? 65 : 71));
	}
	console.log(letters);

	
	const gridSize = 1 + points.reduce(
		(max, point) => Math.max(max, point.x, point.y),
		Number.NEGATIVE_INFINITY
	);
	const grid = Array(gridSize).fill(null).map(elem => Array(gridSize).fill("*"));
	const mask = Array(gridSize).fill(null).map(elem => Array(gridSize).fill("*"));
	//circle each point in points[] at increasingly large radii (0+). Every point in the grid will be accounted for, and by using two symbols (/ and X), we can be sure that a point is either: closest to one point (X), equidistant (. replacing /). At the end of every cycle laying down /'s, replace all remaining / with X's.
	let radius = 0;
	while(!isFilled(mask)){
		for (let i = 0; i < points.length; i++){
			console.log("radius", radius, "point i:", i);
			
			//iterate over a square of side: 2(radius) + 1 centered on points[i] to limit the domain of inquiry. The square contains all possible points whose manhattanDistance is 'radius'.
			for (let x = points[i].x - radius; x <= points[i].x + radius; x++){
				for (let y = points[i].y - radius; y <= points[i].y + radius; y++){
					//check if the square point is inside the grid, then check each point's manhattanDistance, and mark appropriately.
					if (x>=0 && x<gridSize && y>=0 && y<gridSize &&
					manhattanDistance(points[i], {x, y}) == radius){
						if (mask[x][y] === '*'){
							grid[x][y] = letters[i];
							mask[x][y] = '/';
						} else if (mask[x][y] === '/'){
							grid[x][y] = mask[x][y] = '.';
						}
					}
				}
			}
		}
		printGrid(grid, mask);
		//TODO: iterate through grid and turn /'s into X's.
		for (let x = 0; x < gridSize; x++){
			for (let y = 0; y < gridSize; y++){
				if (mask[x][y] === "/"){
					mask[x][y] = "X";
				}
			}
		}

		// printGrid(grid, mask);
		// console.log("==============");
		radius++;
	}
	
	let counts = {};
	
	//iterate over grid, creating a histogram of the counts.
	for (let x=0; x<gridSize; x++){
		for (let y=0; y<gridSize; y++){
			//if there is no property, create and set to 0 to allow the subsequent increment to initialize the count for the letter at grid[x][y] correctly.
			if (!counts.hasOwnProperty(grid[x][y])){
				counts[grid[x][y]] = 0;
			}
			counts[grid[x][y]]++;
			//if the pixel is on the edge of the grid, set it to Infinity. Subsequent increments will keep the value Infinity.
			if (x===0 || x===gridSize-1 || y===0 || y===gridSize-1){
				counts[grid[x][y]] = Infinity;
			}
		}
	}
	
	console.log(counts);
	delete counts ['.'];
	let biggestFinite = Number.NEGATIVE_INFINITY;
	for (let point in counts){
		if (counts[point] !== Infinity && counts[point] > biggestFinite){
			biggestFinite = counts[point];
		}
	}
	
	
	printGrid(grid, mask);
	
	console.log(points);
	console.log("gridSize:", gridSize);
	
	// const puzzle1result = biggestFinite;
	const puzzle1result = "tbd";
	const puzzle2result = "tbd";	
	
	console.log("puzzle1: ", puzzle1result);
	console.log("puzzle2: ", puzzle2result);
}


day6();