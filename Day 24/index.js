import { getInput } from '../helpers/helpers.js'
console.clear();

const VERBOSE = false;
const MODERATE = false || VERBOSE;
const TERSE = false;

const INPUT = getInput(24);
/*all input lines w/ armies.
	const INPUT = "Immune System:\n889 units each with 3275 hit points (weak to bludgeoning, radiation; immune to cold) with an attack that does 36 bludgeoning damage at initiative 12\n94 units each with 1336 hit points (weak to radiation, cold) with an attack that does 127 bludgeoning damage at initiative 7\n1990 units each with 5438 hit points with an attack that does 25 slashing damage at initiative 20\n1211 units each with 6640 hit points with an attack that does 54 fire damage at initiative 19\n3026 units each with 7938 hit points (weak to bludgeoning; immune to cold) with an attack that does 26 bludgeoning damage at initiative 16\n6440 units each with 9654 hit points with an attack that does 14 fire damage at initiative 4\n2609 units each with 8218 hit points (weak to bludgeoning) with an attack that does 28 cold damage at initiative 3\n3232 units each with 11865 hit points (weak to radiation) with an attack that does 30 slashing damage at initiative 14\n2835 units each with 7220 hit points (immune to fire, radiation) with an attack that does 18 bludgeoning damage at initiative 2\n2570 units each with 4797 hit points (weak to cold) with an attack that does 15 radiation damage at initiative 17\n\nInfection:\n333 units each with 44943 hit points (weak to bludgeoning) with an attack that does 223 slashing damage at initiative 13\n1038 units each with 10867 hit points (immune to bludgeoning, slashing, fire) with an attack that does 16 cold damage at initiative 8\n57 units each with 50892 hit points with an attack that does 1732 cold damage at initiative 5\n196 units each with 36139 hit points (weak to cold) with an attack that does 334 fire damage at initiative 6\n2886 units each with 45736 hit points (immune to cold; weak to slashing) with an attack that does 25 cold damage at initiative 1\n4484 units each with 37913 hit points (weak to bludgeoning; immune to fire, radiation, slashing) with an attack that does 16 fire damage at initiative 18\n1852 units each with 49409 hit points (immune to bludgeoning; weak to radiation) with an attack that does 52 radiation damage at initiative 9\n3049 units each with 18862 hit points (weak to radiation) with an attack that does 12 fire damage at initiative 10\n1186 units each with 23898 hit points (immune to fire) with an attack that does 34 bludgeoning damage at initiative 15\n6003 units each with 12593 hit points with an attack that does 2 cold damage at initiative 11\n";
*/
// const INPUT = "18 units each with 729 hit points (weak to fire; immune to cold, slashing) with an attack that does 8 radiation damage at initiative 10";
// const INPUT = "Immune System:\n17 units each with 5390 hit points (weak to radiation, bludgeoning) with an attack that does 4507 fire damage at initiative 2\n989 units each with 1274 hit points (immune to fire; weak to bludgeoning, slashing) with an attack that does 25 slashing damage at initiative 3\n\nInfection:\n801 units each with 4706 hit points (weak to radiation) with an attack that does 116 bludgeoning damage at initiative 1\n4485 units each with 2961 hit points (immune to radiation; weak to fire, cold) with an attack that does 12 slashing damage at initiative 4";

const day24 = () => {
	const lines = INPUT.trim().split("\n");
	// console.log(tokens);

	class Group{
		constructor(line, army, groupIndex){
			this.army = army;
			this.groupNumber = groupIndex+1;
			//pull out all the properties guaranteed to be there.
			[,this.numUnits, this.HP, this.damage, this.attackType, this.initiative] = line.match(/(\d+) units.* (\d+) hit points.* (\d+) (\w+) damage.*initiative (\d+)/).map(match => parseInt(match) || match);

			//pull out optional properties (weakness; immunity)
			if (line.includes('(')){
				const options = line.match(/\((.*)\)/)[1].split(';').map(s => s.trim());
				options.forEach(option => {
					let [,modifier, types] = option.match(/(\w+) to (.*)/);
					modifier = modifier === "weak" ? "weakness" : "immunity";
					this[modifier] = types.split(", ");
				});
			}
		}

		get effectivePower (){
			return this.numUnits * this.damage;
		}
	}
	
	
	class Army{
		constructor(armyDescription){
			this.name = armyDescription.shift().slice(0,-1);
			this.groups = armyDescription.filter(//filter to remove empty lines
				line => line
			).map(//map from lines onto groups.
				(line,i) => new Group(line, this.name, i)
			);
		}		
	}
	//parse lines into armies (usable for both puzzles)
	const unboostedArmies = [];
	let firstLineOfArmy, lastLineOfArmy;
	for (let i=0; i<lines.length; i++){
		if(lines[i].endsWith(':')) firstLineOfArmy = i;
		if(lines[i] === "" || i === lines.length-1) lastLineOfArmy = i;
		if (firstLineOfArmy !=undefined && lastLineOfArmy){//are defined...
			unboostedArmies.push(new Army( lines.slice(firstLineOfArmy, lastLineOfArmy+1) ));
			firstLineOfArmy = lastLineOfArmy = undefined;//reset their definitions.
		}
	}
	
	console.log(unboostedArmies);

	//utility functions.
	const attackerChoosingOrder = (a,b) => b.effectivePower - a.effectivePower || b.initiative - a.initiative;
	const damageDealt = (attacker, enemy) => {
		// console.log("attacker", attacker, attacker.effectivePower, "enemy", enemy, enemy.effectivePower);//testing code
		if (enemy.immunity && enemy.immunity.includes(attacker.attackType)) return 0;
		else if (enemy.weakness && enemy.weakness.includes(attacker.attackType)) return 2*attacker.effectivePower;
		else return attacker.effectivePower;
	};
	const copyArmies = armies => armies.map(army => {
		const newArmy = Object.assign({}, army);
		newArmy.groups = newArmy.groups.map(group => Object.create(group));
		return newArmy;
	});

	let boost25plus = false;

	const armyToString = army => army.name.padStart(13)+": " + army.groups.map(group => group.numUnits.toString().padStart(4)).toString();
	const printGroups = army => console.log(armyToString(army));

	function simulateCombat(armies){
		TERSE && armies.forEach(army => printGroups(army));
		let lastImmuneState, lastInfectionState;//testing code to avoid infinite loop.

		//fight loop
		let round = 0;
		while(armies.every(army => army.groups.length)){//end combat if, after a fight, both armies don't still have units.
			// if (boost25plus) console.log("round", ++round);
			// if (++round > 10) return;
			//Print out Round heading.
			MODERATE && armies.forEach(army => {
				console.log("%c"+army.name, "background: teal");
				army.groups.forEach(group => {
					console.log("Group", group.groupNumber, "contains", group.numUnits, "units");
				});
			});
	
			//Target selection phase.
			for (let i = 0; i < armies.length; i++){//for each army, select targets.
				const attackingArmy = armies[i];
				const enemyArmy = armies[(i+1)%2];//when i=0:[1], when i=1:[0]
	
			
				attackingArmy.groups.sort(//into target choosing order (by decr. effectivePower, then initiative)
					attackerChoosingOrder
				).forEach((attacker, attackerIndex) => {//attackerIndex only for logging purposes.
					const damageToEnemyGroups = enemyArmy.groups.map( 
						(enemyGroup, index) => ({
							potentialDamage: enemyGroup.alreadyTargeted ? null : damageDealt(attacker, enemyGroup),//no damage if there is no fight (i.e.: if the enemy group has already been targeted by a friendly group earlier in the target-selecting order).
							index
						})//wrapping up the index in an object allows us to sort the array while not losing that info.
					);
					const targetsForSelection = damageToEnemyGroups.filter(
						target => target.potentialDamage
					).sort(//in desc. order by potential damage, then by desc. order of target's effectivePower, then by desc. order of the target's initiative.
						(a,b) => b.potentialDamage - a.potentialDamage || 
						enemyArmy.groups[b.index].effectivePower - enemyArmy.groups[a.index].effectivePower ||
						enemyArmy.groups[b.index].initiative - enemyArmy.groups[a.index].initiative
					);
					if (targetsForSelection.length){
						attacker.attack = {
							// targetGroupNumber: targetsForSelection[0].index + 1,//only for Logging
							target: enemyArmy.groups[targetsForSelection[0].index],
							potentialDamage: targetsForSelection[0].potentialDamage//only for logging.
						};
						enemyArmy.groups[targetsForSelection[0].index].alreadyTargeted = true;
					}  else {
						VERBOSE && console.log("%csomehow no target", "background: #600", attacker.army, attacker.groupNumber);
					}
				});
			}
	
			VERBOSE && console.log();
			VERBOSE && armies.forEach(army => {//print out potential damages
				army.groups.forEach(group =>
					group.attack && console.log(army.name, "group", group.groupNumber, "would deal defending group", group.attack.target.groupNumber, group.attack.potentialDamage,"damage")
				);
			});
	
			//Attack phase
			const allGroups = armies[0].groups.concat(armies[1].groups).sort(//by attack order. (initiatives are unique, and equal to starting # of groups)
				(a,b) => b.initiative - a.initiative
			);
			MODERATE && console.log();
			allGroups.forEach(group => {
				if (group.numUnits && group.attack){//some units alive, and they successfully found a non-immune target to attack.
					const numUnitsKilled = Math.min(Math.floor(damageDealt(group, group.attack.target)/group.attack.target.HP), group.attack.target.numUnits);//must recalculate the damage based on CURRENT attacking unit #'s. potentialDamage assume they are all still alive. Also, can't kill more units than there are.
					group.attack.target.numUnits -= numUnitsKilled;
					MODERATE && console.log(group.army, "group", group.groupNumber, "attacks defending group", group.attack.target.groupNumber, "killing", numUnitsKilled,"units");
				}
			});
	
			//cleanup code for next fight:
			armies.forEach(army => army.groups = army.groups.filter(group => group.numUnits));//update the group lists in each army with only living groups
			armies.forEach(army => army.groups.forEach(group => {//remove targeting information from the remaining groups.
				delete group.alreadyTargeted;
				delete group.attack;
			}));

			TERSE && armies.forEach(army => printGroups(army));
			const currentImmuneState = armyToString(armies[0]);
			const currentInfectionState = armyToString(armies[1]);
			if (currentImmuneState === lastImmuneState && currentInfectionState === lastInfectionState){//loop
				// console.log("%carmies at moment of repeat:", "background: navy");
				// console.log(armies);
				return;
			} else {//update surveillance variables
				lastImmuneState = currentImmuneState;
				lastInfectionState = currentInfectionState;
			}
			/*PICK UP HERE: at boost 26, getting into an infinite loop wherein both groups can't kill one another.
			 * LOOK AT TODO 3 and check 0 boost first (before looking over the code);
			 * + TODO: parcel out the groups -> toString function that is part of printGroups() and use it to store a "snapshot" of the groups to which we can compare the current values, to exit a loop. 
			 * TODO: log out the actual armies[] at that moment.
			 * TODO: look at the log trace to see whether this loop is indeed legitimate, and if so, whether I've fucked the algorithm with 0 boost.
			 */
		}
		//figure out how many units the victorious army has.
		const remainingUnitsInVictoriousArmy = armies.reduce(
			(total, army) => total + army.groups.reduce(
				(armySum, group) => armySum + group.numUnits, 0
			), 0
		);
		MODERATE && console.log("%ctotal remaining units in victorious army:", "background: black", remainingUnitsInVictoriousArmy);
		return remainingUnitsInVictoriousArmy;
	}

	function part2(){//run the combat simulation on increasingly large boosted immune systems until the immune system is victorious.
		let immuneSystemVictorious = false;
		let immuneBoost = 1;//because part 1 is unboosted (0).
		let survivingUnits;

		while (!immuneSystemVictorious){
			const boostedArmies = copyArmies(unboostedArmies).map(army => {
				if (army.name === "Immune System"){//then create a new groups array of subclassed objects and overwrite their damage property.
					army.groups = army.groups.map(group => {
						group.damage += immuneBoost;
						return group;
					});
				}
				return army;
			});
			// console.log(boostedArmies);
			survivingUnits = simulateCombat(boostedArmies);
			if (boostedArmies.every(army => army.groups.length)){//a stalemate.
				TERSE && console.log("stalemate at boost:", immuneBoost);
				immuneBoost++;
			} else if (boostedArmies.some(army => army.name==="Immune System" && army.groups.length)){//Immune system won
				immuneSystemVictorious = true;
				console.log("good guys won at boost:", immuneBoost);
			} else {//Infection won.
				// (immuneBoost % 10 === 0) && console.log("bad dudes won at boost:", immuneBoost);
				TERSE && console.log("bad dudes won at boost:", immuneBoost);
				immuneBoost++;
				// //force this to run once:
				// immuneSystemVictorious = true;
			}
		}
		return survivingUnits;
	}

	const puzzle1result = simulateCombat(copyArmies(unboostedArmies));
	const puzzle2result = part2();

	console.log("puzzle1: ", puzzle1result);	
	console.log("puzzle2: ", puzzle2result);
}



day24();