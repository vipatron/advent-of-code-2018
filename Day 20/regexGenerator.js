import { getInput } from '../helpers/helpers.js'
console.clear();
const VERBOSE = true;
const MODERATE = false || VERBOSE;
const TERSE = false || MODERATE;

// const INPUT = getInput(20);

// const INPUT = "^WNE$";
// const INPUT = "^ENWWW(NEEE|SSE(EE|N))$";

// const INPUT = "^ENWWW(NEEE|SSE(EE|N))WEN$";//multiple-depth branches followed by a merge.

const INPUT = "^ENWWW(NEEE|SSE(EE|N)NNN)WEN$";//multiple-depth merges

// const INPUT = "^ENWWW(NEEE|SSE)$"; //simple branch that isn't already on the graph
// const INPUT = "^ENNWSWW(NEWS|)$"; //simple branch that is.
// const INPUT = "^ENNWSWW(NEWS|)SSSEEN$";//simplest branch and merge.

// const INPUT = "^ENNWSWW(NEWS|)SSSEEN(WNSE|)EE(SWEN|)NNN$";

const day20 = () => {
	const trimmedInput = INPUT.trim().slice(1, -1);

	const puzzle1result = part1(trimmedInput);

	// const puzzle1result = "tbd";
	// const puzzle1result = path1(INPUT.trim());
	const puzzle2result = "tbd";
	
	console.log("puzzle1: ", puzzle1result);
	console.log("puzzle2: ", puzzle2result);
}


class RegexGenerator{
	constructor(pattern){
		this.regexString = pattern;
		this.nextNodeSerialNumber = 1;
		this.graph = [];
		this.root = this.parseSubTree(pattern);

		// //eliminate any "empty patterns" by having the grandparents adopt their orphaned children.
		// const adoptiveGrandparents = this.graph.filter(grandparent => grandparent.nextNodes.some(parent => parent.pattern === ""));
		// adoptiveGrandparents.forEach(grandparent =>{
		// 	const dyingEmptyParents = grandparent.nextNodes.filter(parent => parent.pattern === "");
		// 	dyingEmptyParents.forEach(emptyParent => {
		// 		const parentIndex = grandparent.nextNodes.indexOf(emptyParent);
		// 		grandparent.nextNodes.splice(parentIndex, 1, ...emptyParent.nextNodes);
		// 	});
		// });

		//print graph to screen for debug.
		console.log("root:", this.root);
		console.log("leaves:", this.leavesInfo);

		//set up the pathTrackers[] array used by getter nextDirections to get the next possible paths.
		this.pathTrackers = [{
			node: this.root,
			index: 0
		}];
	}

	parseSubTree(regex, isRemainder=false){
		VERBOSE && console.log(`%cparse Called on ${regex}`, "background:darkgreen");

		const firstOpenParenIndex = regex.indexOf('(');
		const nonBranchingSegment = firstOpenParenIndex > -1 ? regex.slice(0, firstOpenParenIndex) : regex;
		const subTreeRoot = {
			pattern: nonBranchingSegment,
			serialNumber: this.nextNodeSerialNumber++,
			nextNodes: [],
		};
		//if this subtree is a remainder of what came before, make it 
		if (isRemainder){
			const leaves = this.graph.filter(node => node.activeLeaf);
			leaves.forEach(leaf => console.log(leaf.pattern, this.IsAncestorOf(leaf, subTreeRoot)));
			VERBOSE && console.log("remainder:", regex, "; leaves at this point:", this.leavesInfo);
			while (leaves.length){
				const leaf = leaves.shift();
				leaf.nextNodes.push(subTreeRoot);
				leaf.activeLeaf = false;
			}
		}

		this.graph.push(subTreeRoot);

		VERBOSE && console.log("non-branching:", nonBranchingSegment);

		if (firstOpenParenIndex > -1){//if there are options/branches...
			//figure out where the top-level split points are by iterating over regex
			let unmatchedOpenParens = 0;
			let matchingToplevelParenIndex;
			const topLevelAlternatorIndices = [];
			let i = firstOpenParenIndex;		
			while (i < regex.length){
				switch(regex[i]){
					case '(':
						unmatchedOpenParens++ //increase the depth counter
					break;
					case ')':
						if (! --unmatchedOpenParens){//decrement depth counter and see if it is now 0.
							matchingToplevelParenIndex = i;
						}
					break;
					case '|':
						if (unmatchedOpenParens === 1){//we're at the top level of options.
							topLevelAlternatorIndices.push(i);
						}
					break;
				}
				if (matchingToplevelParenIndex) break; //only executes if matching top-level closed-paren found.
				i++;
			}

			//take the topLevelAlternatorIndices[] and create an options[].
			topLevelAlternatorIndices.push(matchingToplevelParenIndex);//acts like the last right-hand fencepost to split the options.
			let lastSplitPoint = firstOpenParenIndex;
			let options = [];
			for (let i=0; i<topLevelAlternatorIndices.length; i++){
				options.push(regex.slice(lastSplitPoint+1, topLevelAlternatorIndices[i]));
				lastSplitPoint = topLevelAlternatorIndices[i];
			}

			//parse those options starting from the next available branchpoint.
			options.forEach(option => {
				VERBOSE && console.log("option:", option);
				const optionTreeRoot = this.parseSubTree(option)
				subTreeRoot.nextNodes.push(optionTreeRoot);
				VERBOSE && console.log("graph after returning from parse("+option+"):", this.graphInfo);
			});

			//deal with the remainder of the regex, which describes the identical relevant paths from every leaf in the graph. N.B.: If there were no options, there is by definition no remainder after them.
			const remainder = regex.slice(matchingToplevelParenIndex+1);
			if (remainder !== ""){//there is indeed a remainder. Have all the leaves point to the remainder.
				// const leaves = this.graph.filter(node => node.activeLeaf);// && !node.ancestorOf());
				// VERBOSE && console.log("remainder:", remainder, "; leaves at this point:", this.leavesInfo);
				// console.log("graph:", this.graphInfo);
				// this.graph.forEach(node => console.log(node));
				const remainderNode = this.parseSubTree(remainder, true);
				// while (leaves.length){
				// 	const leaf = leaves.pop();
				// 	leaf.nextNodes.push(remainderNode);
				// 	leaf.activeLeaf = false;
				// }
			} /*else {//clear leaves.
				VERBOSE && console.log("pre-clearance leaves:", this.leavesInfo(leaves));
				leaves.length = 0;
				VERBOSE && console.log("cleared leaves?:", this.leavesInfo(leaves));
			}*/
		} else { //the nonbranching part is all there is to this subtree
			subTreeRoot.activeLeaf = true;
		}
		
		// if (!leaves.length){//then there are no leaves, because we popped them all and attached them to the remainder, or there was only non-branching path, or there was no remainder.
		// 	leaves.push(subTreeRoot);
		// }
		VERBOSE && console.log(`%cpattern: ${regex} graph:`, "background: darkred", this.graphInfo);
	
		return subTreeRoot;
	}

	IsAncestorOf(ancestor, descendant){
		const descendants = ancestor.nextNodes.slice();
		for (let i=0; i<descendants.length; i++){
			if (descendants[i] === descendant){
				return true;
			} else {
				descendant[i].nextNodes.forEach(node => {
					if (!descendants.includes(node)) descendants.push(node);
				});
			}
		}
		return false;
	}

	get leavesInfo(){
		return this.graph.filter(node => node.activeLeaf).map(leaf => leaf.pattern + "*");
	}

	get graphInfo(){
		return this.graph.map(node => node.pattern + (node.activeLeaf ? "*" : ""))
	}

	get nextDirections(){/*TODO: finish this*/
		const directions = [];
		for (let i=0; i<this.pathTrackers.length; i++) {
			let tracker = this.pathTrackers[i];
			if (tracker.index === tracker.node.pattern.length){//should never be over, since we started from 0 and increment by 1.
				tracker.node.nextNodes.forEach(nextNode =>
					this.pathTrackers.push({
						node: nextNode,
						index: 0
					})
				);
				tracker.dead = true;
			} else {
				if (!directions.includes(tracker.node.pattern[tracker.index])) directions.push(tracker.node.pattern[tracker.index]);
				tracker.index++;	
			}
		}
		this.pathTrackers = this.pathTrackers.filter(tracker => !tracker.dead);
		console.log("trackers in get:", this.pathTrackers.map(trkr => '['+trkr.index+']:'+trkr.node.pattern));
		return directions;
	}
	
	get EOF(){
		return !this.pathTrackers.length || this.pathTrackers.every(tracker => !tracker.node.nextNodes.length && tracker.index === tracker.node.pattern.length);
	}
}

const part1 = (regex) => {
	const directions = ['N', 'E', 'S', 'W'];
	const startPoint = new roomNode();//no path, no parent.(this is the graph's root)
	const grid = new MapGrid(1, 1);
	grid.set(startPoint.x, startPoint.y, 'X');
	grid.print("green");

	const paths = regex.match(/[NEWS]+/g);
	const longestPath = paths.reduce((longestLength, path) => Math.max(longestLength, path.length), Number.NEGATIVE_INFINITY);
	console.log("longest path:", longestPath);

	const solvedRooms = [];
	const roomsToVisitQueue = [startPoint];

	let pathGen = new RegexGenerator(regex);
/*	while (!pathGen.EOF){
		console.log("next possible paths:", pathGen.nextDirections);
	}
*/
	// const pathLength = 1;
	// const path = "";
	// //do below for each point on the queue.
	// let counter = 0;
	// while (counter < 7 && roomsToVisitQueue.length){
	// 	counter++;
		
	// 	const currentRoom = roomsToVisitQueue.shift();
	// 	solvedRooms.push(currentRoom);
		
	// 	console.log("current room:", currentRoom);
	// 	console.log(`(${currentRoom.x}, ${currentRoom.y}): "${currentRoom.path}"`)

	// 	for (let i=0; i<directions.length; i++){
	// 		//attempt to extend the pathString, and see if it works against the regex.
	// 		const potentialNextRoom = new roomNode(directions[i], currentRoom);
	// 		console.log("path?", potentialNextRoom.path, "regex:", regex.slice(0, potentialNextRoom.path.length+1));
	// 		const pathLengthRegex = RegExp(regex.slice(0, potentialNextRoom.path.length+1));//add 1 to account for the ^
	// 		if (pathLengthRegex.test(potentialNextRoom.path)){
	// 			roomsToVisitQueue.push(potentialNextRoom);
	// 			// break; (There may be multiple paths out of this room).
	// 		}
	// 	}
	// 	console.log("queue", roomsToVisitQueue);
	// }
	
	// /*TODO: BRANCHES, then REMAINDERS?, or maybe just one-more regexes?*/
	
	// console.log("solved:", solvedRooms);
	
	return "tbd";
};

function roomNode (path="", parentNode = null){
	// console.log("in room node, path:", path, "parentNode:", parentNode);
	this.x = 0;//default value, replace if parentNode exists.
	this.y = 0;
	this.path = path;
	this.priorRoom = parentNode;
	this.nextNodes = [];
	if (parentNode){
		// console.log("parent isn't null");
		this.x = parentNode.x;
		this.y = parentNode.y;
		switch (path){
			case 'N':
				this.y -= 1;
				break;
			case 'S':
				this.y += 1;
				break;
			case 'E':
				this.x += 1;
				break;
			case 'W':
				this.x -= 1;
				break;
		}
		this.path = parentNode.path + path;
		parentNode.nextNodes.push(this);
	}
}

class MapGrid {
	constructor(numCols, numRows, colOffset=0, rowOffset=0){
		// console.log("constructor called with:", numCols, numRows, colOffset, rowOffset);
		this._width = 2 * numCols + 1;//the actual width of the 2-d array.
		this._height = 2 * numRows + 1;
		this._grid =  Array(this._height).fill(null).map(
			(row, rowIndex) => rowIndex%2 === 0 
			? Array(this._width).fill('?').map((val, i) => i%2 === 0 ? '#' : val)//even rows (walls & doors)
			: Array(this._width).fill(' ').map((val, i) => i%2 === 0 ? '?' : val)//odd rows (rooms & doors at first)
		);//the row fill value will change, but for now, '?' means door or wall, and ' ' means unexplored room.
		this._xOffset = colOffset;
		this._yOffset = rowOffset;
	}
	//getters
	get numCols(){
		return (this._width-1)/2; //width = 2*numCols + 1 (fenceposted walls around cols)
	}
	get numRows(){
		return (this._height-1)/2;
	}
	/*This MapGrid object models a "virtual 2-d array" MapGrid[x][y] onto a real 2-d array named grid. Two separate transformations on the given (x,y) coordinates are peformed:
	1. The (x, y) gets added to (-xOffset, -yOffset), which is the same as the (startingX, startingY), so that if the virtual grid positions range from [(startX, startY) to (startX + numCols-1, startY + numRows-1)], they get mapped onto the virtual range [(0, 0) to (numCols-1, numRows-1)]
	2. The virtual grid is then mapped onto the real grid (which is enclosed by fenceposted walls) by the function (rowindex, colIndex) = (2x+1, 2y+1), so the real range is [(1,1) to (2*numCols-1, 2*numRows-1)]
	*/
	set(x, y, value){
		x -= this._xOffset; //given will now be indices in the virtual grid
		y -= this._yOffset;
		/*NOTE: for the following adjustment codes, we have to adjust for each unit by which x is less than 0, not simply ++/--*/
		if (x < 0){
			this._grid.forEach((row, rowIndex) => {for (let i=0; i<-x; i++)row.unshift(
				...(rowIndex%2 === 0
				? ['#', '?']//wall row
				: ['?', ' '])//room row
			);});//fill out the "negative" indices in each row
			this._width -= 2*x; //increment grid width;
			this._xOffset += x;//decrement the offset further.
			x = 0;//because the previously negative index is now the first column.
		} else if (x >= this.numCols){//then grid.forEach(row => row.push) and this._width++;
			let additionalCols = x - this.numCols + 1;//since last "virtual column" is [this.numCols-1]
			this._grid.forEach((row, rowIndex) => {for (let i=0; i<additionalCols; i++)row.push(
				...(rowIndex%2 === 0
					? ['?', '#']//wall row
					: [' ', '?'])//room row
			);}); //fill out the "missing" indices on the right-side of each row
			this._width += 2*additionalCols;//increment grid width;
		}
		if (y < 0){//update rows after updating columns because we need a valid value for this._width
			for (let i=0; i<-y; i++) this._grid.unshift(
				Array(this._width).fill('?').map((val, i) => i%2 === 0 ? '#' : val), //1st or odd row to unshift (walls & doors row)
				Array(this._width).fill(' ').map((val, i) => i%2 === 0 ? '?' : val), //0th or even row to unshift (room row)
			);

			this._height -= 2*y;
			this._yOffset += y;
			y = 0;
		} else if (y >= this.numRows){
			let additionalRows = y - this.numRows + 1;//since last "virtual row" is [this.numRows-1];
			for (let i=0; i<additionalRows; i++) this._grid.push(
				Array(this._width).fill(' ').map((val, i) => i%2 === 0 ? '?' : val), //0th or even row to push (room row)
				Array(this._width).fill('?').map((val, i) => i%2 === 0 ? '#' : val), //1st or odd row to push (walls & doors row)
			);
			this._height += 2*additionalRows;
		}
		this._grid[2*y+1][2*x+1] = value;
	}

	openDoorsBetween(x1, y1, x2, y2){
		// console.log(arguments);
		// console.log("xOffset", this._xOffset, "yOffset", this._yOffset);
		for (let i in Object.keys(arguments)){
			// console.log(i, i%2=== 0 ? 'even row': 'odd row');
			arguments[i] -= i%2=== 0 ? this._xOffset : this._yOffset; //put the coordinates into the virtual grid [0 ... len)
			arguments[i] = 2 * arguments[i] + 1;//map the virtual coordinates onto "real grid" coordinates.
		}
		// console.log(arguments);
		[x1, y1, x2, y2] = arguments;
		// console.log("x1:", x1, "y1:", y1, "x2:", x2, "y2:", y2);
		this._grid[(y1+y2)/2][(x1+x2)/2] = y1 === y2 ? '|' : '-';
	}

	get(x, y){
		x -= this._xOffset;
		y -= this._yOffset
		if (x<0 || x>=this.numCols || y<0 || y >=this.numRows){
			// console.log("get mandates undefined. x:", x, "y:", y, "#rows:", this.numCols, "#cols:", this.numRows);
			return undefined;
		} else {
			return this._grid[2*y+1][2*x+1];
		}
	}

	print(color = "turquoise"){
		const formattingStrings = ["background: none"];
		const rawGridString = this._grid.reduce((gridString, row) => gridString += row.join("")+"\n", "");
		const formattedOutput = "%c"+rawGridString.replace(/[-|X\.]/g, match => {
			formattingStrings.push("background: "+color+"; color: black;", "background: default; color: default;");
			return "%c"+match+"%c";
		});
		console.log(formattedOutput, ...formattingStrings);
	}

	printWithPoint(x, y, pointColor = "red", color = "turquoise"){
		x -= this._xOffset;
		y -= this._yOffset;
		x = 2*x+1;
		y = 2*y+1;
		
		let PointIndexInGridString = y * (this._width+1) + x //each row will include (width + 1) chars because of the newline.
		const rawGridString = this._grid.reduce((gridString, row) => gridString += row.join("")+"\n", "");

		const formattingStringsPrePoint = ["background: none"];
		const formattedOutputPrePoint = "%c"+rawGridString.slice(0, PointIndexInGridString).replace(/[-|X\.]/g, match => {
			formattingStringsPrePoint.push("background: "+color+"; color: black;", "background: default; color: default;");
			return "%c"+match+"%c";
		});
		const formattedPoint = `%c${rawGridString[PointIndexInGridString]}%c`;
		const pointFormattingStrings = ["background: "+pointColor+"; color: black;", "background: default; color: default;"]
		const formattingStringsPostPoint = [];
		const formattedOutputPostPoint = rawGridString.slice(PointIndexInGridString+1).replace(/[-|X\.]/g, match => {
			formattingStringsPostPoint.push("background: "+color+"; color: black;", "background: default; color: default;");
			return "%c"+match+"%c";
		});

		console.log(formattedOutputPrePoint + formattedPoint + formattedOutputPostPoint,
			 ...formattingStringsPrePoint, ...pointFormattingStrings, ...formattingStringsPostPoint);
	}

	finalizeMap(){
		this._grid.forEach((row, i , grid) => grid[i] = row.map(col => col === '?' ? '#' : col))
	}
}

day20();