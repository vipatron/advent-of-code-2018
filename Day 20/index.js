import { getInput } from '../helpers/helpers.js'
console.clear();
const VERBOSE = false;

const INPUT = getInput(20);

// const INPUT = "^WNE$";
// const INPUT = "^ENWWW(NEEE|SSE(EE|N))$";
// const INPUT = "^ENNWSWW(NEWS|)SSSEEN(WNSE|)EE(SWEN|)NNN$";
// const INPUT = "^ESSWWN(E|NNENN(EESS(WNSE|)SSS|WWWSSSSE(SW|NNNE)))$";
// const INPUT = "^WSSEESWWWNW(S|NENNEEEENN(ESSSSW(NWSW|SSEN)|WSWWN(E|WWS(E|SS))))$";

const day20 = () => {
	const trimmedInput = INPUT.trim().slice(1, -1);

	const puzzle1result = part1(trimmedInput);

	// const puzzle1result = "tbd";
	// const puzzle1result = path1(INPUT.trim());
	const puzzle2result = "tbd";
	
	console.log("puzzle1: ", puzzle1result);
	console.log("puzzle2: ", puzzle2result);
}

function part1(regex){
	console.log(regex);
	const chars = [...regex];
	let currentPoint = {x: 0, y: 0};
	const grid = {0: {0: new Point()}};//grid[row][col]

	const stack = [];

	chars.forEach(char => {/*TODO: handle collisions*/
		switch(char){
			case 'N':
				addPoint(0, -1, char);
				break;
			case 'S':
				addPoint(0, 1, char);
				break;
			case 'E':
				addPoint(1, 0, char);
				break;
			case 'W':
				addPoint(-1, 0, char);
				break;
			case '(':
				stack.push(currentPoint);
				VERBOSE && console.log(stack.map(point => `(${point.x}, ${point.y})`));
				break;
			case '|':
				currentPoint = stack[stack.length-1];//for each option, go back to the last branch point, i.e. the top of the stack.
				VERBOSE && console.log(stack.map(point => `(${point.x}, ${point.y})`));
				break;
			case ')':
				currentPoint = stack.pop();
				VERBOSE && console.log(stack.map(point => `(${point.x}, ${point.y})`));
				break;
		}
		VERBOSE && console.log(char+":", currentPoint);
	});

	function addPoint(dX, dY, pathChar){
		const y = currentPoint.y + dY;
		const x = currentPoint.x + dX;
		const row = grid[y] || (grid[y] = {});
		const previouslyVisitedPoint = row[x];

		const potentialNewPoint = new Point(pathChar, grid[currentPoint.y][currentPoint.x]);
		if (!previouslyVisitedPoint || previouslyVisitedPoint.pathLength > potentialNewPoint.pathLength){
			row[x] = potentialNewPoint;
		} //else do nothing: the previous visit to that point is sufficient.
		currentPoint = {x, y};
	}

	console.log(grid);
	console.log(currentPoint, grid[currentPoint.y][currentPoint.x]);

	const allRooms = Object.values(grid).reduce(
		(arr, row) => [...arr, ...Object.values(row)], []
	)
	
	const longestPathLength = allRooms.reduce(
		(longestPathLength, point) => Math.max(longestPathLength, point.pathLength), Number.NEGATIVE_INFINITY
	);


	const numThousandStepPaths = allRooms.reduce(
		(numThousandStepPaths, point) => numThousandStepPaths + (point.pathLength > 999 ? 1 : 0), 0
	);

	return [longestPathLength, numThousandStepPaths];
}

function Point(pathStep="", priorPoint){
	this.path = priorPoint ? priorPoint.path + pathStep : pathStep;
	this.pathLength = this.path.length;
}


day20();