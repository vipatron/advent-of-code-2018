import { getInput } from '../helpers/helpers.js'
console.clear();
const VERBOSE = true;
const TERSE = true || VERBOSE;


// const INPUT = getInput(20);

// const INPUT = "^WNE$";

// const INPUT = "^ENWWW(NEEE|SSE(EE|N))$";
// const INPUT = "^ENWWW(NEEE|SSE(EE|N))WEN$";

const INPUT = "^ENNWSWW(NEWS|)SSSEEN(WNSE|)EE(SWEN|)NNN$";
// const INPUT = "^ESSWWN(E|NNENN(EESS(WNSE|)SSS|WWWSSSSE(SW|NNNE)))$";
// const INPUT = "^WSSEESWWWNW(S|NENNEEEENN(ESSSSW(NWSW|SSEN)|WSWWN(E|WWS(E|SS))))$";

const day20 = () => {
	const puzzle1result = part1(INPUT.slice(1,INPUT.length-1));//assume for part 1 that the farthest room will be located at the end of a straight path.
	const puzzle2result = "tbd";
	
	console.log("puzzle1: ", puzzle1result);
	console.log("puzzle2: ", puzzle2result);
}

function pathSegmentNode (pathString, x, y, priorSegment, nextSegments){
	this.pathSegment = pathString;
	this.pathLength = pathString.length + (priorSegment ? priorSegment.pathLength : 0);
	this.x = x;
	this.y = y;
	this.priorNode = priorSegment || null; //if priorSegment is undefined, define it as null
	/*PICK UP HERE*/
	if (this.priorNode) this.priorNode.nextNodes.push(this);
	this.nextNodes = nextSegments || [];
}

const convertToDepths = regexPattern => {
	const resultArray = Array(regexPattern.length);
	let depth = 0;
	let maxDepth = 0;
	let firstOpenParenIndex, matchingToplevelParenIndex;
	for (let i=0; i < resultArray.length; i++){
		switch(regexPattern[i]){
			case '(':
				if (++depth > maxDepth) maxDepth = depth;
				resultArray[i] = '(';
			break;
			case ')':
				--depth;
				resultArray[i] = ')';
			break;
			case '|':
				resultArray[i] = '|';
			break;
			default:
				if (depth < 30) resultArray[i] = depth % 10;
				else resultArray[i] = '*';
			break;
		}
	}
	console.log("max depth:", maxDepth);
	return resultArray.join("");
}

// let GLOBAL_CALL_STACK_COUNTER = 0;

const parseRegex = (regexPattern, parentNode, graph) => {
	// if (++GLOBAL_CALL_STACK_COUNTER > 20) return;
	VERBOSE && console.log(`%cparseRegex: ${regexPattern}`, "background: #930; color: white");
	let leaves = [];
	// if (regexPattern === "") return [parentNode];

	graph.sort((a, b) => a.x - b.x || a.y - b.y);//sort graph by (x,y) order with push then sort, effective insertion.
	
	//if we assume all input is valid, then inputs are of two possible types:
		//1 [NEWS]* - leading with a nonoptional path
		//2 (.*|.*|...)leading with branches enclosed with parentheses, then a remainder that will be followed by every branch.

	let firstOpenParenIndex = regexPattern.indexOf('(');
	if (firstOpenParenIndex){//case 1: nonoptional path (either -1 or +, and either way, nonzero so 'truthy');
		//if there are no options, just use the whole string, otherwise slice it out
		const nonBranchingPath = firstOpenParenIndex === -1 ? regexPattern : regexPattern.slice(0, firstOpenParenIndex);
		const newNode = getNextWayPoint(parentNode, nonBranchingPath);
		VERBOSE && console.log("%cnonopt node: "+nodeToString(newNode), "background: #600");
		VERBOSE && console.log("%cparent node: "+nodeToString(newNode.priorNode), "background: #006");
		VERBOSE && printGraph(graph);

		const optimalNode = mergeNodeIntoGraph(newNode, graph);
		if (optimalNode !== newNode){
			TERSE && console.log("%c OPTIMAL NODE DIFFERENT -nonbranch", "background: turquoise; color: darkred;");
			// newNode.priorNode.nextNodes.pop();
		}


		if (firstOpenParenIndex !== -1){//there is more to subtree.
			VERBOSE && console.log("calling from nonopt");
			leaves = parseRegex(regexPattern.slice(firstOpenParenIndex), optimalNode, graph);
		} else {//this is the leaf.
			leaves.push(optimalNode);
		}
	} else {//case 2: options.
		let unmatchedOpenParens = 1;
		let matchingToplevelParenIndex;
		const topLevelAlternatorIndices = [];
		let i = 1;//we know [0] is '('
		while (i < regexPattern.length){
			switch(regexPattern[i]){
				case '(':
					unmatchedOpenParens++ //increase the depth counter
				break;
				case ')':
					if (! --unmatchedOpenParens){//decrement depth counter and see if it is now 0.
						matchingToplevelParenIndex = i;
					}
				break;
				case '|':
					if (unmatchedOpenParens === 1){//we're at the top level of options.
						topLevelAlternatorIndices.push(i);
					}
				break;
			}
			if (matchingToplevelParenIndex) break; //only executes if matching top-level closed-paren found.
			i++;
		}
		//assuming all input is valid, we know that branches have now been defined
		const options = [];
		topLevelAlternatorIndices.push(matchingToplevelParenIndex);//acts like the last right-hand fencepost to split the options.
		let lastSplitPoint = firstOpenParenIndex;
		let remainder = regexPattern.slice(matchingToplevelParenIndex+1);
		for (let i=0; i<topLevelAlternatorIndices.length; i++){
			options.push(regexPattern.slice(lastSplitPoint+1, topLevelAlternatorIndices[i]));
			lastSplitPoint = topLevelAlternatorIndices[i];
		}
		const branches = [];
		options.forEach(option => {
			const optionSubtreeIndex = option.indexOf('(');
			const optionFixedPath = optionSubtreeIndex === -1 ? option : option.slice(0,optionSubtreeIndex);
			const optionSubTree = optionSubtreeIndex === -1 ? "" : option.slice(optionSubtreeIndex);
			const newNode = getNextWayPoint(parentNode, optionFixedPath);

			VERBOSE && console.log("%cbranch node: "+nodeToString(newNode), "background: #600");
			VERBOSE && console.log("%cparent node: "+nodeToString(newNode.priorNode), "background: #060");
			VERBOSE && printGraph(graph);
			
			const optimalNode = mergeNodeIntoGraph(newNode, graph);//node representing the optimal path to that (x,y) point.
			// if (optionSubTree !== "" && !branches.some(branch => branch.node === optimalNode)){
			if (optimalNode !== newNode){
				TERSE && console.log("%c MOVE THIS CODE INTO MERGE! OPTIMAL NODE DIFFERENT", "background: turquoise; color: black;");
				// newNode.priorNode.nextNodes.pop();
			}
			if (!branches.some(branch => branch.node === optimalNode)){
				VERBOSE && console.log("calling from BRANCH -> new Optimal Node");//may need to call the subtree whether optimal node is new or not?
				branches.push({
					node: optimalNode,
					subTreeLeaves: parseRegex(optionSubTree, optimalNode, graph) //if subtree is "", (no subtree), the non-optional case (#1) above will return [optimalNode]
				});
			}
		});
		VERBOSE && console.log("options:", options);
		// VERBOSE && console.log("branches", branches);

		leaves = branches.reduce((branchLeaves, branch) => branchLeaves.concat(branch.subTreeLeaves.filter(leaf => leaf.pathSegment !== "")), leaves);

		VERBOSE && console.log("leaves", leaves);

		//this should actually be leaves.forEach
		leaves.forEach(leaf =>
			{VERBOSE && console.log("calling from BRANCH -> leaf.forEach");//DELETE THIS UGLY ARROW CONSOLE LOG?
			parseRegex(remainder, leaf, graph);}
		);
	}

	return leaves;
}

const mergeNodeIntoGraph = (newNode, graph) => {
	/*If we can assume the graph has no redundancies so far, there are only three options, only two of which require action:
		//1 - the (x,y) point isn't on the graph - add it.
		//2 - the (x,y) point is already on the graph with a shorter pathlength - ignore the new node.
		//3 - the (x,y) point is already on the graph, but with a longer path - replace the old node and adopt its children (because of DFS, their pathlengths will have already been determined, and will only be on the graph if they represented the shortest paths already, thus they will be even shorter and not needing replacing themselves at this time.)*/
	const duplicateIndex = graph.findIndex(node => node.x === newNode.x && node.y === newNode.y);
	if (duplicateIndex === -1){ //case 1 - add
		graph.push(newNode);
		TERSE && console.log("%cCase: adding new point", "background: black; color: green");
		TERSE && console.log(`%c${nodeToString(newNode)}, nextNodes: ${newNode.nextNodes.reduce((paths, node) => paths += `"${node.pathSegment}", `, "")}`, "background: black; color: green");
	} else { //cases 2 or 3
		if (graph[duplicateIndex].pathLength > newNode.pathLength){//case 3: replace & adopt children.
			// console.log("%cCase: ORPHANING NODES", "background: navy; color: yellow");
			
			console.log("%cCase: ORPHANING NODES", "background: black; color: turquoise");
			console.log(`%c${nodeToString(graph[duplicateIndex])}, nextNodes: ${graph[duplicateIndex].nextNodes.reduce((paths, node) => paths += `"${node.pathSegment}", `, "")}`, "background: black; color: turquoise");
			
			let oldNode = graph.splice(duplicateIndex, 1, newNode)[0];//replace the dead node with the better node.
			oldNode.priorNode.nextNodes.splice(oldNode.priorNode.nextNodes.indexOf(oldNode), 1);//get rid of references to the dead node above it in the graph. The shortest path to this (x,y) path goes through newNode and it's priorNode, parentNode.
			oldNode.nextNodes.forEach(orphan => {//adopt the orphaned paths and update their paths
				orphan.priorNode = newNode;
				newNode.nextNodes.push(orphan);
				updatePathLengths(orphan);
			});
		} else {//case 2: ignore
			TERSE && console.log("%cCase: ignoring longer pathlength", "background: black; color: red");
			TERSE && console.log(`%c${nodeToString(graph[duplicateIndex])}, nextNodes: ${graph[duplicateIndex].nextNodes.reduce((paths, node) => paths += `"${node.pathSegment}", `, "")}`, "background: black; color: red");
			return graph[duplicateIndex];
		}
	}
	return newNode;
};

const getNextWayPoint = (startingNode, pathSegment) => {
	const newNode = new pathSegmentNode(pathSegment, startingNode.x, startingNode.y, startingNode);
	for (let i = 0; i < pathSegment.length; i++){
		switch(pathSegment[i]){
			case 'N':
				newNode.y--;
			break;
			case 'S':
				newNode.y++;
			break;
			case 'E':
				newNode.x++;
			break;
			case 'W':
				newNode.x--;
			break;
		}
	}
	return newNode;
}

const nodeToString = node => `(${node.x}, ${node.y})\t"${node.pathSegment}"\t${node.pathSegment.length} of ${node.pathLength}`;

const printGraph = graph => console.log(graph.reduce((graphString, node, i) => 
	graphString + `[${i}] ${nodeToString(node)}\n`
, ""));

const updatePathLengths = subTree => {
	// console.log(`subTree: ${subTree.pathSegment}[${subTree.pathSegment.length}] - old pathLen: ${subTree.pathLength},  new parent: ${subTree.priorNode.pathSegment} - parent pathLen: ${subTree.priorNode.pathLength}`);
	subTree.pathLength = subTree.pathSegment.length + subTree.priorNode.pathLength;
	// console.log(`new pathLength: ${subTree.pathLength}`);
	subTree.nextNodes.forEach(subSubTree => updatePathLengths(subSubTree));
};

const graphToGrid = (graph, graphRoot) => {
	console.log("graph fed to graphToGrid:", graph);
	const extents = graph.reduce(
		(boundaries, node) => {
			return {
				xMin: Math.min(boundaries.xMin, node.x),
				xMax: Math.max(boundaries.xMax, node.x),
				yMin: Math.min(boundaries.yMin, node.y),
				yMax: Math.max(boundaries.yMax, node.y),
			};
		}, 
		{
			xMin: Infinity,
			xMax: Number.NEGATIVE_INFINITY,
			yMin: Infinity,
			yMax: Number.NEGATIVE_INFINITY
		}
	);
	console.log("extents", extents);

	/* Starting values: we need to build in two-ended redundancies in our fill examples below */
	let numRows = extents.yMax - extents.yMin + 1;//+1 for fenceposting.
	let rowOffset = extents.yMin;// node.y - rowOffset === grid[index]
	let numCols = extents.xMax - extents.xMin + 1;
	let colOffset = extents.xMin;

	console.log(`${numCols} x ${numRows} grid to start.`);
	
	// const grid = Array(numRows).fill(null).map(row => Array(numCols).fill('?'));//the row fill value will change, but for now, '?' means unexplored.
	const grid = new Grid(numCols, numRows, colOffset, rowOffset);

	let node = graphRoot;
	let x = node.x-colOffset;
	let y = node.y-rowOffset;
	console.log("node", node);
	console.log(`(${x}, ${y}) after offsets`);
	grid.set(node.x, node.y, 'X');
	printGrid(grid);
	fillGrid(grid, graphRoot, 0, 0);
	return grid;
};

class Grid {
	constructor(numCols, numRows, colOffset, rowOffset){
		console.log("constructor called with:", numCols, numRows, colOffset, rowOffset);
		this._width = numCols;
		this._height = numRows;
		this.grid =  Array(this._height).fill(null).map(row => Array(this._width).fill('?'));//the row fill value will change, but for now, '?' means unexplored.
		this._xOffset = colOffset || 0;
		this._yOffset = rowOffset || 0;
	}
	//getters
	get numCols(){
		return this._width;
	}
	get numRows(){
		return this._height;
	}
	set(x, y, value){
		x -= this._xOffset;
		y -= this._yOffset; 
		TERSE && console.log(`attempting to set (${x}, ${y}) to ${value}`);
		/*NOTE: for the following adjustment codes, we have to adjust for each unit by which x is less than 0, not simply ++/--*/
		if (x < 0){
			this.grid.forEach(row => {for (let i=0; i<-x; i++)row.unshift('?');});//fill out the "negative" indices in each row
			this._width -= x; //increment grid width;
			this._xOffset += x;//decrement the offset further.
			x = 0;//because the previously negative index is now the first column.
		} else if (x >= this._width){//then grid.forEach(row => row.unshift('?')) and this._width++;
			let additionalCols = x - this._width + 1;//since last column is [this._width-1]
			this.grid.forEach(row => {for (let i=0; i<additionalCols; i++)row.push('?');}); //fill out the "missing" indices on the right-side of each row
			this._width += additionalCols;//increment grid width;
		}
		if (y < 0){//update rows after updating columns because we need a valid value for this._width
			for (let i=0; i<-y; i++) this.grid.unshift(Array(this._width).fill('?'));
			this._height -= y;
			this._yOffset += y;
			y = 0;
		} else if (y >= this._height){;
			let additionalRows = y - this._height + 1;//since last row is [this._height-1];
			for (let i=0; i<additionalRows; i++) this.grid.push(Array(this._width).fill('?'));
			this._height += additionalRows;
		}
		this.grid[y][x] = value;
	}
}

const fillGrid = (grid, pathNode, x, y) => {//x and y are the starting positions for the pathSegment.
	VERBOSE && console.log(`%cfillGrid called (${x}, ${y}) -> "${pathNode.pathSegment}"`, "background: yellow; color: black;");
	for (let i=0; i<pathNode.pathSegment.length; i++){
		switch (pathNode.pathSegment[i]){
			case 'N':
				// grid[--y][x] = '.';
				grid.set(x, --y, '.');
				break;
			case 'S':
				// grid[++y][x] = '.';
				grid.set(x, ++y, '.');
				break;
			case 'E':
				// grid[y][++x] = '.';
				grid.set(++x, y, '.');
				break;
			case 'W':
				// grid[y][--x] = '.';
				grid.set(--x, y, '.');
				break;
		}
		VERBOSE && console.log(`i:${i}, (${x}, ${y})`);
		TERSE && console.log(`${grid.numCols} x ${grid.numRows} grid:`);
		TERSE && printGrid(grid);
	}
	pathNode.nextNodes.forEach(
		child => fillGrid(grid, child, x, y)//now x and y should be the end of the pathsegment, aka the starting point for the children's paths.
	);
};

const printGrid = grid => console.log(grid.grid.reduce((gridString, row) => gridString += row.join("")+"\n", ""));

const part1 = (regexPattern) => {
	// console.log("regex:", regexPattern);
	// console.log("depth:", convertToDepths(regexPattern));

	const root = new pathSegmentNode("", 0, 0);
	const GRAPH = [];
	const leaves = parseRegex(regexPattern, root, GRAPH);
	console.log("tree", root);
	console.log("graph", GRAPH);
	console.log("leaves", leaves);
	// return "not yet solved";
	// console.log("num points", GRAPH.length, "num leaves", GRAPH.filter(node => node.nextNodes.length === 0).length);

	printGraph(GRAPH);
	const grid = graphToGrid(GRAPH, root);
	printGrid(grid);

	return (GRAPH.sort((a,b) => b.pathLength - a.pathLength)[0].pathLength);
	/* NOTE: An edge case this doesn't catch is if the farthest room is visted, then the path segment doubles back.*/
};

day20();