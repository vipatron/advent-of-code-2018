import { getInput } from '../helpers/helpers.js'
console.clear();
const VERBOSE = false;
const MODERATE = false || VERBOSE;
const TERSE = false || MODERATE;

const INPUT = getInput(20);

// const INPUT = "^WNE$";
// const INPUT = "^ENWWW(NEEE|SSE(EE|N))$";

// const INPUT = "^ENWWW(NEEE|SSE)$"; //simple branch that isn't already on the graph
// const INPUT = "^ENNWSWW(NEWS|)$"; //simple branch that is.

// const INPUT = "^ENNWSWW(NEWS|)SSSEEN(WNSE|)EE(SWEN|)NNN$";

const day20 = () => {
	const trimmedInput = INPUT.trim().slice(1, -1);

	const puzzle1result = part1(trimmedInput);
	const puzzle2result = "tbd";
	
	console.log("puzzle1: ", puzzle1result);
	console.log("puzzle2: ", puzzle2result);
}

const part1 = (regex) => {
	let [x, y] = [0, 0];
	const grid = new MapGrid(1, 1);
	grid.set(x, y, 'X');
	grid.print("green");

	const graph = [];
	const root = parseSubTree(grid, graph, regex);
	grid.finalizeMap();
	grid.print("red");

	const graphWithLeaves = graph.slice();
	graph.forEach(node => graphWithLeaves.push(...node.nextNodes));
	graphWithLeaves.sort((a, b) => b.pathLength - a.pathLength);
	return graphWithLeaves[0].pathLength;
};

/** @function parseSubTree
 * @param  {MapGrid} grid the MapGrid onto which to draw the map
 * @param  {pathSegmentNode[]} graph the whole tree/graph
 * @param  {string} regex the regex describing this subtree
 * @param  {pathSegmentNode} [priorSegmentNode=null] the parent node of this subtree.
 * @returns {pathSegmentNode} the node describing the optimal path to the root of this subTree.
 */
function parseSubTree (grid, graph, regex, priorSegmentNode=null){
	let optimalNodeToRootPoint;
	MODERATE && console.log(`%cparseSubTree called (${priorSegmentNode ? nodeToString(priorSegmentNode) : priorSegmentNode}) -> "${regex}"`, "background: orange; color: black;");
	let {x, y} = priorSegmentNode ? priorSegmentNode : {x:0, y:0};

	//find the nonbranching part (trunk) of the subtree.
	const firstOpenParenIndex = regex.indexOf('(');
	const nonBranchingSegment = firstOpenParenIndex > -1 ? regex.slice(0, firstOpenParenIndex) : regex;
	const subTreeRoot = parseSegment(grid, nonBranchingSegment, priorSegmentNode);

	//add the node to the graph if it's not a duplicate, keeping it sorted by ascending pathLength.
	const duplicateIndex = graph.findIndex(node => node.x === subTreeRoot.x && node.y === subTreeRoot.y);
	VERBOSE && console.log("duplicate index:", duplicateIndex);
	if (duplicateIndex === -1) {//point isn't in the graph at all, so add it.
		MODERATE && console.log(`%c${subTreeRoot.pathSegment} added to graph as child of ${subTreeRoot.priorNode && subTreeRoot.priorNode.pathSegment}`, "background: brown; color: white");
		graph.push(subTreeRoot);
		graph.sort((a,b) => a.pathLength - b.pathLength);
		optimalNodeToRootPoint = subTreeRoot;
	} else {//point is in the graph, so...
		if (subTreeRoot.pathLength < graph[duplicateIndex].pathLength){//this option is the shortest path to that point. (shouldn't happen [TODO 1: IN A TREE ONLY], since the length of any option = priorNode's length + segment.length, so option.length >= priorNode.length)
			console.log("%cSOMEHOW THIS IS THE SHORTEST PATH - need to code this", 'background: red; color: darkgreen');//optimalNodeToRootPoint will be undefined
			console.log("subTreeRoot:", nodeToString(subTreeRoot));
			console.log("subTreeRoot Node:", subTreeRoot);
			console.log("duplicate:", nodeToString(graph[duplicateIndex]));
			console.log("duplicate Node:", graph[duplicateIndex]);
			
			grid.printWithPoint(subTreeRoot.x, subTreeRoot.y);
			printGraph(graph);
		} else {//this is a redundant way to get to the same point. It should exist as a leaf of the graph without being explored itself?
			MODERATE && console.log(`%c"${subTreeRoot.pathSegment}" will remain a leaf of ${nodeToString(priorSegmentNode)}`, "background: darkgreen; color: white");
			optimalNodeToRootPoint = graph[duplicateIndex];
		}
	}
	MODERATE && console.log("%cAfter nonBranchingSegment parsed:", "color: cornflowerblue");
	MODERATE && printGraph(graph);

	MODERATE && grid.print();

	if (firstOpenParenIndex > -1){//if there are options/branches...
		//figure out where the top-level split points are by iterating over regex
		let unmatchedOpenParens = 0;
		let matchingToplevelParenIndex;
		const topLevelAlternatorIndices = [];
		let i = firstOpenParenIndex;		
		while (i < regex.length){
			// console.log(`regex[${i}]: ${regex[i]}`); //DEBUG
			switch(regex[i]){
				case '(':
					unmatchedOpenParens++ //increase the depth counter
				break;
				case ')':
					if (! --unmatchedOpenParens){//decrement depth counter and see if it is now 0.
						matchingToplevelParenIndex = i;
					}
				break;
				case '|':
					if (unmatchedOpenParens === 1){//we're at the top level of options.
						topLevelAlternatorIndices.push(i);
					}
				break;
			}
			if (matchingToplevelParenIndex) break; //only executes if matching top-level closed-paren found.
			i++;
		}

		// console.log("alternatorindices:", topLevelAlternatorIndices); //DEBUG
		
		//take the topLevelAlternatorIndices[] and create an options[].
		topLevelAlternatorIndices.push(matchingToplevelParenIndex);//acts like the last right-hand fencepost to split the options.
		let lastSplitPoint = firstOpenParenIndex;
		let options = [];
		for (let i=0; i<topLevelAlternatorIndices.length; i++){
			options.push(regex.slice(lastSplitPoint+1, topLevelAlternatorIndices[i]));
			lastSplitPoint = topLevelAlternatorIndices[i];
		}
		TERSE && console.log("options:", options);

		//parse those options starting from the next available branchpoint.
		options.forEach(option => {
			const optionNode = parseSubTree(grid, graph, option, subTreeRoot);//subTreeRoot's nextNodes will include all the options because of this line, even if they won't become active leaves themselves.
			optionNode.activeLeaf = true;
			
			MODERATE && console.log(`%creturned from parseSubTree call on "${option}"`, "background: red;")
			VERBOSE && console.log("option node just created/chosen:", nodeToString(optionNode));
			VERBOSE && console.log("graph upon return from parseSubTree on that node:");
			VERBOSE && printGraph(graph);
		});
		//deal with the remainder of the regex, which describes the identical relevant paths from every leaf in the graph. N.B.: If there were no options, there is by definition no remainder after them.
		const remainder = regex.slice(matchingToplevelParenIndex+1);
		if (remainder !== ""){//if there is indeed a remainder after the options...
			const activeLeaves = graph.filter(node => node.activeLeaf);
			VERBOSE && console.log("active leaves:", activeLeaves);
			activeLeaves.forEach(leaf => {
				leaf.activeLeaf = false;
				parseSubTree(grid, graph, remainder, leaf);				
			});
		}
	}
	return optimalNodeToRootPoint;
}


/** @function parseSegment
 * @param {MapGrid} grid the MapGrid onto which to draw the map.
 * @param {string} segment the [NEWS]* string to parse
 * @param {pathSegmentNode} [priorSegmentNode=null] the parent node of the returned node.
 * @returns {pathSegmentNode} The node representing parameter "segment", optionally as a child of priorSegmentNode
 */
const parseSegment = (grid, segment, priorSegmentNode=null) => {//parses the segment, filling in the map grid and returning the pathSegmentNode
	let lastX = priorSegmentNode ? priorSegmentNode.x : 0;
	let lastY = priorSegmentNode ? priorSegmentNode.y : 0;
	TERSE && console.log(`%cparseSegment called (${lastX}, ${lastY}) -> "${segment}"`, "background: yellow; color: black;");

	const currentNode = new pathSegmentNode(segment, priorSegmentNode);
	
	let newRoomsExplored = 0;//equivalent to the # of doors opened along this segment, assuming non of the path segments cross
	let gridValues = [];
	for (let i=0; i<segment.length; i++){
		let gridValue = null;//because undefined is one of the values that is a positive test.
		switch (segment[i]){
			case 'N':
				gridValue = grid.get(currentNode.x, --currentNode.y);
				break;
			case 'S':
				gridValue = grid.get(currentNode.x, ++currentNode.y);
				break;
			case 'E':
				gridValue = grid.get(++currentNode.x, currentNode.y);
				break;
			case 'W':
				gridValue = grid.get(--currentNode.x, currentNode.y);
				break;
		}
		// grid.print("orange");
		gridValues.push(`(${currentNode.x}, ${currentNode.y}): '${gridValue}'`);
		// console.log("%cgrid values:","background: black", gridValues);

		if (gridValue === undefined || gridValue === ' ') newRoomsExplored++;
		grid.set(currentNode.x, currentNode.y, '.');
		// console.log("x:", x, "y:", y, "lastx:", lastX, "lastY:", lastY);
		grid.openDoorsBetween(lastX, lastY, currentNode.x, currentNode.y);
		lastX = currentNode.x;
		lastY = currentNode.y;
		VERBOSE && grid.print("magenta");
	}
	if (newRoomsExplored < currentNode.pathSegment.length){
		MODERATE && console.log(`%cThis path backtracks. SWAP OUT THE LENGTH ${currentNode.pathSegment.length} with ${newRoomsExplored}`, "background: navy");
		// console.log("grid values:", gridValues);
		currentNode.updateSegmentLength(newRoomsExplored);
	}
	return currentNode;
};

/* UTILITY DEBUG FUNCTIONS */
const nodeToString = node => `(${node.x}, ${node.y})\t"${node.pathSegment}"\t${node.pathSegment.length} of ${node.pathLength}\tnextNodes: [${node.nextNodes.map(next => '"'+next.pathSegment+'"').join(', ')}]${node.activeLeaf ? "*" : ""}`;

const printGraph = graph => console.log("%c"+graph.reduce((graphString, node, i) => 
	graphString + `[${i}] ${nodeToString(node)}\n`
, ""), "background: #909;");

/* OBJECT DECLARATIONS */

/** @function pathSegmentNode
 * @param  {string} pathString [NEWS]* string for this segment
 * @param  {pathSegmentNode} [parentNode=null] the parent node of this one in the graph
 * @param  {string[]} [childNodes=[]] an array of nodes that are children to this one
 */
function pathSegmentNode (pathString, parentNode = null, childNodes = []){
	this.pathSegment = pathString;
	this.pathLength = pathString.length + (parentNode ? parentNode.pathLength : 0);
	this.startX = parentNode ? parentNode.x : 0;
	this.startY = parentNode ? parentNode.y : 0;
	this.x = this.startX;
	this.y = this.startY;
	this.priorNode = parentNode;
	if (this.priorNode) this.priorNode.nextNodes.push(this);
	this.nextNodes = childNodes;
}
pathSegmentNode.prototype.updateSegmentLength = function(newSegmentLength){
	this.pathLength = newSegmentLength + (this.priorNode ? this.priorNode.pathLength : 0);
};

class MapGrid {
	constructor(numCols, numRows, colOffset=0, rowOffset=0){
		// console.log("constructor called with:", numCols, numRows, colOffset, rowOffset);
		this._width = 2 * numCols + 1;//the actual width of the 2-d array.
		this._height = 2 * numRows + 1;
		this._grid =  Array(this._height).fill(null).map(
			(row, rowIndex) => rowIndex%2 === 0 
			? Array(this._width).fill('?').map((val, i) => i%2 === 0 ? '#' : val)//even rows (walls & doors)
			: Array(this._width).fill(' ').map((val, i) => i%2 === 0 ? '?' : val)//odd rows (rooms & doors at first)
		);//the row fill value will change, but for now, '?' means door or wall, and ' ' means unexplored room.
		this._xOffset = colOffset;
		this._yOffset = rowOffset;
	}
	//getters
	get numCols(){
		return (this._width-1)/2; //width = 2*numCols + 1 (fenceposted walls around cols)
	}
	get numRows(){
		return (this._height-1)/2;
	}
	/*This MapGrid object models a "virtual 2-d array" MapGrid[x][y] onto a real 2-d array named grid. Two separate transformations on the given (x,y) coordinates are peformed:
	1. The (x, y) gets added to (-xOffset, -yOffset), which is the same as the (startingX, startingY), so that if the virtual grid positions range from [(startX, startY) to (startX + numCols-1, startY + numRows-1)], they get mapped onto the virtual range [(0, 0) to (numCols-1, numRows-1)]
	2. The virtual grid is then mapped onto the real grid (which is enclosed by fenceposted walls) by the function (rowindex, colIndex) = (2x+1, 2y+1), so the real range is [(1,1) to (2*numCols-1, 2*numRows-1)]
	*/
	set(x, y, value){
		x -= this._xOffset; //given will now be indices in the virtual grid
		y -= this._yOffset;
		/*NOTE: for the following adjustment codes, we have to adjust for each unit by which x is less than 0, not simply ++/--*/
		if (x < 0){
			this._grid.forEach((row, rowIndex) => {for (let i=0; i<-x; i++)row.unshift(
				...(rowIndex%2 === 0
				? ['#', '?']//wall row
				: ['?', ' '])//room row
			);});//fill out the "negative" indices in each row
			this._width -= 2*x; //increment grid width;
			this._xOffset += x;//decrement the offset further.
			x = 0;//because the previously negative index is now the first column.
		} else if (x >= this.numCols){//then grid.forEach(row => row.push) and this._width++;
			let additionalCols = x - this.numCols + 1;//since last "virtual column" is [this.numCols-1]
			this._grid.forEach((row, rowIndex) => {for (let i=0; i<additionalCols; i++)row.push(
				...(rowIndex%2 === 0
					? ['?', '#']//wall row
					: [' ', '?'])//room row
			);}); //fill out the "missing" indices on the right-side of each row
			this._width += 2*additionalCols;//increment grid width;
		}
		if (y < 0){//update rows after updating columns because we need a valid value for this._width
			for (let i=0; i<-y; i++) this._grid.unshift(
				Array(this._width).fill('?').map((val, i) => i%2 === 0 ? '#' : val), //1st or odd row to unshift (walls & doors row)
				Array(this._width).fill(' ').map((val, i) => i%2 === 0 ? '?' : val), //0th or even row to unshift (room row)
			);

			this._height -= 2*y;
			this._yOffset += y;
			y = 0;
		} else if (y >= this.numRows){
			let additionalRows = y - this.numRows + 1;//since last "virtual row" is [this.numRows-1];
			for (let i=0; i<additionalRows; i++) this._grid.push(
				Array(this._width).fill(' ').map((val, i) => i%2 === 0 ? '?' : val), //0th or even row to push (room row)
				Array(this._width).fill('?').map((val, i) => i%2 === 0 ? '#' : val), //1st or odd row to push (walls & doors row)
			);
			this._height += 2*additionalRows;
		}
		this._grid[2*y+1][2*x+1] = value;
	}

	openDoorsBetween(x1, y1, x2, y2){
		// console.log(arguments);
		// console.log("xOffset", this._xOffset, "yOffset", this._yOffset);
		for (let i in Object.keys(arguments)){
			// console.log(i, i%2=== 0 ? 'even row': 'odd row');
			arguments[i] -= i%2=== 0 ? this._xOffset : this._yOffset; //put the coordinates into the virtual grid [0 ... len)
			arguments[i] = 2 * arguments[i] + 1;//map the virtual coordinates onto "real grid" coordinates.
		}
		// console.log(arguments);
		[x1, y1, x2, y2] = arguments;
		// console.log("x1:", x1, "y1:", y1, "x2:", x2, "y2:", y2);
		this._grid[(y1+y2)/2][(x1+x2)/2] = y1 === y2 ? '|' : '-';
	}

	get(x, y){
		x -= this._xOffset;
		y -= this._yOffset
		if (x<0 || x>=this.numCols || y<0 || y >=this.numRows){
			// console.log("get mandates undefined. x:", x, "y:", y, "#rows:", this.numCols, "#cols:", this.numRows);
			return undefined;
		} else {
			return this._grid[2*y+1][2*x+1];
		}
	}

	print(color = "turquoise"){
		const formattingStrings = ["background: none"];
		const rawGridString = this._grid.reduce((gridString, row) => gridString += row.join("")+"\n", "");
		const formattedOutput = "%c"+rawGridString.replace(/[-|X\.]/g, match => {
			formattingStrings.push("background: "+color+"; color: black;", "background: default; color: default;");
			return "%c"+match+"%c";
		});
		console.log(formattedOutput, ...formattingStrings);
	}

	printWithPoint(x, y, pointColor = "red", color = "turquoise"){
		x -= this._xOffset;
		y -= this._yOffset;
		x = 2*x+1;
		y = 2*y+1;
		
		let PointIndexInGridString = y * (this._width+1) + x //each row will include (width + 1) chars because of the newline.
		const rawGridString = this._grid.reduce((gridString, row) => gridString += row.join("")+"\n", "");

		const formattingStringsPrePoint = ["background: none"];
		const formattedOutputPrePoint = "%c"+rawGridString.slice(0, PointIndexInGridString).replace(/[-|X\.]/g, match => {
			formattingStringsPrePoint.push("background: "+color+"; color: black;", "background: default; color: default;");
			return "%c"+match+"%c";
		});
		const formattedPoint = `%c${rawGridString[PointIndexInGridString]}%c`;
		const pointFormattingStrings = ["background: "+pointColor+"; color: black;", "background: default; color: default;"]
		const formattingStringsPostPoint = [];
		const formattedOutputPostPoint = rawGridString.slice(PointIndexInGridString+1).replace(/[-|X\.]/g, match => {
			formattingStringsPostPoint.push("background: "+color+"; color: black;", "background: default; color: default;");
			return "%c"+match+"%c";
		});

		console.log(formattedOutputPrePoint + formattedPoint + formattedOutputPostPoint,
			 ...formattingStringsPrePoint, ...pointFormattingStrings, ...formattingStringsPostPoint);
	}

	finalizeMap(){
		this._grid.forEach((row, i , grid) => grid[i] = row.map(col => col === '?' ? '#' : col))
	}
}

day20();