import { getInput, makeLetterArray} from '../helpers/helpers.js'
console.clear();

const INPUT = getInput(8);
// const INPUT = "2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2";

function Node(label){
	this.label = label;
	this.children = [];
	this.metadata = [];
}

const buildTree = (tokens) => {
	const letters = makeLetterArray(52);
	return buildTreeWithLetterNodes(letters, tokens);
}

const buildTreeWithLetterNodes = (letters, tokens) => {
	let numChildren = tokens.shift();
	let numMetadata = tokens.shift();
	let newNode = new Node(letters.shift());
	for (let c=0; c<numChildren; c++){
		newNode.children.push(buildTreeWithLetterNodes(letters, tokens));
	}
	newNode.metadata.push(...tokens.splice(0, numMetadata).map(chr => parseInt(chr)));
	return newNode;	
}

const sumMetadata = tree => tree.metadata.reduce((a, b) => a + b) + tree.children.map(subtree => sumMetadata(subtree)).reduce((a, b) => a + b, 0);

const value = tree => {
	console.log("tree", tree);
	if (tree.children.length){
		return tree.metadata.map(idx => idx <= tree.children.length ? value(tree.children[idx-1]) : 0).reduce((a,b) => a+b);
	} else { // no kids
		return tree.metadata.reduce((a,b) => a+b);
	}
}

const day8 = () => {
	const tokens = INPUT.split(" ");
	const tree = buildTree(tokens);
	console.log(tree);

	const puzzle1result = sumMetadata(tree);
	const puzzle2result = value(tree);
	
	console.log("puzzle1: ", puzzle1result);
	console.log("puzzle2: ", puzzle2result);
}

day8();