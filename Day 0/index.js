import { getInput } from '../helpers/helpers.js'
console.clear();

const INPUT = getInput(0);

const day0 = () => {
	const tokens = INPUT.trim().split("\n");

	const puzzle1result = "tbd";
	const puzzle2result = "tbd";
	
	console.log("puzzle1: ", puzzle1result);
	console.log("puzzle2: ", puzzle2result);
}

day0();