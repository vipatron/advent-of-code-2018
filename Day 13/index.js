import { getInput } from '../helpers/helpers.js'
console.clear();

const INPUT = getInput(13);

const printTrack = (track, carts) => {
	let result = track.slice().map((row, i) => track[i].slice());
	if (carts) {
		carts.forEach(cart => {
			result[cart.y][cart.x] = "%c"+cart.heading+"%c";
		});
	}
	// console.log("Prefix%cHeader%cSuffix%cHeader", "background-color: black; color: magenta;", "", "background-color: black; color: magenta;");
	let stylings = Array(carts.length*2);
	for (let i=0; i<stylings.length; i+=2){
		stylings[i] = "background-color: black; color: magenta;";
		stylings[i+1] = "";
	}
	// console.log(stylings);
	console.log(result.reduce((string, row) => string + row.join("") + "\n", ""), ...stylings);
}

const day13 = () => {

	//Lookup table to create the initial track object.
	const initialTrackReplacement = {
		'>': '-',
		'<': '-',
		'^': '|',
		'v': '|'
	}
	const intersectionOrder = ['left', 'straight', 'right'];
	//nested object used to turn the cart's heading. The first property is the turn direction, the second is the current heading.
	const newHeadingAtIntersection = { //[turnDirection][currentHeading]
		'left': {
			'>': '^',
			'<': 'v',
			'^': '<',
			'v': '>'
		},
		'right': {
			'>': 'v',
			'<': '^',
			'^': '>',
			'v': '<'
		}
	};

	// for horizontal segments of track, (/ & \ === left & right), but for verticals, it's the inverse. 
	// this could be achieved by calling the newHeadingAtIntersection object above with turnDirection determined by a pair of nested conditionals, one for horizontality and another for the track segment, but I'd be sacrificing simplicity of code below to avoid an extra 8-property (2x4 nested) object literal, which probably performs better.
	const newHeadingAtTurn = { //[trackTurnSegment][currentHeading]
		'/': {
			'>': '^',
			'<': 'v',
			'^': '>',
			'v': '<'
		},
		'\\': {
			'>': 'v',
			'<': '^',
			'^': '<',
			'v': '>'
		}
	};
	
	// parse the input, then iterate over the grid, creating cart objects, and replacing them in the track with the actual track feature (horizontal or vertical line)
	const track = INPUT.split("\r\n").map(row => row.split(""));
	let carts = [];
	for (let row = 0; row < track.length; row++){
		for (let col = 0; col < track[row].length; col++){
			if (/[\^v><]/g.test(track[row][col])){
				//cart objects must have a position (x,y), current heading string '>', e.g., next turn direction (L, S, R) for use at the next intersection, and maybe(probably not) a track variable to keep "track" of which segment they are on top of for the purposes of printing?
				carts.push({
					x: col,
					y: row,
					heading: track[row][col],
					atNextIntersection: 0 //index of intersectionOrder[] where the direction resides.
				});
				track[row][col] = initialTrackReplacement[carts[carts.length-1].heading];
			}
		}
	}
	
	//during each "tick" (outer for loop) ...
	let tick = 0;
	let firstCollision = { found: false };
	while (carts.length > 1 && tick < 100000){
		//define a convenience boolean to determine when to print the state of the track to the console.
		let collisionDuringThisTick = false;

		// iterate through the carts
		for (let i=0; i<carts.length; i++){
			//advance them in the direction they are facing.
			switch (carts[i].heading){
				case '>':
					carts[i].x++;
					break;
				case '<':
					carts[i].x--;
					break;
				case '^':
					carts[i].y--;
					break;
				case 'v':
					carts[i].y++;
					break;
			}
			//readjust the direction (or not) as the next track segment determines:
			let nextTrackSegment = track[carts[i].y][carts[i].x];
			switch (nextTrackSegment){
				case '+':
					//for an intersection, face in the direction of the "next turn" variable.  (using the newHeadingAtIntersection lookup object) and set the behavior at the next intersection.
					let directionAtIntersection = intersectionOrder[carts[i].atNextIntersection];
					if (directionAtIntersection !== "straight"){
						carts[i].heading = newHeadingAtIntersection[directionAtIntersection][carts[i].heading]; //go left or right, and set heading according to lookup table.
					}
					carts[i].atNextIntersection = ++carts[i].atNextIntersection % intersectionOrder.length;
					break;
				case '/':
				case '\\':
					//for a turn, face in the direction of a turn (using the newHeadingAtTurn lookup object)
					carts[i].heading = newHeadingAtTurn[nextTrackSegment][carts[i].heading];
					break;
				default:
					break;
			}

			//iterate over the other carts and check if the change in cart position caused a collision.
			for (let j=0; j<carts.length; j++){
				if (j!==i && !carts[j].crashed){ //don't check the current cart, or any carts that have already collided
					if (carts[i].x === carts[j].x && carts[i].y === carts[j].y){ //collision found.
						//if first collision found, set result object for part 1
						if (!firstCollision.found){ //solves for part 1
		
							/*** WORK ON SYNTAX */
							// collision = ({ x, y } = carts[i]);
		
							firstCollision = {
								x: carts[i].x,
								y: carts[i].y,
								found: true
							};
						}
						//take note of the carts we will remove
						carts[i].crashed = true;
						carts[j].crashed = true;
						//set a flag to print the track after iterating through the carts
						collisionDuringThisTick = true;

						console.log(" *** COLLISION!!! @ tick:", tick, "*** ");
						console.log("at indices:", i, "&", j+": (",carts[i].x,",",carts[i].y,")");
					}
				}
			}
			
		}
		// sort the list of carts by row(y), then by column(x)
		carts.sort((cartA, cartB) => cartA.y - cartB.y || cartA.x - cartB.x); // if ∆y==0, then the second expression after the OR evaluates.

		//remove the carts that collided.
		carts = carts.filter(cart => !cart.crashed);
		if (collisionDuringThisTick){
			printTrack(track, carts);
		}
		tick++;
	}
	const puzzle1result = firstCollision;
	const puzzle2result = carts.length === 1 ? carts[0] : "not found";
	
	console.log("puzzle1: ", puzzle1result);
	console.log("puzzle2: ", puzzle2result);
}

day13();