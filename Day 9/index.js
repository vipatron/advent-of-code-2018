// import { getInput } from '../helpers/helpers.js'
console.clear();

// const INPUT = getInput(9);
// const INPUT = "9 players; last marble is worth 25 points";
// const INPUT = "9 players; last marble is worth 100 points";
const INPUT = "441 players; last marble is worth 7103200 points";

// const INPUT="10 players; last marble is worth 1618 points: high score is 8317";
// const INPUT="13 players; last marble is worth 7999 points: high score is 146373";
// const INPUT="17 players; last marble is worth 1104 points: high score is 2764";
// const INPUT="21 players; last marble is worth 6111 points: high score is 54718";
// const INPUT="30 players; last marble is worth 5807 points: high score is 37305";

//define a doubly-linked list Node
function Node (value, previousNode, nextNode) {
	this.value = value;
	this.previousNode = previousNode;
	this.nextNode = nextNode;
};

const printCircle = (currentMarble) => {
	printCircleFromNode(currentMarble, currentMarble);
}
const printCircleFromNode = (currentMarble, startMarble) => {
	let result = "";
	let lastNode = startMarble;
	let nodeToPrint = lastNode.previousNode;
	while (lastNode !== startMarble || result.length === 0){
		result = (nodeToPrint === currentMarble ? `(${nodeToPrint.value})` : nodeToPrint.value) + " " + result;
		lastNode = nodeToPrint;
		nodeToPrint = nodeToPrint.previousNode;
	}
	
	console.log(result);
};

const day9 = () => {
	const [, numPlayers, lastMarble] = INPUT.match(/(\d+) players.* (\d+) points/).map(str => parseInt(str));
	console.log(`${numPlayers} players, ${lastMarble} marbles`);

	//define a "current marble" pointer to be the pointer to the linked list of marble nodes, and make the list circular.
	let currentMarble = new Node(0);
	currentMarble.nextNode = currentMarble.previousNode = currentMarble;
	
	let zerothMarble = currentMarble;
	
	// define player index starting at -1.(b/c of implementation in for loop below)
	let currentPlayer;
	let playerScores = Array(numPlayers).fill(0);
	
	// for each marble = [1, lastMarble]...
	for (let marble = 1; marble <= lastMarble; marble++){
		if ( marble % 23 === 0 ) { // if marble is a multiple of 23
			// console.log("MARBLE 23n");

			currentPlayer = marble % numPlayers // the exact value doesn't matter, just that we can assign to a consistent slot in playerScores[]. (This assignment could happen outside this if block, but it would do this 22 unnecessary times for each useful one.)
			
			// keep score: move the currentMarble pointer back as part of it
			playerScores[currentPlayer] += marble;
			for (let i=0; i<7; i++){
				currentMarble = currentMarble.previousNode;
			}
			playerScores[currentPlayer] += currentMarble.value;
			
			//splice out the node, and join up the "cut ends"
			currentMarble.previousNode.nextNode = currentMarble.nextNode;
			currentMarble.nextNode.previousNode = currentMarble.previousNode;
			
			//move the currentMarble pointer onto the node CW of the one spliced out.
			currentMarble = currentMarble.nextNode;
		} else { // "normal" marble placement for non-multiples.
			let firstCW = currentMarble.nextNode;
			let secondCW = firstCW.nextNode;
			//Node (value, previousNode, nextNode)
			currentMarble = new Node(marble, firstCW, secondCW);
			firstCW.nextNode = currentMarble;
			secondCW.previousNode = currentMarble;
		}
		// printCircleFromNode(currentMarble, zerothMarble);
	}
	// console.log("scores", playerScores);

	const puzzle1result = playerScores.reduce((max, score) => Math.max(max, score), Number.NEGATIVE_INFINITY);
	const puzzle2result = "tbd";
	
	console.log("puzzle1: ", puzzle1result);
	console.log("puzzle2: ", puzzle2result);
}

console.time("LinkedList");
day9();
console.timeEnd("LinkedList");