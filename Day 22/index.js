// import { getInput } from '../helpers/helpers.js'
console.clear();

// const INPUT = getInput(22);
const INPUT = "depth: 5355\ntarget: 14,796";
const VERBOSE = false;
const TERSE = true || VERBOSE;

const day22 = () => {
	const tokens = INPUT.trim().split("\n");
	const depth = parseInt(tokens[0].match(/\d+/)[0]);
	// const depth = 510;
	const [x,y] = tokens[1].match(/\d+/g).map(num => parseInt(num));

	const erosionLevel = geoIndex => (geoIndex + depth) % 20183;
	const typeString = erosionLevel => ['.', '=', '|'][erosionLevel%3];
	const region = geoIndex => ({
		geoIndex,
		erosion: erosionLevel(geoIndex),
		type: typeString(erosionLevel(geoIndex))
	});

	// const puzzle1result = part1(510, 10, 10);//example
	const puzzle1result = part1(depth, x, y);
	console.time("part 2");
	// const puzzle2result = part2(10, 10);
	const puzzle2result = part2(x, y);
	console.timeEnd("part 2");

	console.log("puzzle1: ", puzzle1result);
	console.log("puzzle2: ", puzzle2result);

	function part2(targetX, targetY){
		const map = {};
		// VERBOSE && window.alert(`called with (${targetX}, ${targetY}), depth: ${depth}`);
		const corner = getRegion(targetX,targetY);
		// console.log(corner);

/*
		const mapRowToString = (row) => Object.entries(row).sort((a,b) => a[0] - b[0]).map(pointEntry => pointEntry[1].type).join("");
		const mapString = Object.entries(map).sort((a,b) => a[0] - b[0]).map(
			rowEntry => mapRowToString(rowEntry[1])
		).join("\n");
		console.log(mapString);
*/

		let currentNode = Object.create(map[0][0]);
		currentNode.time = 0;
		currentNode.equipment = "torch";
		// console.log("currentNode.prototype is the original point?", currentNode.__proto__ === map[0][0]);
		const queue = [ currentNode ];
		const solvedGraph = {};
		let solvedCount = 0;
		// console.log("queue's initial object", queue[0]);

		const printNodeArray = (arr, color="default") => console.log("%c"+arr.reduce(
			(str, node) => str+`[${node.time}+${node.heuristic} = ${node.time + node.heuristic}]: (${node.x}, ${node.y}) - ${node.equipment}\n`
			, ""
		), "background: "+color);
		
		function printSolved(color="default"){
			let xObjects = Object.values(solvedGraph);
			let yObjects = xObjects.reduce((yObjs, xObj) => [...yObjs, ...Object.values(xObj)], []);
			let allNodes = yObjects.reduce((all, yObj) => [...all, ...Object.values(yObj)], []);
			printNodeArray(allNodes, color);
		}

		const disallowed = {
			'.': "neither",
			'=': "torch",
			'|': "climbing gear"
		};
		//check whether equipment isn't on the disallowed list for that type of region.
		const allowsEquipment = (region, equipment) => equipment !== disallowed[region.type];
		
		const switchEquipment = (regionType, equipment) => ["neither", "torch", "climbing gear"].filter(
			next => next !== equipment && next !== disallowed[regionType]
		)[0];

		function insertSolved(node){
			let xObject = solvedGraph[node.x] || (solvedGraph[node.x] = {});
			let yObject = xObject[node.y] || (xObject[node.y] = {});
			yObject[node.equipment] = node;
		}
		function isSolved(node){
			return solvedGraph[node.x] && solvedGraph[node.x][node.y] && solvedGraph[node.x][node.y][node.equipment];
		}

		let foundPathToTarget = false;
		let counter = 0;
		while (!foundPathToTarget){// && counter < 600){
			// VERBOSE && console.log("round", ++counter);
			// {console.log("round", ++counter); printNodeArray([currentNode], "teal");console.log("q:",queue.length,"s:",solvedCount);}
			if (++counter %1000 === 0 && TERSE && !VERBOSE) {console.log("round", counter); printNodeArray([currentNode], "teal");}//console.log("q:",queue.length,"s:",solvedCount);}
			currentNode = queue.shift();
			// VERBOSE && printNodeArray([currentNode], "teal");
			if (!isSolved(currentNode)){
				// solvedGraph.push(currentNode);
				insertSolved(currentNode);
				solvedCount++;
				VERBOSE && printSolved("darkred");
				if (currentNode.x === targetX && currentNode.y === targetY && currentNode.equipment === "torch"){//found the target node.(targetX/Y)
					foundPathToTarget = true;
					break;
				}

				const adjacentRegions = [
					getRegion(currentNode.x+1, currentNode.y),
					getRegion(currentNode.x-1, currentNode.y),
					getRegion(currentNode.x, currentNode.y+1),
					getRegion(currentNode.x, currentNode.y-1),
				];
				
				const possibleMoves = adjacentRegions.filter(
					region => region && allowsEquipment(region, currentNode.equipment)//region exists && can move into the adjacent region
				).map(//move there, and mark the passage of time.
					region => Object.assign(
						Object.create(region),
						{
							time: currentNode.time + 1,
							equipment: currentNode.equipment,
							priorNode: currentNode
						}
					)
				);
				possibleMoves.push(//stay in the same place, but switch equipment
					Object.assign(
						Object.create(currentNode.__proto__),
						{
							time: currentNode.time + 7,
							equipment: switchEquipment(currentNode.type, currentNode.equipment),
							priorNode: currentNode
						}
					)
				);
	
				queue.push(...possibleMoves.filter(
					nodeToMoveTo => !isSolved(nodeToMoveTo)
				));
				queue.sort((a,b) => a.time+a.heuristic - (b.time + b.heuristic) || a.heuristic-b.heuristic);//keep queue prioritized by time.
			} /*else {
				console.log("%c CURRENT NODE IN SOLVED???", "background: yellow; color: navy");
				printNodeArray([currentNode], "NAVY")
			}*/
			VERBOSE && printNodeArray(queue, "darkgreen");
		}
		console.log("target?:", currentNode);
		return currentNode.time;
		
		function getRegion(x,y){//will return undefined for negative indices
			let regionPoint = map[y] && map[y][x];

			if (!regionPoint){//then either the row map[y] doesn't exist, or the point map[y][x] doesn't exist. Fill in map.
				//for each row
				for (let row = 0; row <= y; row++){
					//for each col
					for (let col = 0; col <= x; col++){
						regionPoint = (map[row] || (map[row] = {}))[col]//ensure that row exists, then try to access [col] in the row.
						if (!regionPoint){
							//if doesn't already exist, make it according to the rules. (no way to calculate (x>0, y>0) without ensuring the whole rectangle up to it exists.)
							if (row === targetY && col === targetY){//target coordinates.
								regionPoint = region(0);
							} else if (row === 0){
								regionPoint = region(16807 * col);
							} else if (col === 0){
								regionPoint = region(48271 * row);
							} else {
								//we can guarantee that (row-1) and (col-1) exist at this point.
								regionPoint = region(map[row-1][col].erosion * map[row][col-1].erosion);
							};
							regionPoint.x = col;
							regionPoint.y = row;
							regionPoint.heuristic = heuristicFunction(col, row);
							map[row][col] = regionPoint
						}
					}
				}
			}

			return regionPoint;
		}

		function heuristicFunction(x, y){
			const dx = targetX - x;
			const dy = targetY - y;
			return Math.abs(dx) + Math.abs(dy) + .001 * Math.abs(dx*targetY - targetX*dy);//manh. distance + 1/1000th of cross-product to break tie.
		}

		function manhattanDistance(x,y){//our heuristic for A* (admissible & consistent b/c manhattan distance)
			return Math.abs(targetX - x) + Math.abs(targetY - y)
		}

	}

	function part1(depth, targetX, targetY){
		const grid = [Array(targetX+1).fill(null).map( (x,i) => region(16807 * i) )];
	
		for (let y = 1; y <= targetY; y++){
			grid.push([region(48271*y)]);
			for (let x = 1; x <= targetX; x++){
				const geoIndex = grid[y-1][x].erosion * grid[y][x-1].erosion;
				grid[y].push( region(geoIndex) );
			}
		}
	
		/*Print map for debug purposes
			grid[0][0].type = 'M';
			grid[targetY][targetX].type = 'T';
			// console.log(grid[0].map(region => region.type).join(""));
			const map = grid.reduce((gridString, row) => gridString + row.map(region => region.type).join("")+"\n", "");
			console.log(map);
		*/
	
		/*set for calculations of risk level*/
		grid[0][0].type = '.';
		grid[targetY][targetX].type = '.';
	
		const risk = type => ({'.':0, '=':1, '|':2})[type];
		const totalRiskLevel = grid.reduce((gridSum, row) => gridSum + row.reduce((rowSum, region) => rowSum + risk(region.type), 0), 0);
		return totalRiskLevel;
	}
}

day22();