// import { getInput } from '../helpers/helpers.js'
console.clear();

// const INPUT = 8;
// const INPUT = 18;
// const INPUT = 42;
const INPUT = 9005;

// const TEST_CELLS = "Fuel cell at  3,5, grid serial number 8: power level 4.\nFuel cell at  122,79, grid serial number 57: power level -5.\nFuel cell at 217,196, grid serial number 39: power level  0.\nFuel cell at 101,153, grid serial number 71: power level  4.";

const printGrid = (grid) => { //print 2D array in [row][column] format.
	for (let row=0; row<grid.length; row++){
		console.log(grid[row].join("\t"));
	}
}

const day11 = () => {
	let serial = INPUT;
	const GRID_SIZE = 1 + 300;
	let cells = Array(GRID_SIZE).fill(null).map(el => Array(GRID_SIZE).fill('.'));

	for (let y = 1; y < GRID_SIZE; y++){
		for (let x = 1; x < GRID_SIZE; x++){
			let rackID = x + 10;
			let powerLevel = rackID * y;
			powerLevel += serial;
			powerLevel *= rackID;
			powerLevel = parseInt(powerLevel/100)%10;
			powerLevel -= 5;
			cells[y][x] = powerLevel;
		}
	}
	// printGrid(cells);
	// console.log(" ");
	
	let maxPower = Number.NEGATIVE_INFINITY;
	let position;
	for(let frameSize = 1; frameSize < GRID_SIZE; frameSize++){
	// for(let frameSize = 3; frameSize < 4; frameSize++){
		console.time("frame"+frameSize);
		// let totalPowers = Array(GRID_SIZE).fill(null).map(el => Array(GRID_SIZE).fill(0));
		for (let row=1; row < GRID_SIZE - frameSize + 1; row++){
			for (let col=1; col<GRID_SIZE - frameSize + 1; col++){
				let totalPowers = 0;
				for (let x = 0; x < frameSize; x++){
					for (let y = 0; y < frameSize; y++){
						totalPowers += cells[row+x][col+y];
						// totalPowers[row][col] += cells[row+x][col+y];
						// if (totalPowers[row][col] > maxPower){
					}
				}
				if (totalPowers > maxPower){
					position = {
						x: col,
						y: row,
						size: frameSize
					};
					maxPower = totalPowers;
				}
			}
		}
		console.timeEnd("frame"+frameSize);
		// console.log(frameSize);
	}
	// printGrid(totalPowers);
	console.log("max:", maxPower, "position:", position);

	const puzzle1result = "tbd";
	const puzzle2result = "tbd";
	
	console.log("puzzle1: ", puzzle1result);
	console.log("puzzle2: ", puzzle2result);
}

day11();