// import { getInput } from '../helpers/helpers.js'
console.clear();

const printScoreboard = (scoreboard, elves) => {
	let result = "";
	let styling = [];
	for (let i=0; i<scoreboard.length; i++){
		if (i === elves[0]){
			result += `%c(${scoreboard[i]})%c`;
			styling.push("background: red; color:yellow;", "");
		} else if (i === elves[1]){
			result += `%c[${scoreboard[i]}]%c`;
			styling.push("background: blue; color:yellow;", "");
		} else {
			result += ` ${scoreboard[i]} `;
		}
	}
	console.log(result, ...styling);
};

// const INPUT = 9;
// const INPUT2 = "51589";

// const INPUT = 5;
// const INPUT2 = "01245";

// const INPUT = 18;
// const INPUT2 = "92510";

// const INPUT = 2018;
// const INPUT2 = "59414";

const INPUT = 760221;
const INPUT2 = "760221";

const part1 = () => {
	const scoreboard = [3,7];
	const elves = [0,1];
	console.log(elves);
	printScoreboard(scoreboard, elves);
	
	//for a certain number of additional numbers beyond the initial value on the scoreboard...
	while (scoreboard.length < 10 + INPUT){
		const sum = scoreboard[elves[0]] + scoreboard[elves[1]];
		scoreboard.push(...sum.toString().split("").map(digit => parseInt(digit)));
		elves[0] = (elves[0] + 1 + scoreboard[elves[0]]) % scoreboard.length;
		elves[1] = (elves[1] + 1 + scoreboard[elves[1]]) % scoreboard.length;
	}
	
	return scoreboard.slice(INPUT, INPUT+10).join("");
};

function RecipeNode(score, prevNode, nextNode){
	this.score = score;
	this.prevNode = prevNode;
	this.nextNode = nextNode;
}

const printList = (startNode, firstElf, secondElf) => {
	let currentNode = startNode;
	let result = "";
	let styling = [];
	let firstNodePrinted = false;
	while (currentNode !== startNode || !firstNodePrinted){
		// console.log("result:", `(${result})`, currentNode);
		firstNodePrinted = true;
		
		if (firstElf && currentNode === firstElf){
			result += `%c(${currentNode.score})%c`;
			styling.push("background: red; color:yellow;", "");
		} else if (secondElf && currentNode === secondElf){
			result += `%c[${currentNode.score}]%c`;
			styling.push("background: blue; color:yellow;", "");
		} else {
			result += ` ${currentNode.score} `;
		}
		currentNode = currentNode.nextNode;
	}
	console.log(result, ...styling);
};

const lastNScoresString = (lastScore, numScores) => {
	let result="";
	for (let i=0; i<numScores; i++){
		result = lastScore.score + result;
		lastScore = lastScore.prevNode;
	}
	// return "the last N digits as a string";
	return result;
}

const part2 = () => {
	//pointers for the beginning and end of the list
	let firstScore = new RecipeNode("3");
	let lastScore = new RecipeNode("7", firstScore, firstScore);
	firstScore.nextNode = firstScore.prevNode = lastScore;
	//pointers for the elves;
	let elves = [firstScore, lastScore];
	let numRecipes = 2;
	let result = false;
	
	while (numRecipes<100000000){//from 3rd recipe to the last.
		const sum = parseInt(elves[0].score) + parseInt(elves[1].score);
		//instead of pushing onto the scoreboard, weave each digit into the list.
		sum.toString().split("").forEach(digit => {
			//create a new recipe in the list, creating an alternate path between lastScore and firstScore that goes THROUGH the new score..
			const score = new RecipeNode(digit, lastScore, firstScore);
			//eliminate the alternate pathway by reassigning the pointers on lastScore and firstScore.
			lastScore.nextNode = score;
			firstScore.prevNode = score;
			//advance the lastScore pointer and the recipe count.
			lastScore = score;
			numRecipes++;
			
			//test the last N digits against the pattern.
			// printList(firstScore, elves[0], elves[1]);
			let lastFewScores = lastNScoresString(lastScore, INPUT2.length);
			// console.log("#:", numRecipes, lastFewScores);
			if (lastFewScores === INPUT2){
				// console.log("found the pattern @:", numRecipes);
				result = numRecipes - INPUT2.length;
			}
		});
		if (result){
			// printList(firstScore, elves[0], elves[1]);
			return result;
		}
		// console.log("**finished adding digits**");
		//update the positions of the two elves
		elves = elves.map(elf => {
			const numScoresToAdvance = parseInt(elf.score) + 1;
			for (let i=0; i<numScoresToAdvance; i++){
				// console.log("i:", i, elf.score+"->"+elf.nextNode.score);
				elf = elf.nextNode;
			}
			return elf;
		});
		// printList(firstScore, elves[0], elves[1]);
		// console.log("**done with elf pointer adjustment**");
	}
};

const day14 = () => {
	const puzzle1result = part1();
	const puzzle2result = part2();
	
	console.log("puzzle1: ", puzzle1result);
	console.log("puzzle2: ", puzzle2result);
};

day14();