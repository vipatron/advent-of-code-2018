// import { getInput } from '../helpers/helpers.js'
console.clear();

const INPUT = "............|#.|..|..|#.#.....#.#||.#.#...#.|#....\n|...|.##...#.|..||#.##..###...|..#||#|.....|..|...\n#.|.#..||#..|.|..|..|.....##....#.##||..#|.|.....|\n.##||.|..#.......###||.#.#.|..##.#|..#...#.....#.|\n....#|##|.|.|...|||..|...#.......|#..|#|.|..|#|...\n|....|...#.....|#||...#.....#....#|....|.....|....\n.#||..|##.##|..||.#.....#...|##..|#...#.#.|.#.|##.\n..#|##..|......#|..#.|#.##....|.....|..#.#...|...#\n#....#||.|..|..#..#.|........|.|.....||||.|..|....\n|.#|.|...#..|||#.##|#.........|...#..#.|#.....||.|\n.#.##.........|...|.|.#..|.|##...|.|#.||....||.#..\n#.#.....##|.#|..###..|........|.....|.....|#.||.#|\n|..||....#..###.|..#..#.#.......##.#.|.#..|.|.#|.#\n|...##.#..#|..|..|......#..#......|..|..#.....|.||\n.......||#|.|...##.#.##..|#.|.|#|....##||...#.#.|#\n..|###..|.....#|.|.|.|....|....#.#..##|...||......\n|.|#|.#|||#.|#|#..|...#.|..|..#.#...#..#.#.#.#..#.\n...#.###...#............#..|#...||#|..#..|###...|.\n.|......|...#....#.......|...##.|.|..##.#...#||||.\n#...|.#|.|.......|##.#...|#.|.........#.|.|##.||..\n|.|#..#.||#|.|..|..|#........#..#.....#.#..#.#.|..\n#..|.|...#|#..#|....|...|..|#|....|..##|#|......||\n....||...|....|...#|..|.#|.....#.|..||..|...|.....\n#......#.#..##.|#.|||.|#||||.|...#...|#.|#...||...\n.|...|...|.#..|#.|####|.......|.#||.|..|..|..#.#..\n.#.|##.......|||...#..#.#....#..|..#.###........#.\n#.#..|.##..|...#....#.#.|...|.#.|.....|.|......|#.\n#.||.#......#.....##.|...|#..#.#|..#..|..#..#.#.|.\n|...#|#...|....#|##..|....||....#.||||.|#...|.....\n.|...|.|...|...|..|..#|...||||#.#...|.#....##...|.\n.|.#...#|...#||.#....|##.#|..#|||......|.|..||...|\n|#...|.|..|.#...#..||......|.|.##.....||#|#.#|..|.\n..#.|.###......|.|.###|...#.#|#||....#..|....|#..|\n|....|.|.|#.|.#..|.|.#.#.||..##|..|.||#|..|...||..\n#.|#.|....|#|#.|..#...|...|.#.#.....#...#|###.#..#\n...###|.#....#..#..|.|....#|.|.###.|...|..|.##.|#.\n...#....#|.#|#.|#.|#|.|...|.....#..|......|.||###.\n.||#|.|.#.|..|||.#.|..#.....#..|..###||#|||...#..#\n###||#........#|####.|..|#....#|.|.|.|#|.|.#|.##.|\n|..#.#|....#....|.#.|..|...|..#..|....#.###.|.#|#.\n.#..##|##...#.....#||||#.#...||..|........|#......\n.#.|||#.|...#.#.|.|..|......#.#....##|..|#||....##\n...##..|....|#.||......##...#..||##.##.#..#.|##|..\n..#.||...|#..||#....#|.##.#|.........#...||.|#|#|#\n#.......|.|#...##.#..#|#.|.|.|.##|...#...|...#.#..\n.....||......|...#..|......||.|#..#....##|...#...#\n..#|.#....|.......#.#..##......|.#.|..|..#..#.#.||\n.|.....#...#......#...#..||||#|.....||..|....#|...\n#...#.||..#......|.##|#.....|..|..|..|.......|.||.\n..|#.|......||.|.||.|##|||.|..#.......#|||.#.|.|#|";
// const INPUT = getInput(0);
// const INPUT = ".#.#...|#.\n.....#|##|\n.|..|...#.\n..|#.....#\n#.#|||#|#|\n...#.||...\n.|....|...\n||...#|.#|\n|.||||..|.\n...#.|..|.";

const printGrid = grid => console.log(
	grid.reduce((result, row) => result + row.join("") + '\n', "")
);

const resourceScores = grid => {
	const counts = grid.reduce((gridSums, row) => {
		const rowSums = row.reduce((sums, pt) => {
			if (pt === '|') sums.trees++;
			if (pt === '#') sums.yards++;
			return sums;
		}, {trees: 0, yards: 0});
		gridSums.trees += rowSums.trees;
		gridSums.yards += rowSums.yards;
		return gridSums;
	}, {trees: 0, yards: 0});
	return counts.trees * counts.yards;
};

const day0 = () => {
	const tokens = INPUT.split("\n");
	let grid = tokens.map(line => line.split(""));
	printGrid(grid);
	// console.log(grid);
	
	const scores = [];
	let wavelength;

	let MAX_MINUTES = 999;
	//For each minute ...
	for (let minute=1; minute <= MAX_MINUTES; minute++){
	// for (let minute = 1; minute <= 1; minute++){
		//create a blank grid to represent the next minute's state
		const nextGrid = Array(grid.length).fill(null).map(x => Array(grid[0].length));
		// console.log("nextGrid:");
		// printGrid(nextGrid);
		
		// console.log(`grid.length: ${grid.length}, width: ${grid[0].length}`)
		//iterate over the grid and for each (col, row) point ...
		for (let row = 0; row < grid.length; row++){
			for (let col = 0; col < grid[row].length; col++){
				// console.log(`%c${grid[row][col]} @ (${col}, ${row})`, "background: darkred");
				const adjacents = [];
				//push in-range adjacent squares onto the adjacent[] array.
				for(let y = row - 1; y <= row + 1; y++){
					if ( y > -1 && y < grid.length ) {
						for (let x = col -1; x <= col + 1; x++){
							if ( x > -1 && x < grid[y].length ){
								if ( !(y === row && x === col) ){
									adjacents.push(grid[y][x]);
								}
							}
						}
					}
				}
				switch (grid[row][col]){
					case '.':
						if (adjacents.reduce((numTrees, pt) => numTrees + (pt === '|' ? 1 : 0), 0) >= 3){
							nextGrid[row][col] = '|';
						} else {
							nextGrid[row][col] = '.';
						}
					break;
					case '|':
						if (adjacents.reduce((numYards, pt) => numYards + (pt === '#' ? 1 : 0), 0) >= 3){
							nextGrid[row][col] = '#';
						} else {
							nextGrid[row][col] = '|';
						}
					break;
					case '#':
						if (adjacents.includes('#') && adjacents.includes('|')){
							nextGrid[row][col] = '#';
						} else {
							nextGrid[row][col] = '.';
						}
					break;
				}
				// console.log(adjacents, "->", nextGrid[row][col]);
			}
			// console.log(`row ${row}:`, nextGrid[row].join(""));
		}
		scores.push(resourceScores(grid));
		if (wavelength === undefined){
			let periodStartIndex = scores.findIndex(score => score === scores[scores.length - 1]);
			if (periodStartIndex > -1 && periodStartIndex !== scores.length-1){
				wavelength = scores.length - 1 - periodStartIndex;
				MAX_MINUTES = minute + wavelength;
				console.log(`scores.length: ${scores.length}, minute: ${minute}, wavelength: ${wavelength}, MAX_MINUTES: ${MAX_MINUTES}`);
				console.log(`scores[${minute-1}]: ${scores[minute-1]}, scores[${minute-1-wavelength}]: ${scores[minute-1-wavelength]}`);
				// break;
			}
		} else { //wavelength potentially defined. check if it is true.
			console.log(`scores[${minute-1}]: ${scores[minute-1]}, scores[${minute-1-wavelength}]: ${scores[minute-1-wavelength]}`);
			if (scores[minute-1] !== scores[minute-1-wavelength]){//if periodicity not maintained, reset wavelength and upper search limit.
				wavelength = undefined;
				MAX_MINUTES = 999;
			}
		}
		grid = nextGrid;
		// console.log(`%cminute: ${minute}`, "background: navy");
		// printGrid(grid);
	}
	console.log(`%cminute: ${MAX_MINUTES}`, "background: navy");
	printGrid(grid);
	scores.push(resourceScores(grid));
	
	const baseValues = scores.slice(-1 * wavelength);
	const phi = scores.length;
	console.log(`scores[${phi-1}]: ${scores[phi-1]}, baseValues[${0}]: ${baseValues[0]}`);
	
	const testMinute = 1000000000;
	const testScoreIndex = (testMinute - phi) % wavelength;
	console.log(`testMinute: ${testMinute}, testScoreIndex: ${testScoreIndex},  baseValues[${testScoreIndex}]: ${baseValues[testScoreIndex]}`);
	
	// let thousand = 1000;
	// let thousand 
	
	const puzzle1result = scores[10];
	const puzzle2result = baseValues[testScoreIndex];
	
	console.log("puzzle1: ", puzzle1result);
	console.log("puzzle2: ", puzzle2result);
};

day0();