// Gets the input.txt associated with a specific day.
export function getInput(day){
	let xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function(){
		if (xmlhttp.readyState === 4 && xmlhttp.status === 200){
			return xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET", `http://localhost:8080/Day ${day}/input.txt`, false);
	// xmlhttp.open("GET", `https://adventofcode.com/2018/day/${day}/input`, false);
	
	try{
		xmlhttp.send();
		return xmlhttp.responseText;
	} catch (error) {
		console.log(error);
		console.log("Check if Apache XAMPP is running.");
	}	
}

/***
 * @name makeLetterArray
 * @param length
 * @description creates a list of symbols for use, an returns an array of them from [0 to length) (uppercase letters, then lowercase)
 */
//creates a list of symbols for use, an returns an array of them from [0 to length) (uppercase letters, then lowercase)
export const makeLetterArray = length => Array(length).fill(null).map((el, i) =>  String.fromCharCode(i + (i < 26 ? 65 : 71)));