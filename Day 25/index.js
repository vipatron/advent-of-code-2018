import { getInput } from '../helpers/helpers.js'
console.clear();

const VERBOSE = false;

const INPUT = getInput(25);
// const INPUT = " 0,0,0,0\n 3,0,0,0\n 0,3,0,0\n 0,0,3,0\n 0,0,0,3\n 0,0,0,6\n 9,0,0,0\n12,0,0,0";//first example
// const INPUT = "-1,2,2,0\n0,0,2,-2\n0,0,0,-2\n-1,2,0,0\n-2,-2,-2,2\n3,0,2,-1\n-1,3,2,2\n-1,0,-1,0\n0,2,1,-2\n3,0,0,0";//second example
// const INPUT = "1,-1,0,1\n2,0,-1,0\n3,2,-1,0\n0,0,3,1\n0,0,-1,-1\n2,3,-2,0\n-2,2,0,0\n2,-2,0,-1\n1,-1,0,-1\n3,2,0,2";//third example
// const INPUT = "1,-1,-1,-2\n-2,-2,0,1\n0,2,1,3\n-2,3,-2,1\n0,2,3,-2\n-1,-1,1,-2\n0,-2,-1,0\n-2,2,3,-1\n1,2,2,0\n-1,-2,0,-2";//fourth example

const day25 = () => {
	const tokens = INPUT.trim().split("\n");
	// console.log(tokens);
	const points = tokens.map(token => token.trim().match(/-?\d+/g).map(str => parseInt(str)));
	// console.log(points);

	const puzzle1result = part1(points);
	const puzzle2result = "tbd";
	
	console.log("puzzle1: ", puzzle1result);
	console.log("puzzle2: ", puzzle2result);
}

function part1(points){
	const manhattanDistance = (point1, point2) => point1.reduce((sum, coordinate, axis) => sum + Math.abs(coordinate - point2[axis]), 0);
	//begin with each point in its own constellation.
	//for each constellation
		//for each point in that constellation... (1 to len-1)
			//for all other constellations
				//for each point in that other constellation
					//check distance between the points
					//if at least one distance is within the distance cutoff
						//merge the two constellations
							//push the members of the other onto this
							//splice out the other from constellations[]
						//decrease the "other constellation" counter (b/c o/w it will be incremented after the break, and miss the constellation after the one just deleted)
						//break "other constellation loop": keep checking from the perspective of points in this constellation. (len will have increased.)
	const constellations = points.map(point => [point]);
	// console.log(constellations.slice().map(point => point.toString()));
	let counter = 0;
	for (let thisConstellation = 0; thisConstellation < constellations.length; thisConstellation++){
		for (let thisPoint = 0; thisPoint < constellations[thisConstellation].length; thisPoint++){
			for (let otherConstellation = thisConstellation+1; otherConstellation < constellations.length; otherConstellation++){
				for (let otherPoint = 0; otherPoint < constellations[otherConstellation].length; otherPoint++){
					counter++;
					const distance = manhattanDistance(constellations[thisConstellation][thisPoint], constellations[otherConstellation][otherPoint]);
					VERBOSE && console.log(`${constellations[thisConstellation][thisPoint]} to ${constellations[otherConstellation][otherPoint]}: ${distance}`);
					if (distance <= 3){
						VERBOSE && console.log(`%cmerging constellations: [${otherConstellation}] into [${thisConstellation}]`, "background: teal");
						constellations[thisConstellation].push(...constellations[otherConstellation]);
						constellations.splice(otherConstellation, 1);
						otherConstellation--;
						break;
					}
				}
			}
		}
	}
	console.log("total number of runs:", counter, "vs:", points.length * (points.length - 1));
	// console.log(constellations);
	return constellations.length;
}

day25();