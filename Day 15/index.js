// import { getInput } from '../helpers/helpers.js'
console.clear();

// const INPUT = getInput(15);
// const INPUT = "#######\n#.G.E.#\n#E.G.E#\n#.G.E.#\n#######";
// const INPUT = "#######\n#.G...#\n#...EG#\n#.#.#G#\n#..G#E#\n#.....#\n#######";

/*** OTHER EXAMPLES ***/
// const INPUT = "#######\n#G..#E#\n#E#E.E#\n#G.##.#\n#...#E#\n#...E.#\n#######";
// const INPUT = "#######\n#E..EG#\n#.#G.E#\n#E.##E#\n#G..#.#\n#..E#.#\n#######";
// const INPUT = "#######\n#E.G#.#\n#.#G..#\n#G.#.G#\n#G..#.#\n#...E.#\n#######";
// const INPUT = "#######\n#.E...#\n#.#..G#\n#.###.#\n#E#G#G#\n#...#G#\n#######";
// const INPUT = "#########\n#G......#\n#.E.#...#\n#..##..G#\n#...##..#\n#...#...#\n#.G...G.#\n#.....G.#\n#########";


const INPUT = "################################\n####################.###########\n###################..##..#######\n###################.###..#######\n###################.###.########\n##################G.###.########\n##..############.#..##..########\n#...#####.####.....##...########\n#G.....##..###.#.GG#...G.....G##\n#...G....G.G##.....#.#.......###\n##..............G............###\n#......GG.....G............#####\n#####.........#####.......E..###\n#####.....G..#######.......#####\n#####.......#########......###.#\n######......#########..........#\n#####.....G.#########..........#\n##.....#...G#########......#...#\n###.#....G..#########G.........#\n##..###......#######E..........#\n##..###...G...#####E..E.....E..#\n###..##...............E.#.E....#\n###..##..##E...###......##..####\n##..G...###....###......########\n##......###E....##....##########\n###...#####..E..###...##########\n##....####......####.###########\n####..#####.....##...###########\n############..####....##########\n############.#####....##########\n##########...#####.#.###########\n################################";

//Edge cases from the subreddit:
// const INPUT = "####\n##E#\n#GG#\n####";
// const INPUT = "#####\n#GG##\n#.###\n#..E#\n#.#G#\n#.E##\n#####";

//Someone else's input: https://www.reddit.com/r/adventofcode/comments/a6yleq/2018_day_15_part_2_my_solution_seems_to_work_for/
// const INPUT = "################################\n####.#######..G..########.....##\n##...........G#..#######.......#\n#...#...G.....#######..#......##\n########.......######..##.E...##\n########......G..####..###....##\n#...###.#.....##..G##.....#...##\n##....#.G#....####..##........##\n##..#....#..#######...........##\n#####...G.G..#######...G......##\n#########.GG..G####...###......#\n#########.G....EG.....###.....##\n########......#####...##########\n#########....#######..##########\n#########G..#########.##########\n#########...#########.##########\n######...G..#########.##########\n#G###......G#########.##########\n#.##.....G..#########..#########\n#............#######...#########\n#...#.........#####....#########\n#####.G..................#######\n####.....................#######\n####.........E..........########\n#####..........E....E....#######\n####....#.......#...#....#######\n####.......##.....E.#E...#######\n#####..E...####.......##########\n########....###.E..E############\n#########.....##################\n#############.##################\n################################";

//forked path simple example:
// const INPUT = "##########\n#E.......#\n#........#\n#..##....#\n#..##....#\n#.....G..#\n#........#\n#........#\n#........#\n##########";

// INPUT FROM REPLY TO MY CALL FOR INPUT:
//https://www.reddit.com/r/adventofcode/comments/afdy8y/spoilers_day_15_part_2_need_your_inputs_and/edxtv22
// const INPUT = "################################\n#########...####################\n#########...###########.########\n#########G..##########....######\n##########..###########...######\n#########G...##########...######\n#########..G.###########..######\n########...#.##########..#######\n#######G#..###E######....#######\n#######G.....#.######....#######\n######...G......##E......#######\n####...##.#..G..G.........######\n###..........G#####.......####.#\n####........G#######...........#\n####..G.....#########......#...#\n###.........#########........###\n##.....G.G..#########......#####\n#...G.......#########.........##\n#.G.........#########.E.##...###\n##.....G.....#######....G#.E...#\n##............#####...E.......##\n#.G...........E.......#E...##.##\n#....G........###########.....##\n#......##...#.##################\n#.#.........E..##.##############\n#.#.......G.......##############\n#.###........E....##############\n#.####.....###....##############\n#.#####......E..################\n#######..........###############\n#########..####.################\n################################";

// https://www.reddit.com/r/adventofcode/comments/afdy8y/spoilers_day_15_part_2_need_your_inputs_and/edyb4vw
// const INPUT = "################################\n################.G#...##...#####\n#######..#######..#.G..##..#####\n#######....#####........##.#####\n######.....#####.....GG.##.#####\n######..GG.##.###G.........#####\n#####........G####.......#######\n######.#..G...####........######\n##########....#####...G...######\n########.......###..........####\n#########...GG####............##\n#########....................###\n######........#####...E......###\n####....G....#######........####\n###.........#########.......####\n#...#.G..G..#########..........#\n#..###..#...#########E.E....E###\n#..##...#...#########.E...E...##\n#.....G.....#########.........##\n#......G.G...#######........####\n###..G...#....#####........#####\n###########....G........EE..####\n##########...................###\n##########...................###\n#######.............E....##E####\n#######................#########\n########.#.............#########\n#######..#####.#......##########\n######...#######...##.##########\n################..###.##########\n###############.......##########\n################################";

const printGrid = grid => console.log(grid.reduce(
	(gridString, row) => gridString += row.join("") + "\n"
	, ""
));

const printGridWithHP = (grid, units) => {
	let unitStrings = Array(grid.length).fill("");
	units.slice().sort((unitA, unitB) => unitA.y - unitB.y || unitA.x - unitB.x).forEach( unit => unitStrings[unit.y] += unit.type+`(${unit.HP}), `);
	console.log(grid.reduce(
		(gridString, row, y) => gridString += row.join("") + "\t" + unitStrings[y] + "\n"
		, ""
	));
};

//Returns true if point1 preceded point2 in reading order.
const inReadingOrder = (point1, point2) => point1.y < point2.y || point1.y === point2.y && point1.x < point2.x;

const readingOrderCompare = (point1, point2) => point1.y - point2.y || point1.x - point2.x;

const printSearchQueue = queue => {
	let result = "";
	queue.forEach(pt => {
		result += `[${pt.distance} @ ${pt.x}, ${pt.y}], `
	});
	console.log(result);
};

const enemy = {
	E: 'G',
	G: 'E'
};

const day15 = () => {

	//whichever most lax level of output is allowed includes all the stricter outputs.
	const VERBOSE = false;
	const MODERATELY_VERBOSE = false || VERBOSE;
	const TERSE = true || MODERATELY_VERBOSE;

	const ATTACK_STRENGTH = {
		E: 16,
		// E: 3,
		G: 3
	};
	const BASE_HP = 200;

	const puzzleResults = {};
	/*** START HERE WITH EVERY ELF POWER ATTACK ++ */
	// while(ATTACK_STRENGTH.E < 17 && !puzzleResults.hasOwnProperty('puzzle2result')){
	while(ATTACK_STRENGTH.E < 50 && !puzzleResults.hasOwnProperty('puzzle2result')){
		const grid = INPUT.split("\n").map(y => y.split(""));
		console.log("Initial State:");
		printGrid(grid);
		
		//initial traversal of input grid: create a list of unit objects
		let units = [];
		for (let y=1; y < grid.length-1; y++){
			for (let col=1; col < grid[y].length-1; col++){
				// pointsScanned += grid[y][col];
				
				//model unit object: { type: "E/G", y:#, col: #, HP: 200 }
				if (grid[y][col] === "E" || grid[y][col] === "G"){
					units.push({
						type: grid[y][col],
						y: y,
						x: col,
						HP: BASE_HP
					});
				}
			}
		}
		const TOTAL_ELVES = units.filter(unit => unit.type === 'E').length;
		console.log("%cInitial number of elves:", "background-color: #c3c", TOTAL_ELVES);
		console.log("units:", units); //the units are current in reading order. We will keep them sorted and assume reading order sort.
		// console.log(pointsScanned);
		
		//during each round...
		let round = 1;
		let combatOver = false;
		while (round < 100 && !combatOver){
			console.log("%cround", "background: orange", round);
			TERSE && console.log("%cunits:"+units.reduce((result, unit, i) => result + `  ${i}:${unit.type}@(${unit.x}, ${unit.y})`, ""), "background: orange");
			
			//for each unit... perform a BFS by traversing the grid in reading order (by y then column).
			for (let currentUnit = 0; currentUnit < units.length; currentUnit++){
				TERSE && console.log("unit #:"+currentUnit+">>", units[currentUnit]);
				
				if (units[currentUnit].HP > 0){
					let chosenPointFound = false;
							
					/*** This check may have to occur before the BFS ***/
					if (!units.filter(unit => unit.type === enemy[units[currentUnit].type] && unit.HP > 0).length){
						console.log("%cNO SURVIVING ENEMY UNITS, last complete round = "+(round-1), "background: #a33; color: #0f0");
						let sumOfRemainingHP = units.reduce((sum, unit) => sum += unit.HP > 0 ? unit.HP : 0, 0);
						console.log("remainingHP:", sumOfRemainingHP);
						if (!puzzleResults.hasOwnProperty('puzzle1result')){
							puzzleResults.puzzle1result = (round - 1) * sumOfRemainingHP;
						}
						const remainingElves = units.filter(unit => unit.HP > 0 && unit.type === 'E'); //alive AND elves.
						if (remainingElves.length === TOTAL_ELVES){ //the elves won, and without any losses.
							puzzleResults.puzzle2result = `Attack Strength: ${ATTACK_STRENGTH.E}, outcome: ${(round - 1) * sumOfRemainingHP}`;
						}
						if (puzzleResults.hasOwnProperty('puzzle1result') && puzzleResults.hasOwnProperty('puzzle2result')){
							printGridWithHP(grid, units);
							return puzzleResults;
						} else {
							combatOver = true;
							// puzzleResults.puzzle2result = "tbd";
							// console.log(puzzleResults);
							// return puzzleResults; /**TESTING ONLY - START HERE **/
						}
					} //else {
						// console.log("%cEnemies remain.", "background: #0a0; color: #c00; font-weight: 600");
					// }
					if (combatOver){
						break;
					}
					let currentPoint = {
						y: units[currentUnit].y,
						x: units[currentUnit].x,
						distance: 0
					};
					const solvedPoints = [];
					const searchQueue = [currentPoint]; //priority queue of points to examine
					let pointsVisited = 0;
					let finalSearchDepth = Infinity;

					//SEARCH FOR A "CHOSEN POINT" towards which to move.
					while (searchQueue.length > 0){//&& pointsVisited < 1000){ /*** MUST GET RID OF points limit */
						let unitSummary = `[${units[currentUnit].type} @ ${units[currentUnit].x}, ${units[currentUnit].y}]`
						// console.log("queue size:", searchQueue.length);
	
						currentPoint = searchQueue.shift();
						pointsVisited++;
						VERBOSE && console.log(`%c*** round: ${round} unit: ${currentUnit} ${unitSummary} current dist#: ${currentPoint.distance} (${currentPoint.x}, ${currentPoint.y  }) ***`, "background: lightgreen");
						
						// console.log("current point:", grid[currentPoint.y][currentPoint.x], currentPoint);
						if (currentPoint.distance > finalSearchDepth){
							VERBOSE && console.log("%cSearch depth exceeded the depth at which an in-range point was found. exiting", "background: turquoise");
							break;
						}
						const adjacents = [
							{y: currentPoint.y-1, x: currentPoint.x},
							{y: currentPoint.y, x: currentPoint.x-1},
							{y: currentPoint.y, x: currentPoint.x+1},
							{y: currentPoint.y+1, x: currentPoint.x},
						];
						//console.log("adj:", adjacents);
						//Test each new point: Of the 4 adjacent points, there are 4 options:
						for (let i=0; i<adjacents.length; i++){
							adjacents[i].distance = currentPoint.distance + 1;
							adjacents[i].type = grid[adjacents[i].y][adjacents[i].x];
							adjacents[i].priorPoints = [currentPoint];
							adjacents[i].inRange = false;
							
							//1: Already been visited (on the queue, or solved): If the distance is less than or equal to the distance of the priorStepNode, replace it IFF it comes at a lower reading order.
							if (!solvedPoints.some(pt => pt.x === adjacents[i].x && pt.y === adjacents[i].y)){
								const indexOfAdj = searchQueue.findIndex(pt => pt.x === adjacents[i].x && pt.y === adjacents[i].y);
								if (indexOfAdj > -1){
									VERBOSE && console.log(`%cfound adjacent: (${searchQueue[indexOfAdj].x}, ${searchQueue[indexOfAdj].y}) @ dist ${searchQueue[indexOfAdj].distance}:` + searchQueue[indexOfAdj].priorPoints.reduce(
										(str, pt) =>  str + ` (${pt.x}, ${pt.y})`, ""
									), "background: pink");
									if (searchQueue[indexOfAdj].distance === adjacents[i].distance){
										searchQueue[indexOfAdj].priorPoints.push(currentPoint);
										VERBOSE && console.log(`%cnew priorPoints:` + searchQueue[indexOfAdj].priorPoints.reduce(
											(str, pt) =>  str + ` (${pt.x}, ${pt.y})`, ""
										), "background: #c99");
									} else {
										VERBOSE && console.log("adjacent point's prior point unchanged:", `(${searchQueue[indexOfAdj].priorPoint.x}, ${searchQueue[indexOfAdj].priorPoint.y})`);
									}
								} else {
									//4: There is a friendly unit or wall there: That point is inaccessible - ignore it.
									if (adjacents[i].type !== units[currentUnit].type && adjacents[i].type !== "#"){
										VERBOSE && console.log("Adj type:", adjacents[i].type, "@ ("+adjacents[i].x+",",adjacents[i].y+")", "current unit", units[currentUnit].type);
										//3: The next point is an enemy: thus, the current point is "in range" of an enemy. Set a flag to complete the search after this level of depth. There is no guarantee that this "in-range" point will be the one at this depth with the lowest reading order.
										if (adjacents[i].type === enemy[units[currentUnit].type]){
											VERBOSE && console.log("IN RANGE POINT FOUND at distance",currentPoint.distance+"."," Enemy at:", `(${adjacents[i].x}, ${adjacents[i].y})`);
											finalSearchDepth = currentPoint.distance;
											currentPoint.inRange = true;
											MODERATELY_VERBOSE && console.log("POINT AT:", `(${currentPoint.x}, ${currentPoint.y})`," inRange:", currentPoint.inRange);
											chosenPointFound = true;
										} else {
											//2: Not visited yet: put it on the queue to visit. This is how we will know what to visit next—if it were a stack we'd be doing DFS— All the points reachable in 1 step are guaranteed to be on the queue before the points reachable in 2 steps, because they were the only ones that could have been added from the origin (0 steps). Thus, every treeNode object on the queue needs {y, column, numSteps, and priorStepNode} properties in order to be able to break ties (at some point, we'll need tiered sorting by: steps, y, col: since push & sort must be equally as costly, it's probably most efficient to perform insertion sort on the back end of the queue.)
											let insertionIndex = searchQueue.length;
											//the loop will stop decrementing the insertionIndex when any of the three consequences are false.
											while ( insertionIndex > 0 // so the last decrement occurs when index ==1, so final insertionIndex will be 0. (i.e., only decrement if we're still going to be inserting into the list.)
												&& (searchQueue[insertionIndex-1].distance > adjacents[i].distance // keep looking as long as our new point is to the right of any point with a larger distance.
												 || (searchQueue[insertionIndex-1].distance === adjacents[i].distance && inReadingOrder(adjacents[i], searchQueue[insertionIndex-1])) ) //at this point, the distance of the left-side point in the queue could be smaller or equal to that of our new point, so we have to check, and if they're equal, only then break the tie by reading order.
											){
												insertionIndex--;
											}
											searchQueue.splice(insertionIndex, 0, adjacents[i]);
											VERBOSE && printSearchQueue(searchQueue);
										}			
									} else {
										VERBOSE && console.log(`not added: ${adjacents[i].type} @ ${adjacents[i].x}, ${adjacents[i].y}`);
									}
								}
							} else {
								let solved = solvedPoints.find(pt => pt.x === adjacents[i].x && pt.y === adjacents[i].y);
								VERBOSE && console.log(`%c(${solved.x}, ${solved.y}) already solved at distance ${solved.distance}`, "background: turquoise")
							}
						}
						solvedPoints.push(currentPoint);
					}
	
					//IF CHOSEN POINT FOUND, move the unit towards it.
					if (chosenPointFound){
						//find the chosen point.
						const backtrace = [solvedPoints.filter(pt => pt.inRange).sort(readingOrderCompare)[0]];
						TERSE && console.log(`%cchosen point found: ${backtrace[0].x}, ${backtrace[0].y}`, "background: violet");
						let backtraceProgress = 0;
						
						while (backtrace[backtrace.length-1].distance > 0){
							backtrace[backtraceProgress].priorPoints.forEach(
								pt => {
									if (!backtrace.some(a => a.x === pt.x && a.y === pt.y)){
										backtrace.push(pt);
									}
								}
							);
							backtraceProgress++;
						}
						MODERATELY_VERBOSE && console.log("progress:", backtraceProgress);
						MODERATELY_VERBOSE && console.log("backtrace:", backtrace);
						let chosenPoint = backtrace.filter(pt => pt.distance === 1).sort(readingOrderCompare)[0] || backtrace[backtrace.length-1];
						
						// console.log("chosenPoint to move to", nextMove);
						// let chosenPoint = units.find(unit => unit.x ==)
						TERSE && console.log("%cNext Move to:", "background: grey", chosenPoint);
	
						//MOVE UNIT
						//Move the unit in the grid
						grid[units[currentUnit].y][units[currentUnit].x] = '.';
						grid[chosenPoint.y][chosenPoint.x] = units[currentUnit].type;
						//Update the unit object.
						units[currentUnit].y = chosenPoint.y;
						units[currentUnit].x = chosenPoint.x;
					} else { // KEEP THIS VERSION FOR TESTING ONLY.
						MODERATELY_VERBOSE && console.log("%csearch path ended without solution.", "background: yellow");
					}
	
					//Attack if possible
					//Look around the current position of the unit for enemies.
					const potentialTargets = [
						{y: units[currentUnit].y-1, x: units[currentUnit].x},
						{y: units[currentUnit].y, x: units[currentUnit].x-1},
						{y: units[currentUnit].y, x: units[currentUnit].x+1},
						{y: units[currentUnit].y+1, x: units[currentUnit].x},
					].filter(//out everything except points occupied by enemy units
						point => grid[point.y][point.x] === enemy[units[currentUnit].type]
					);
					MODERATELY_VERBOSE && console.log("potential targets:", potentialTargets);
					if (potentialTargets.length){
						let target = potentialTargets.map(//onto enemy unit objects instead of locations
							// enemyLocation => units.find(unit => unit.y === enemyLocation.y && unit.x === enemyLocation.x)
							enemyLocation => units.find(unit => unit.HP > 0 && unit.y === enemyLocation.y && unit.x === enemyLocation.x)
						).reduce(//down to the unit with the lowest HP, and the lowest reading order in case of HP tie.
							(target, enemy) =>
								enemy.HP < target.HP
									? enemy
									: enemy.HP === target.HP && inReadingOrder(enemy, target)
										? enemy
										: target
							, { HP: Infinity, x: Infinity, y: Infinity }
						);
						TERSE && console.log("actual target:", target);
						target.HP -= ATTACK_STRENGTH[units[currentUnit].type];
						//if the HP <=0, remove it from the grid and unit list.
						if(target.HP < 1){
							TERSE && console.log(`%ccurrent grid value at target position: ${grid[target.y][target.x]}`, "background: lightgreen");
							grid[target.y][target.x] = '.';
						}
	
						//if there were no legal targets, combat ends.
							//output number = round # of the round before the final one * sum(all remaining HP on the board);	
					} else {
						MODERATELY_VERBOSE && console.log("No potential targets");
					}
				}
			}
			units = units.filter(unit => unit.HP > 0);
			units.sort((unitA, unitB) => unitA.y - unitB.y || unitA.x - unitB.x);
	
			ATTACK_STRENGTH.E > 13 && printGridWithHP(grid, units);
	
			round++;
		}

		console.log(`%cThe goblins won. Elf Attack: ${ATTACK_STRENGTH.E} -> ${ATTACK_STRENGTH.E+1}`, "background-color: turquoise; font-weight: 600");
		// puzzleResults.puzzle2result = "ENDED AFTER ELF ATTACK STRENGTH "+ATTACK_STRENGTH.E;
		// return puzzleResults; /** ELIMINATE THIS RETURN STATEMENT */
		ATTACK_STRENGTH.E++;
	}
}

const { puzzle1result, puzzle2result } = day15();
// day15();
// let puzzle1result, puzzle2result;
// puzzle1result = puzzle2result = "tbd";

console.log("puzzle1: ", puzzle1result);
console.log("puzzle2: ", puzzle2result);
