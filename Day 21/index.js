import { getInput } from '../helpers/helpers.js'
console.clear();

const INPUT = getInput(21);

const day21 = () => {
	const tokens = INPUT.trim().split("\n");
	const IP_Index = tokens.shift().match(/\d+/)[0];

	const program = tokens.map(line => line.split(" ").map((x, i) => i === 0 ? x : parseInt(x)));
	const parsedProgram = parseProgram(program);

	const numRounds = 1000000;
	console.time("getting values");
	const haltValues = altProgram(numRounds);
	console.timeEnd("getting values");

	console.log("upper limit", numRounds, "rounds:", haltValues);
	const puzzle1result = haltValues[0]; //initially solved using worksheet in "restructuredProgram.txt"
	// const puzzle1result = parseProgram(program);
	const puzzle2result = haltValues[haltValues.length-1];
/*
	const printNumWithBitString = num => console.log(num+":", num.toString(2)) || true;

	let numbers = [123, 456, 65536, 678134, 16777215, 678134*65899];
	numbers.forEach(printNumWithBitString);

	console.log("banning");
	const banAndPrint = (num1, num2) => {
		let str1 = num1.toString(2);
		let str2 = num2.toString(2);
		if (str1.length > str2.length){
			str2 = str2.padStart(str1.length, "0");
		} else {
			str1 = str1.padStart(str2.length, "0");
		}

		console.log(num1, num2);
		let bitwiseAnd = num1 & num2;
		console.log(str1);
		console.log(str2);
		console.log("=".repeat(str1.length));
		console.log("%c"+(bitwiseAnd).toString(2).padStart(str1.length, "0"), "background: lightgreen; color:black", bitwiseAnd);
	}
	const borAndPrint = (num1, num2) => {
		let str1 = num1.toString(2);
		let str2 = num2.toString(2);
		if (str1.length > str2.length){
			str2 = str2.padStart(str1.length, "0");
		} else {
			str1 = str1.padStart(str2.length, "0");
		}

		console.log(num1, num2);
		let bitwiseOr = num1 | num2;
		console.log(str1);
		console.log(str2);
		console.log("=".repeat(str1.length));
		console.log("%c"+(bitwiseOr).toString(2).padStart(str1.length, "0"), "background: lightgreen; color:black", bitwiseOr);
	}

	banAndPrint(numbers[0], numbers[1]);
	banAndPrint(numbers[2], 255);
	banAndPrint(numbers[3], numbers[4]);
	banAndPrint(numbers[4], numbers[5]);
*/
	console.log("puzzle1: ", puzzle1result);
	console.log("puzzle2: ", puzzle2result);
}

function altProgram (numRounds = 1) {
	const valuesForA = [];//values for A that will halt the outer loop.
	let counter = 0;

	let [b, c, e, f] = [0, 0, 0, 0];
	do {
		counter++;

		b = e | 65536 //b = e + (e<2**16 ? 2:16 : 0);
		e = 678134
		e = (((e + b % 256) % 16777216) * 65899) % 16777216

		while (b >= 256){
			b = Math.floor(b/256)
			e = (((e + b % 256) % 16777216) * 65899) % 16777216
		}

		if (valuesForA.includes(e))break;
		valuesForA.push(e);
	} while (counter < numRounds)//while(e !== a)

	return valuesForA;
}

const parseProgram = program => {
	let result = program.map((line, i) => {
		let lineResult;
		lineResult = cmdString(line) || `${line.join(" ")}`;
		if (lineResult.includes("d =")){
			if (!/[acbef]/.test(lineResult)){
				let expression = lineResult.match(/d =(.*)/)[1].replace(/d/g, i);
				let value = eval(expression);
				// window.alert("("+value+")")
				lineResult = "GOTO " + (value + 1);//changed to point to correct line.
			} else {
				lineResult = lineResult.replace("d =", "GOTO").replace("d", i+1); //need the +1 to GOTO the correct line.
			}
		} else {
			lineResult = lineResult.replace("d", i); //we know the value of the instruction pointer at any line.
		}
		return `[${i}]: ${lineResult}`;
	});
	/** RESUME HERE:
	TODO: gt or eq BEFORE A GOTO is an IF/ELSE STATEMENT WITH TWO POSSIBLE GOTO OUTCOMES.
	TODO: summarize the nested operations on a variable? (Not strictly necessary).
	*/
	return "\n"+result.join("\n");
}

const cmdString = line => {
	switch (line[0]){
		case 'setr':
			return `${varName(line[3])} = ${varName(line[1])}`;
		case 'seti':
			return `${varName(line[3])} = ${line[1]}`;
		case 'banr':
			return `${varName(line[3])} = ${varName(line[1])} & ${varName(line[2])}`;
		case 'bani':
			return `${varName(line[3])} = ${varName(line[1])} & ${line[2]}`;
		case 'borr':
			return `${varName(line[3])} = ${varName(line[1])} | ${varName(line[2])}`;
		case 'bori':
			return `${varName(line[3])} = ${varName(line[1])} | ${line[2]}`;
		case 'addr':
			return `${varName(line[3])} = ${varName(line[1])} + ${varName(line[2])}`;
		case 'addi':
			return `${varName(line[3])} = ${varName(line[1])} + ${line[2]}`;
		case 'mulr':
			return `${varName(line[3])} = ${varName(line[1])} * ${varName(line[2])}`;
		case 'muli':
			return `${varName(line[3])} = ${varName(line[1])} * ${line[2]}`;
		case 'gtir':
			return `(${line[1]} > ${varName(line[2])}) ? -> ${varName(line[3])}`;
		case 'gtri':
			return `(${varName(line[1])} > ${line[2]}) ? -> ${varName(line[3])}`;
		case 'gtrr':
			return `(${varName(line[1])} > ${varName(line[2])}) ? -> ${varName(line[3])}`;
		case 'eqir':
			return `(${line[1]} == ${varName(line[2])}) ? -> ${varName(line[3])}`;
		case 'eqri':
			return `(${varName(line[1])} == ${line[2]}) ? -> ${varName(line[3])}`;
		case 'eqrr':
			return `(${varName(line[1])} == ${varName(line[2])}) ? -> ${varName(line[3])}`;
		default:
			return undefined;
	}
}

const varName = registerNumber => String.fromCharCode(97 + registerNumber);

const OPERATIONS = {
	addr: ([, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A] + before[B];
		return after;
	},
	addi: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A] + B;
		return after;
	},
	mulr: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A] * before [B];
		return after;
	},
	muli: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A] * B;
		return after;
	},
	banr: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A] & before [B];
		return after;
	},
	bani: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A] & B;
		return after;
	},
	borr: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A] | before [B];
		return after;
	},
	bori: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A] | B;
		return after;
	},
	setr: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A];
		return after;
	},
	seti: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = A;
		return after;
	},
	gtir: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = A > before [B] ? 1 : 0;
		return after;
	},
	gtri: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A] > B ? 1 : 0;
		return after;
	},
	gtrr: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A] > before [B] ? 1 : 0;
		return after;
	},
	eqir: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = A == before [B] ? 1 : 0;
		return after;
	},
	eqri: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A] == B ? 1 : 0;
		return after;
	},
	eqrr: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A] == before [B] ? 1 : 0;
		return after;
	}
};

day21();