import { getInput } from '../helpers/helpers.js'
console.clear();

const INPUT = getInput(16);

const day16 = () => {
	// Split 
	const parts = INPUT.trim().split("\n\n\n\n").map((part, i) => part.split(i===0 ? "\n\n" : "\n"));
	// console.log(parts);

	console.time("puzzle1 timer");
	const puzzle1result = part1(parts[0]);
	console.timeEnd("puzzle1 timer");

	console.time("puzzle2 timer");
	const puzzle2result = part2(...parts);
	console.timeEnd("puzzle2 timer");
	
	console.log("puzzle1: ", puzzle1result);
	console.log("puzzle2: ", puzzle2result);
};

const part1 = (samples) => {
	// console.log(samples);
	let numSamplesBehaveLikeThreeOps = 0;
	for (let sampleNum = 0; sampleNum < samples.length; sampleNum++){
		const [before, instruction, after] = samples[sampleNum].split("\n").map(line => line.match(/(\d+).*(\d+).*(\d+).*(\d+)/).slice(1).map(num => parseInt(num)));
		const instructionString = instruction.join(" ");
		let numOpsWithSameResult = 0;
		for(let opCode in OPERATIONS){
			// console.log(`before: [${before.join(", ")}]`);
			// console.log(opCode+"?", instructionString);
			const opResult = OPERATIONS[opCode](instruction, before);
			// console.log(`result: [${opResult.join(", ")}]`);
			// console.log(` after: [${after.join(", ")}]`);
			let opResultsInAfter = registersIdentical(opResult, after);
			// console.log("registers identical?", opResultsInAfter);
			if (opResultsInAfter){
				numOpsWithSameResult++;
			}
			// console.log("+++++++++++");
		}
		// console.log(`%c# of Ops that could produce this sample: ${numOpsWithSameResult}`, "background-color: darkgreen");
		if (numOpsWithSameResult > 2) {
			numSamplesBehaveLikeThreeOps++;
		}
	}
	return numSamplesBehaveLikeThreeOps;
};

const part2 = (samples, testProgram) => {
	// console.log(samples);
	// console.log(testProgram);
	let opCodes = Array(16).fill(null).map(i => Object.keys(OPERATIONS));
	// console.log(opCodes);
	for (let sampleNum = 0; sampleNum < samples.length; sampleNum++){
	// for (let sampleNum = 0; sampleNum < 3; sampleNum++){
		const [before, instruction, after] = samples[sampleNum].split("\n").map(line => line.match(/(\d+).*(\d+).*(\d+).*(\d+)/).slice(1).map(num => parseInt(num)));
		const instructionString = instruction.join(" ");
		for(let opCode in OPERATIONS){
			// console.log(`%c${opCode}? ${instructionString}`, "background: #600");
			const opResult = OPERATIONS[opCode](instruction, before);
			// console.log(`result: [${opResult.join(", ")}]`);
			// console.log(` after: [${after.join(", ")}]`);
			if (!registersIdentical(opResult, after)){
				// console.log(`opCode ${instruction[0]} ${opCodes[instruction[0]]}`);
				// console.log(`%cDoesn't match, ${instruction[0]} can't be ${opCode}`, "background: #303");
				let opCodeIndex = opCodes[instruction[0]].findIndex(op => op === opCode);
				// console.log("opCodeIndex:", opCodeIndex);
				if (opCodeIndex > -1){
					opCodes[instruction[0]].splice(opCodeIndex, 1);
					// console.log(`opCode ${instruction[0]} ${opCodes[instruction[0]]}`);
				}
			}
			// console.log("+++++++++++");
		}
		// console.log(`%c# of Ops that could produce this sample: ${numOpsWithSameResult}`, "background-color: darkgreen");
	}

	console.log("%c"+opCodes.reduce( (str, possInstr, i) => str + i + ": " + possInstr.toString() + "\n" , ""), "background:darkgreen" );

	//Solved the constraint problem created by processing the samples in order to figure out which code [0,15] stands for which operation.
	while (opCodes.some(possibleOperations => possibleOperations.length > 1)){
		//get an array of strings representing the ops for which we know the codes, and iterate over the solved ops
		const solvedOps = opCodes.filter(possibleOperations => possibleOperations.length === 1).map(operationArray => operationArray[0]);
		for (let solvedOp = 0; solvedOp < solvedOps.length; solvedOp++){
			// console.log(`%cOne possible code for op ${solvedOps[solvedOp]}`, "background: navy; font-weight: 800");
			// console.log("%c"+opCodes.reduce( (str, possInstr, i) => str + i + ": " + possInstr.toString() + "\n" , ""), "background:navy" );
			//search through the opCodes[], removing the solvedOp from the array of possibleOps from code [0, 15] except where they are already solved. (That prevents the current solvedOp from being removed from it's correct place.)
			for (let code = 0; code < 16; code++){
				let solvedOpIndex = opCodes[code].findIndex(op => op === solvedOps[solvedOp]);
				if (solvedOpIndex > -1 && opCodes[code].length > 1){
					opCodes[code].splice(solvedOpIndex, 1);
				}
			}
			// console.log("%c"+opCodes.reduce((str, instr, i) => str + i + ": " + instr.toString() + "\n" , ""), "background: darkred");
		}
	}
	console.log("Solved the constraint problem");
	console.log("%c"+opCodes.reduce((str, instr, i) => str + i + ": " + instr.toString() + "\n" , ""), "background: darkred");
	opCodes = opCodes.map(opArray => opArray[0]);

	//Run the program on initial register state [0, 0, 0, 0];
	let registers = [0, 0, 0, 0];
	console.log(registers);
	console.log(testProgram);
	for (let line=0; line < testProgram.length; line++){
	// for (let line = 0; line < 5; line++){
		const instruction = testProgram[line].split(" ").map(num => parseInt(num));
		const op = opCodes[instruction[0]];
		registers = OPERATIONS[op](instruction, registers)
		// console.log(op, instruction, registers);
	}

	return registers[0];
};


const OPERATIONS = {
	addr: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A] + before[B];
		return after;
	},
	addi: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A] + B;
		return after;
	},
	mulr: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A] * before [B];
		return after;
	},
	muli: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A] * B;
		return after;
	},
	banr: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A] & before [B];
		return after;
	},
	bani: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A] & B;
		return after;
	},
	borr: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A] | before [B];
		return after;
	},
	bori: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A] | B;
		return after;
	},
	setr: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A];
		return after;
	},
	seti: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = A;
		return after;
	},
	gtir: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = A > before [B] ? 1 : 0;
		return after;
	},
	gtri: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A] > B ? 1 : 0;
		return after;
	},
	gtrr: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A] > before [B] ? 1 : 0;
		return after;
	},
	eqir: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = A == before [B] ? 1 : 0;
		return after;
	},
	eqri: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A] == B ? 1 : 0;
		return after;
	},
	eqrr: ([op, A, B, C], before) => {
		const after = before.slice();
		after[C] = before[A] == before [B] ? 1 : 0;
		return after;
	}
};

const registersIdentical = (regArr1, regArr2) => { // can assume an array of 4 numbers.
	for (let i=0; i < 4; i++){
		if (regArr1[i] !== regArr2[i]){
			return false;//optimizes execution to break as soon as Identicality not true.
		}
	}
	return true;//only executes if all the registers were identical.
}

day16();